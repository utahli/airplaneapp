package com.lqt.dao;

import com.lqt.entity.Passengers_Pnformation;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/4/10.
 */
@Repository
public interface IPassengers_Pnformation {

    void add(Passengers_Pnformation passengers_pnformation);

    void del(Passengers_Pnformation passengers_pnformation);

    void update(Passengers_Pnformation passengers_pnformation);

    Passengers_Pnformation findOneByPri(Passengers_Pnformation passengers_pnformation);

    List<Passengers_Pnformation> check(Passengers_Pnformation passengers_pnformation);
    List<Passengers_Pnformation> updateCheck(Passengers_Pnformation passengers_pnformation);

    List<Passengers_Pnformation> findAll();

    List<Passengers_Pnformation> adminfindBy(Passengers_Pnformation passengers_pnformation);
}

package com.lqt.dao;

import com.lqt.entity.Company_Flight_Relationship;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
@Repository
public interface ICompany_Flight_Relationship {
    void add(Company_Flight_Relationship company_flight_relationship);

    List<Company_Flight_Relationship> findAll();

    Company_Flight_Relationship findOneByName(Company_Flight_Relationship company_flight_relationship);

    Company_Flight_Relationship findOneByNum(Company_Flight_Relationship company_flight_relationship);

    List<Company_Flight_Relationship> findListByPri(Company_Flight_Relationship company_flight_relationship);
}

package com.lqt.dao;

import com.lqt.entity.Users_Flight_Relationship;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/11.
 */
@Repository
public interface IUsers_Flight_Relationship {

    void add(Users_Flight_Relationship users_flight_relationship);

    void update(Users_Flight_Relationship users_flight_relationship);

    void updateunpay(Users_Flight_Relationship users_flight_relationship);

    void updatepaied(Users_Flight_Relationship users_flight_relationship);

    List<Users_Flight_Relationship> findListByPri(Users_Flight_Relationship users_flight_relationship);


    List<Users_Flight_Relationship> adminFindLikeCode(Users_Flight_Relationship users_flight_relationship);
    List<Users_Flight_Relationship> adminFindBy(Users_Flight_Relationship users_flight_relationship);
    List<Users_Flight_Relationship> findAll();

    Users_Flight_Relationship findTiky(Users_Flight_Relationship users_flight_relationship);

    Users_Flight_Relationship findTikyNopri(Users_Flight_Relationship users_flight_relationship);

    void delete(Users_Flight_Relationship users_flight_relationship);
}

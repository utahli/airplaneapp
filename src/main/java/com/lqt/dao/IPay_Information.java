package com.lqt.dao;

import com.lqt.entity.Pay_Information;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
@Repository
public interface IPay_Information {
    void add(Pay_Information pay_information);

    List<Pay_Information> findListByAcc(Pay_Information pay_information);

    List<Pay_Information> adminFindByNotype(Pay_Information pay_information);
    List<Pay_Information> adminFindByWithtype(Pay_Information pay_information);

    List<Pay_Information> findAll();
}

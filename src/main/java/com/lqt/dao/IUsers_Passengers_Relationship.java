package com.lqt.dao;

import com.lqt.entity.Users_Passengers_Relationship;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/4/10.
 */
@Repository
public interface IUsers_Passengers_Relationship {

    void add(Users_Passengers_Relationship users_passengers_relationship);

    void del(Users_Passengers_Relationship users_passengers_relationship);

    void update(Users_Passengers_Relationship users_passengers_relationship);

    Users_Passengers_Relationship findOneByPri(Users_Passengers_Relationship users_passengers_relationship);

    List<Users_Passengers_Relationship> findAll();
}

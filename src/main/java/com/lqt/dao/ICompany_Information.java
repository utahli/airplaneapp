package com.lqt.dao;

import com.lqt.entity.Company_Information;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
@Repository
public interface ICompany_Information {
    void add(Company_Information company_information);

    void update(Company_Information company_information);

    Company_Information findOneByPri(Company_Information company_information);

    Company_Information findOneByCode(Company_Information company_information);

    List<Company_Information> findAll();
}

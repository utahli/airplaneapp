package com.lqt.dao;

import com.lqt.entity.Financial_Information;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
@Repository
public interface IFinancial_Information {
    void add(Financial_Information financial_information);

    void update(Financial_Information financial_information);

    Financial_Information findOndBypri(Financial_Information financial_information);

    List<Financial_Information> findListByPri(Financial_Information financial_information);

    List<Financial_Information> findListByCode(Financial_Information financial_information);

    List<Financial_Information> findAll();
}

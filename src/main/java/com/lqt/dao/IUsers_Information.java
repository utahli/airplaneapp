package com.lqt.dao;

import com.lqt.entity.Users_Information;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/4/10.
 */
@Repository
public interface IUsers_Information {

    void add(Users_Information users_information);

    void del(Users_Information users_information);

    void update(Users_Information users_information);

    void updateAcc(Users_Information users_information);

    Users_Information findOneByPri(Users_Information users_information);

    List<Users_Information> findAll();

}

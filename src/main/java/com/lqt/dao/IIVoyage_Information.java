package com.lqt.dao;

import com.lqt.entity.Voyage_Information;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Utah on 2017/5/7.
 */
@Repository
public interface IIVoyage_Information {
    void add(Voyage_Information voyage_information);
    void update(Voyage_Information voyage_information);
    List<Voyage_Information> findListByPri(Voyage_Information voyage_information);
    Voyage_Information findOneByNum(Voyage_Information voyage_information);

    List<Voyage_Information> findListLikeCode(Voyage_Information voyage_information);
    List<Voyage_Information> findListIf(Voyage_Information voyage_information);

}

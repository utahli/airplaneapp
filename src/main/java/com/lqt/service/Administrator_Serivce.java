package com.lqt.service;

import com.lqt.entity.Administrator_Imp;

import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
public interface Administrator_Serivce {
    void add(Administrator_Imp administratorImp);

    void update(Administrator_Imp administratorImp);

    void delete(Administrator_Imp administratorImp);

    List<Administrator_Imp> findAll();

    Administrator_Imp findByAcc(Administrator_Imp administratorImp);

    Administrator_Imp findByNum(Administrator_Imp administratorImp);

    List<Administrator_Imp> findByType(Administrator_Imp administratorImp);

    List<Administrator_Imp> adminFindBy(Administrator_Imp administratorImp);
}

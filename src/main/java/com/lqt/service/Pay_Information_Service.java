package com.lqt.service;

import com.lqt.entity.Pay_Information;

import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
public interface Pay_Information_Service {
    void add(Pay_Information pay_information) throws Exception;

    List<Pay_Information> findListByAcc(Pay_Information pay_information);
    List<Pay_Information> findAll();
    List<Pay_Information> adminFindByNotype(Pay_Information pay_information);
    List<Pay_Information> adminFindByWithtype(Pay_Information pay_information);

}

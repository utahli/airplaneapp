package com.lqt.service;

import com.lqt.entity.Company_Flight_Relationship;

import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
public interface Company_Flight_Relationship_Service {
    void add(Company_Flight_Relationship company_flight_relationship);
    Company_Flight_Relationship findOneByName(Company_Flight_Relationship company_flight_relationship);
    List<Company_Flight_Relationship> findAll();
    List<Company_Flight_Relationship> findListByPri(Company_Flight_Relationship company_flight_relationship);
    Company_Flight_Relationship findOneByNum(Company_Flight_Relationship company_flight_relationship);
}

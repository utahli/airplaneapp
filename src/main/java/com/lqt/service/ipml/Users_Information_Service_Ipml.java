package com.lqt.service.ipml;

import com.lqt.dao.IUsers_Information;
import com.lqt.entity.Users_Information;
import com.lqt.service.Users_Information_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Utah on 2017/4/10.
 */
@Service
public class Users_Information_Service_Ipml implements Users_Information_Service {

    @Resource
    private IUsers_Information dao;

    @Override
    public void add(Users_Information users_information) throws Exception{
         dao.add(users_information);
    }

    @Override
    public void updateAcc(Users_Information users_information) {
        dao.updateAcc(users_information);
    }

    @Override
    public Users_Information findOneByPri(Users_Information users_information){
        return dao.findOneByPri(users_information);
    }

    @Override
    public void update(Users_Information users_information) {
        dao.update(users_information);
    }
}

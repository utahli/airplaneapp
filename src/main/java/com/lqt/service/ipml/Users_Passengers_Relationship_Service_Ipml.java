package com.lqt.service.ipml;

import com.lqt.dao.IUsers_Passengers_Relationship;
import com.lqt.entity.Users_Passengers_Relationship;
import com.lqt.service.Users_Passengers_Relationship_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Utah on 2017/4/10.
 */
@Service
public class Users_Passengers_Relationship_Service_Ipml implements Users_Passengers_Relationship_Service{

    @Resource
    private IUsers_Passengers_Relationship dao;

    @Override
    public void add(Users_Passengers_Relationship users_passengers_relationship) throws Exception{

         dao.add(users_passengers_relationship);
    }

    @Override
    public Users_Passengers_Relationship findOneByPri(Users_Passengers_Relationship users_passengers_relationship){

        return dao.findOneByPri(users_passengers_relationship);
    }

    @Override
    public void update(Users_Passengers_Relationship users_passengers_relationship) {
        dao.update(users_passengers_relationship);
    }
}

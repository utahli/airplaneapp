package com.lqt.service.ipml;

import com.lqt.dao.IPay_Information;
import com.lqt.entity.Pay_Information;
import com.lqt.service.Pay_Information_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
@Service
public class Pay_Information_Service_Ipml implements Pay_Information_Service {

    @Resource
    private IPay_Information dao;

    @Override
    public void add(Pay_Information pay_information) throws Exception {
        dao.add(pay_information);
    }

    @Override
    public List<Pay_Information> findListByAcc(Pay_Information pay_information) {
        return dao.findListByAcc(pay_information);
    }

    @Override
    public List<Pay_Information> findAll() {
        return dao.findAll();
    }

    @Override
    public List<Pay_Information> adminFindByNotype(Pay_Information pay_information) {
        return dao.adminFindByNotype(pay_information);
    }

    @Override
    public List<Pay_Information> adminFindByWithtype(Pay_Information pay_information) {
        return dao.adminFindByWithtype(pay_information);
    }

}

package com.lqt.service.ipml;

import com.lqt.dao.IFinancial_Information;
import com.lqt.entity.Financial_Information;
import com.lqt.service.Financial_Information_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
@Service
public class Financial_Information_Service_Ipml implements Financial_Information_Service {

    @Resource
    private IFinancial_Information dao;

    @Override
    public void add(Financial_Information financial_information) throws Exception {
        dao.add(financial_information);
    }

    @Override
    public void update(Financial_Information financial_information) {
        dao.update(financial_information);
    }

    @Override
    public Financial_Information findOndBypri(Financial_Information financial_information) {
        return dao.findOndBypri(financial_information);
    }

    @Override
    public List<Financial_Information> findListByPri(Financial_Information financial_information) {
        return dao.findListByPri(financial_information);
    }

    @Override
    public List<Financial_Information> findAll() {
        return dao.findAll();
    }

    @Override
    public List<Financial_Information> findListByCode(Financial_Information financial_information) {
        return dao.findListByCode(financial_information);
    }
}

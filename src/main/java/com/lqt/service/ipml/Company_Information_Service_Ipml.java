package com.lqt.service.ipml;

import com.lqt.dao.ICompany_Information;
import com.lqt.entity.Company_Information;
import com.lqt.service.Company_Information_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
@Service
public class Company_Information_Service_Ipml implements Company_Information_Service {

    @Resource
    private ICompany_Information dao;

    @Override
    public void add(Company_Information company_information) throws Exception {
        dao.add(company_information);
    }

    @Override
    public void update(Company_Information company_information) {
        dao.update(company_information);
    }

    @Override
    public Company_Information findOneByPri(Company_Information company_information) {
        return dao.findOneByPri(company_information);
    }

    @Override
    public Company_Information findOneByCode(Company_Information company_information) {
        return dao.findOneByCode(company_information);
    }

    @Override
    public List<Company_Information> findAll() {
        return dao.findAll();
    }
}

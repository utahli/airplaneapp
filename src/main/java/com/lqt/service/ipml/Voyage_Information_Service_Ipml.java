package com.lqt.service.ipml;

import com.lqt.dao.IIVoyage_Information;
import com.lqt.entity.Voyage_Information;
import com.lqt.service.Voyage_Information_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/7.
 */
@Service
public class Voyage_Information_Service_Ipml implements Voyage_Information_Service{

    @Resource
    private IIVoyage_Information dao;

    @Override
    public void add(Voyage_Information voyage_information) throws Exception {
        dao.add(voyage_information);
    }

    @Override
    public void update(Voyage_Information voyage_information) {
        dao.update(voyage_information);
    }

    @Override
    public List<Voyage_Information> findListByPri(Voyage_Information voyage_information) {
        return dao.findListByPri(voyage_information);
    }

    @Override
    public Voyage_Information findOneByNum(Voyage_Information voyage_information) {
        return dao.findOneByNum(voyage_information);
    }

    @Override
    public List<Voyage_Information> findListLikeCode(Voyage_Information voyage_information) {
        return dao.findListLikeCode(voyage_information);
    }

    @Override
    public List<Voyage_Information> findListIf(Voyage_Information voyage_information) {
        return dao.findListIf(voyage_information);
    }
}

package com.lqt.service.ipml;

import com.lqt.dao.IAdministrator_Imp;
import com.lqt.entity.Administrator_Imp;
import com.lqt.service.Administrator_Serivce;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
@Service
public class Administrator_Service_Ipml implements Administrator_Serivce {

    @Resource
    private IAdministrator_Imp dao;

    @Override
    public void add(Administrator_Imp administratorImp) {
        dao.add(administratorImp);
    }

    @Override
    public void update(Administrator_Imp administratorImp) {
        dao.update(administratorImp);
    }

    @Override
    public void delete(Administrator_Imp administratorImp) {
        dao.delete(administratorImp);
    }

    @Override
    public List<Administrator_Imp> findAll() {
        return dao.findAll();
    }

    @Override
    public Administrator_Imp findByAcc(Administrator_Imp administratorImp) {
        return dao.findByAcc(administratorImp);
    }

    @Override
    public Administrator_Imp findByNum(Administrator_Imp administratorImp) {
        return dao.findByNum(administratorImp);
    }

    @Override
    public List<Administrator_Imp> findByType(Administrator_Imp administratorImp) {
        return dao.findByType(administratorImp);
    }

    @Override
    public List<Administrator_Imp> adminFindBy(Administrator_Imp administratorImp) {
        return dao.adminFindBy(administratorImp);
    }
}

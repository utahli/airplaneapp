package com.lqt.service.ipml;

import com.lqt.dao.IUsers_Flight_Relationship;
import com.lqt.entity.Users_Flight_Relationship;
import com.lqt.service.Users_Flight_Relationship_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/11.
 */
@Service
public class Users_Flight_Relationship_Service_Ipml implements Users_Flight_Relationship_Service {

    @Resource
    private IUsers_Flight_Relationship dao;

    @Override
    public void add(Users_Flight_Relationship users_flight_relationship) throws Exception{
        dao.add(users_flight_relationship);
    }

    @Override
    public void update(Users_Flight_Relationship users_flight_relationship) {
        dao.update(users_flight_relationship);
    }

    @Override
    public void updateunpay(Users_Flight_Relationship users_flight_relationship) {
        dao.updateunpay(users_flight_relationship);
    }

    @Override
    public void updatepaied(Users_Flight_Relationship users_flight_relationship) {
        dao.updatepaied(users_flight_relationship);
    }


    @Override
    public List<Users_Flight_Relationship> findListByPri(Users_Flight_Relationship users_flight_relationship) {
        return dao.findListByPri(users_flight_relationship);
    }

    @Override
    public Users_Flight_Relationship findTiky(Users_Flight_Relationship users_flight_relationship) {
        return dao.findTiky(users_flight_relationship);
    }

    @Override
    public Users_Flight_Relationship findTikyNopri(Users_Flight_Relationship users_flight_relationship) {
        return dao.findTikyNopri(users_flight_relationship);
    }

    @Override
    public void delete(Users_Flight_Relationship users_flight_relationship) {
        dao.delete(users_flight_relationship);
    }

    @Override
    public List<Users_Flight_Relationship> findAll() {
        return dao.findAll();
    }

    @Override
    public List<Users_Flight_Relationship> adminFindLikeCode(Users_Flight_Relationship users_flight_relationship) {
        return dao.adminFindLikeCode(users_flight_relationship);
    }

    @Override
    public List<Users_Flight_Relationship> adminFindBy(Users_Flight_Relationship users_flight_relationship) {
        return dao.adminFindBy(users_flight_relationship);
    }
}

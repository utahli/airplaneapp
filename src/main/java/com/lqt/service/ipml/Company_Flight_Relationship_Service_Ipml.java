package com.lqt.service.ipml;

import com.lqt.dao.ICompany_Flight_Relationship;
import com.lqt.entity.Company_Flight_Relationship;
import com.lqt.service.Company_Flight_Relationship_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
@Service
public class Company_Flight_Relationship_Service_Ipml implements Company_Flight_Relationship_Service {

    @Resource
    private ICompany_Flight_Relationship dao;

    @Override
    public void add(Company_Flight_Relationship company_flight_relationship) {
        dao.add(company_flight_relationship);
    }

    @Override
    public Company_Flight_Relationship findOneByName(Company_Flight_Relationship company_flight_relationship) {
        return dao.findOneByName(company_flight_relationship);
    }

    @Override
    public List<Company_Flight_Relationship> findAll() {
        return dao.findAll();
    }


    @Override
    public List<Company_Flight_Relationship> findListByPri(Company_Flight_Relationship company_flight_relationship) {
        return dao.findListByPri(company_flight_relationship);
    }

    @Override
    public Company_Flight_Relationship findOneByNum(Company_Flight_Relationship company_flight_relationship) {
        return dao.findOneByNum(company_flight_relationship);
    }
}

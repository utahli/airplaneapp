package com.lqt.service.ipml;

import com.lqt.dao.IPassengers_Pnformation;
import com.lqt.entity.Passengers_Pnformation;
import com.lqt.service.Passengers_Pnformation_Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Utah on 2017/4/10.
 */
@Service
public class Passengers_Pnformation_Service_Ipml implements Passengers_Pnformation_Service{

    @Resource
    private IPassengers_Pnformation dao;

    @Override
    public void add(Passengers_Pnformation passengers_pnformation) throws Exception{
        dao.add(passengers_pnformation);
    }

    @Override
    public Passengers_Pnformation findOneByPri(Passengers_Pnformation passengers_pnformation){
        return dao.findOneByPri(passengers_pnformation);
    }

    @Override
    public List<Passengers_Pnformation> check(Passengers_Pnformation passengers_pnformation) {
        return dao.check(passengers_pnformation);
    }

    @Override
    public List<Passengers_Pnformation> updateCheck(Passengers_Pnformation passengers_pnformation) {
        return dao.updateCheck(passengers_pnformation);
    }

    @Override
    public void update(Passengers_Pnformation passengers_pnformation) {
        dao.update(passengers_pnformation);
    }

    @Override
    public List<Passengers_Pnformation> findAll() {
        return dao.findAll();
    }

    @Override
    public List<Passengers_Pnformation> adminfindBy(Passengers_Pnformation passengers_pnformation) {
        return dao.adminfindBy(passengers_pnformation);
    }
}

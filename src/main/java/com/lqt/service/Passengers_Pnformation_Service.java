package com.lqt.service;

import com.lqt.entity.Passengers_Pnformation;

import java.util.List;

/**
 * Created by Utah on 2017/4/10.
 */
public interface Passengers_Pnformation_Service {

    void add(Passengers_Pnformation passengers_pnformation) throws Exception;

    Passengers_Pnformation findOneByPri(Passengers_Pnformation passengers_pnformation);

    List<Passengers_Pnformation> check(Passengers_Pnformation passengers_pnformation);
    List<Passengers_Pnformation> updateCheck(Passengers_Pnformation passengers_pnformation);

    void update(Passengers_Pnformation passengers_pnformation);

    List<Passengers_Pnformation> findAll();

    List<Passengers_Pnformation> adminfindBy(Passengers_Pnformation passengers_pnformation);
}

package com.lqt.service;

import com.lqt.entity.Voyage_Information;

import java.util.List;

/**
 * Created by Utah on 2017/5/7.
 */
public interface Voyage_Information_Service {
    void add(Voyage_Information voyage_information) throws Exception;
    void update(Voyage_Information voyage_information);
    List<Voyage_Information> findListByPri(Voyage_Information voyage_information);
    Voyage_Information findOneByNum(Voyage_Information voyage_information);
    List<Voyage_Information> findListLikeCode(Voyage_Information voyage_information);
    List<Voyage_Information> findListIf(Voyage_Information voyage_information);
}

package com.lqt.service;

import com.lqt.entity.Financial_Information;

import java.util.List;

/**
 * Created by Utah on 2017/5/12.
 */
public interface Financial_Information_Service {
    void add(Financial_Information financial_information) throws Exception;

    void update(Financial_Information financial_information);

    Financial_Information findOndBypri(Financial_Information financial_information);

    List<Financial_Information> findListByPri(Financial_Information financial_information);

    List<Financial_Information> findAll();
    List<Financial_Information> findListByCode(Financial_Information financial_information);
}

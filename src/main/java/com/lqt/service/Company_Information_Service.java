package com.lqt.service;

import com.lqt.entity.Company_Information;

import java.util.List;

/**
 * Created by Utah on 2017/5/16.
 */
public interface Company_Information_Service {
    void add(Company_Information company_information) throws Exception;

    void update(Company_Information company_information);

    Company_Information findOneByPri(Company_Information company_information);

    Company_Information findOneByCode(Company_Information company_information);

    List<Company_Information> findAll();
}

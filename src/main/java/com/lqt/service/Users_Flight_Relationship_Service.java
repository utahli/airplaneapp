package com.lqt.service;

import com.lqt.entity.Users_Flight_Relationship;

import java.util.List;

/**
 * Created by Utah on 2017/5/11.
 */
public interface Users_Flight_Relationship_Service {
    void add(Users_Flight_Relationship users_flight_relationship) throws Exception;
    void update(Users_Flight_Relationship users_flight_relationship);
    void updateunpay(Users_Flight_Relationship users_flight_relationship);

    void updatepaied(Users_Flight_Relationship users_flight_relationship);
    List<Users_Flight_Relationship> findListByPri(Users_Flight_Relationship users_flight_relationship);
    Users_Flight_Relationship findTiky(Users_Flight_Relationship users_flight_relationship);
    Users_Flight_Relationship findTikyNopri(Users_Flight_Relationship users_flight_relationship);
    void delete(Users_Flight_Relationship users_flight_relationship);
    List<Users_Flight_Relationship> findAll();
    List<Users_Flight_Relationship> adminFindLikeCode(Users_Flight_Relationship users_flight_relationship);
    List<Users_Flight_Relationship> adminFindBy(Users_Flight_Relationship users_flight_relationship);
}

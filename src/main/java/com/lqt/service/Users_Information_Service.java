package com.lqt.service;

import com.lqt.entity.Users_Information;

/**
 * Created by Utah on 2017/4/10.
 */
public interface Users_Information_Service {

    void add(Users_Information users_information) throws Exception;
    void updateAcc(Users_Information users_information);
    Users_Information findOneByPri(Users_Information users_information);
    void update(Users_Information users_information);
}

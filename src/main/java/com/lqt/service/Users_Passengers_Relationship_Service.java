package com.lqt.service;

import com.lqt.entity.Users_Passengers_Relationship;

/**
 * Created by Utah on 2017/4/10.
 */
public interface Users_Passengers_Relationship_Service {

    void add(Users_Passengers_Relationship users_passengers_relationship) throws Exception;

    Users_Passengers_Relationship findOneByPri(Users_Passengers_Relationship users_passengers_relationship);
    void update(Users_Passengers_Relationship users_passengers_relationship);
}

package com.lqt.entity;

/**
 * Created by Utah on 2017/5/24.
 */
public class PayList {
    private String name;
    private Pay_Information pi;

    @Override
    public String toString() {
        return "PayList{" +
                "name='" + name + '\'' +
                ", pi=" + pi +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pay_Information getPi() {
        return pi;
    }

    public void setPi(Pay_Information pi) {
        this.pi = pi;
    }
}

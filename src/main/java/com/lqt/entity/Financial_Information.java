package com.lqt.entity;

import java.sql.Date;

/**
 * Created by Utah on 2017/5/12.
 */
public class Financial_Information {
    private Date date;
    private String flightNumber;
    private float salesVolume;
    private int salesIntegral;

    @Override
    public String toString() {
        return "Financial_Information{" +
                "date=" + date +
                ", flightNumber='" + flightNumber + '\'' +
                ", salesVolume=" + salesVolume +
                ", salesIntegral=" + salesIntegral +
                '}';
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public float getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(float salesVolume) {
        this.salesVolume = salesVolume;
    }

    public int getSalesIntegral() {
        return salesIntegral;
    }

    public void setSalesIntegral(int salesIntegral) {
        this.salesIntegral = salesIntegral;
    }
}

package com.lqt.entity;

/**
 * Created by Utah on 2017/4/10.
 * 用户基本信息表
 */
public class Users_Information {
    private String usersAccount;//帐号(手机/邮箱)
    private String usersPassword;//密码
    private int integral;//积分

    public String getUsersPassword() {
        return usersPassword;
    }

    public void setUsersPassword(String usersPassword) {
        this.usersPassword = usersPassword;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public String getUsersAccount() {

        return usersAccount;
    }

    public void setUsersAccount(String usersAccount) {
        this.usersAccount = usersAccount;
    }

    @Override
    public String toString() {
        return "Users_Information{" +
                "usersAccount='" + usersAccount + '\'' +
                ", usersPassword='" + usersPassword + '\'' +
                ", integral='" + integral + '\'' +
                '}';
    }
}


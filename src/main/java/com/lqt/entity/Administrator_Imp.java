package com.lqt.entity;

/**
 * Created by Utah on 2017/5/16.
 */
public class Administrator_Imp {
    private String administratorAccount;
    private String administratorPassword;
    private String administratorNumber;
    private String administratorName;
    private String administratorType;

    @Override
    public String toString() {
        return "Administrator_Imp{" +
                "administratorAccount='" + administratorAccount + '\'' +
                ", administratorPassword='" + administratorPassword + '\'' +
                ", administratorNumber='" + administratorNumber + '\'' +
                ", administratorName='" + administratorName + '\'' +
                ", administratorType='" + administratorType + '\'' +
                '}';
    }

    public String getAdministratorAccount() {
        return administratorAccount;
    }

    public void setAdministratorAccount(String administratorAccount) {
        this.administratorAccount = administratorAccount;
    }

    public String getAdministratorPassword() {
        return administratorPassword;
    }

    public void setAdministratorPassword(String administratorPassword) {
        this.administratorPassword = administratorPassword;
    }

    public String getAdministratorNumber() {
        return administratorNumber;
    }

    public void setAdministratorNumber(String administratorNumber) {
        this.administratorNumber = administratorNumber;
    }

    public String getAdministratorName() {
        return administratorName;
    }

    public void setAdministratorName(String administratorName) {
        this.administratorName = administratorName;
    }

    public String getAdministratorType() {
        return administratorType;
    }

    public void setAdministratorType(String administratorType) {
        this.administratorType = administratorType;
    }
}

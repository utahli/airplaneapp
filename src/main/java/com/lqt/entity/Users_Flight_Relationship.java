package com.lqt.entity;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Utah on 2017/5/11.
 */
public class Users_Flight_Relationship {
    private Timestamp bookingTime;
    private String usersAccount;
    private String idNumber;
    private String flightNumber;
    private Timestamp startTime;
    private Timestamp dandTime;
    private String startPlace;
    private String landPlace;
    private String seatType;
    private String ticketType;
    private int seatNumber;
    private float money;
    private int integral;
    private int ifBuy;
    private String statAirport;
    private String landAirport;

    @Override
    public String toString() {
        return "Users_Flight_Relationship{" +
                "bookingTime=" + bookingTime +
                ", usersAccount='" + usersAccount + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", startTime=" + startTime +
                ", dandTime=" + dandTime +
                ", startPlace='" + startPlace + '\'' +
                ", landPlace='" + landPlace + '\'' +
                ", seatType='" + seatType + '\'' +
                ", ticketType='" + ticketType + '\'' +
                ", seatNumber=" + seatNumber +
                ", money=" + money +
                ", integral=" + integral +
                ", ifBuy=" + ifBuy +
                ", statAirport='" + statAirport + '\'' +
                ", landAirport='" + landAirport + '\'' +
                '}';
    }

    public String getStatAirport() {
        return statAirport;
    }

    public void setStatAirport(String statAirport) {
        this.statAirport = statAirport;
    }

    public String getLandAirport() {
        return landAirport;
    }

    public void setLandAirport(String landAirport) {
        this.landAirport = landAirport;
    }

    public Date getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(Timestamp bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getUsersAccount() {
        return usersAccount;
    }

    public void setUsersAccount(String usersAccount) {
        this.usersAccount = usersAccount;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getDandTime() {
        return dandTime;
    }

    public void setDandTime(Timestamp dandTime) {
        this.dandTime = dandTime;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getLandPlace() {
        return landPlace;
    }

    public void setLandPlace(String landPlace) {
        this.landPlace = landPlace;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public int getIfBuy() {
        return ifBuy;
    }

    public void setIfBuy(int ifBuy) {
        this.ifBuy = ifBuy;
    }
}

package com.lqt.entity;

/**
 * Created by Utah on 2017/5/16.
 */
public class Company_Information {
    private String companyName;
    private String companyInformation;
    private String companyCode;
    private String address;
    private String website;
    private String telephone;

    @Override
    public String toString() {
        return "Company_Information{" +
                "companyName='" + companyName + '\'' +
                ", companyInformation='" + companyInformation + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", address='" + address + '\'' +
                ", website='" + website + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyInformation() {
        return companyInformation;
    }

    public void setCompanyInformation(String companyInformation) {
        this.companyInformation = companyInformation;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}

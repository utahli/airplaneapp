package com.lqt.entity;

/**
 * Created by Utah on 2017/4/10.
 * 乘客信息表
 */
public class Passengers_Pnformation {
    private String idNumber;//身份证号
    private String idType;//证件类型
    private String passengersName;//姓名
    private String passengersPhone;//手机号
    private String passengersEmail;//邮箱
    private int ifUsers;//是否为用户？

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getPassengersName() {
        return passengersName;
    }

    public void setPassengersName(String passengersName) {
        this.passengersName = passengersName;
    }

    public String getPassengersPhone() {
        return passengersPhone;
    }

    public void setPassengersPhone(String passengersPhone) {
        this.passengersPhone = passengersPhone;
    }

    public String getPassengersEmail() {
        return passengersEmail;
    }

    public void setPassengersEmail(String passengersEmail) {
        this.passengersEmail = passengersEmail;
    }

    public int getIfUsers() {
        return ifUsers;
    }

    public void setIfUsers(int ifUsers) {
        this.ifUsers = ifUsers;
    }

    @Override
    public String toString() {
        return "Passengers_Pnformation{" +
                "idNumber='" + idNumber + '\'' +
                ", idType='" + idType + '\'' +
                ", passengersName='" + passengersName + '\'' +
                ", passengersPhone='" + passengersPhone + '\'' +
                ", passengersEmail='" + passengersEmail + '\'' +
                ", ifUsers=" + ifUsers +
                '}';
    }
}

package com.lqt.entity;

/**
 * Created by Utah on 2017/5/16.
 */
public class AdminFlightManeger {
    private String companyName;
    private Voyage_Information flight;

    @Override
    public String toString() {
        return "AdminFlightManeger{" +
                "companyName='" + companyName + '\'' +
                ", flight=" + flight +
                '}';
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Voyage_Information getFlight() {
        return flight;
    }

    public void setFlight(Voyage_Information flight) {
        this.flight = flight;
    }
}

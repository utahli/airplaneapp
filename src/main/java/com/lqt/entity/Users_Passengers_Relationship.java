package com.lqt.entity;

/**
 * Created by Utah on 2017/4/10.
 *用户乘客关系表
 */
public class Users_Passengers_Relationship {
    private String usersAccount;//帐号（手机/邮箱）
    private String idNumber;//身份证号

    public Users_Passengers_Relationship() {
    }

    public Users_Passengers_Relationship(String usersAccount, String idNumber) {
        this.usersAccount = usersAccount;
        this.idNumber = idNumber;
    }

    public String getUsersAccount() {
        return usersAccount;
    }

    public void setUsersAccount(String usersAccount) {
        this.usersAccount = usersAccount;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public String toString() {
        return "Users_Passengers_Relationship{" +
                "usersAccount='" + usersAccount + '\'' +
                ", idNumber='" + idNumber + '\'' +
                '}';
    }
}

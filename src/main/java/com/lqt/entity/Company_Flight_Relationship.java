package com.lqt.entity;

/**
 * Created by Utah on 2017/5/16.
 */
public class Company_Flight_Relationship {
    private String companyName;
    private String flightNumber;

    @Override
    public String toString() {
        return "Company_Flight_Relationship{" +
                "companyName='" + companyName + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                '}';
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}

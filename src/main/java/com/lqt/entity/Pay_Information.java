package com.lqt.entity;

import java.sql.Timestamp;

/**
 * Created by Utah on 2017/5/12.
 */
public class Pay_Information {
    private Timestamp payTime;
    private String usersAccount;
    private String payType;
    private String payNumbers;
    private float payMoney;

    @Override
    public String toString() {
        return "Pay_Information{" +
                "payTime=" + payTime +
                ", usersAccount='" + usersAccount + '\'' +
                ", payType='" + payType + '\'' +
                ", payNumbers='" + payNumbers + '\'' +
                ", payMoney=" + payMoney +
                '}';
    }

    public Timestamp getPayTime() {
        return payTime;
    }

    public void setPayTime(Timestamp payTime) {
        this.payTime = payTime;
    }

    public String getUsersAccount() {
        return usersAccount;
    }

    public void setUsersAccount(String usersAccount) {
        this.usersAccount = usersAccount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayNumbers() {
        return payNumbers;
    }

    public void setPayNumbers(String payNumbers) {
        this.payNumbers = payNumbers;
    }

    public float getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(float payMoney) {
        this.payMoney = payMoney;
    }
}

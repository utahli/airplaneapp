package com.lqt.entity;


import java.sql.Timestamp;

/**
 * Created by Utah on 2017/5/5.
 */
public class Voyage_Information {
    private String flightNumber;//航班号
    private String voyageNumber;//航次编号
    private Timestamp startTime;//起飞时间
    private Timestamp landTime;//降落时间
    private String startPlace;//起飞地点
    private String landPlace;//降落地点
    private int economicNumber;//经济舱数
    private int businessNumber;//商务舱数
    private float economicPrice;//经济舱票价
    private float businessPrice;//商务舱票价
    private int waitTime;//经停时间（分钟）
    private String statAirport;//起飞机场
    private String landAirport;//降落机场
    private String flightType;
    private int discountRate;

    @Override
    public String toString() {
        return "Voyage_Information{" +
                "flightNumber='" + flightNumber + '\'' +
                ", voyageNumber='" + voyageNumber + '\'' +
                ", startTime=" + startTime +
                ", landTime=" + landTime +
                ", startPlace='" + startPlace + '\'' +
                ", landPlace='" + landPlace + '\'' +
                ", economicNumber=" + economicNumber +
                ", businessNumber=" + businessNumber +
                ", economicPrice=" + economicPrice +
                ", businessPrice=" + businessPrice +
                ", waitTime=" + waitTime +
                ", statAirport='" + statAirport + '\'' +
                ", landAirport='" + landAirport + '\'' +
                ", flightType='" + flightType + '\'' +
                ", discountRate=" + discountRate +
                '}';
    }

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public int getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(int discountRate) {
        this.discountRate = discountRate;
    }

    public String getStatAirport() {
        return statAirport;
    }

    public void setStatAirport(String statAirport) {
        this.statAirport = statAirport;
    }

    public String getLandAirport() {
        return landAirport;
    }

    public void setLandAirport(String landAirport) {
        this.landAirport = landAirport;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getVoyageNumber() {
        return voyageNumber;
    }

    public void setVoyageNumber(String voyageNumber) {
        this.voyageNumber = voyageNumber;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getLandTime() {
        return landTime;
    }

    public void setLandTime(Timestamp landTime) {
        this.landTime = landTime;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getLandPlace() {
        return landPlace;
    }

    public void setLandPlace(String landPlace) {
        this.landPlace = landPlace;
    }

    public int getEconomicNumber() {
        return economicNumber;
    }

    public void setEconomicNumber(int economicNumber) {
        this.economicNumber = economicNumber;
    }

    public int getBusinessNumber() {
        return businessNumber;
    }

    public void setBusinessNumber(int businessNumber) {
        this.businessNumber = businessNumber;
    }

    public float getEconomicPrice() {
        return economicPrice;
    }

    public void setEconomicPrice(float economicPrice) {
        this.economicPrice = economicPrice;
    }

    public float getBusinessPrice() {
        return businessPrice;
    }

    public void setBusinessPrice(float businessPrice) {
        this.businessPrice = businessPrice;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }


}

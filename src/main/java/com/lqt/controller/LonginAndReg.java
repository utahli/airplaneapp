package com.lqt.controller;

import com.lqt.common.ResponseObj;
import com.lqt.entity.Administrator_Imp;
import com.lqt.entity.Passengers_Pnformation;
import com.lqt.entity.Users_Information;
import com.lqt.entity.Users_Passengers_Relationship;
import com.lqt.service.ipml.Administrator_Service_Ipml;
import com.lqt.service.ipml.Passengers_Pnformation_Service_Ipml;
import com.lqt.service.ipml.Users_Information_Service_Ipml;
import com.lqt.service.ipml.Users_Passengers_Relationship_Service_Ipml;
import com.lqt.utils.GsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by Utah on 2017/4/10.
 */
@Controller
@RequestMapping("/logAndReg")
@SessionAttributes({"user","userid","useracc","adname","adtype","adacc","chajia"})
public class LonginAndReg {

    @Resource
    private Users_Information_Service_Ipml uInfoService;

    @Resource
    private Users_Passengers_Relationship_Service_Ipml uPassRsService;

    @Resource
    private Passengers_Pnformation_Service_Ipml passPnfService;

    @Resource
    private Administrator_Service_Ipml adminService;

    private ResponseObj responseObj;    //返回json数据的实体


    @ResponseBody
    @RequestMapping(value = "adminchangpwd", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adminchangpwd(@RequestBody Map<String,String> map,HttpSession httpSession) {
        Object result;
        responseObj = new ResponseObj<>();

        Administrator_Imp admin = new Administrator_Imp();
        admin.setAdministratorAccount((String) httpSession.getAttribute("adacc"));
        Administrator_Imp findBy = adminService.findByAcc(admin);
        if (findBy.getAdministratorPassword().equals(map.get("oldpwd"))) {
            findBy.setAdministratorPassword(map.get("newpwd"));
            adminService.update(findBy);

            responseObj.setCode(ResponseObj.OK);
            responseObj.setMsg("密码修改成功！");
            result = new GsonUtils().toJson(responseObj);
        } else {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("旧密码错误！");
            result = new GsonUtils().toJson(responseObj);
        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "userupdate", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object userupdate(@RequestBody Map<String,String> map,HttpSession httpSession) {
        Object result;
        responseObj = new ResponseObj<>();
        System.out.println("userupdate==="+map.toString());

        Administrator_Imp admin = new Administrator_Imp();
        admin.setAdministratorAccount(map.get("useracc"));
        Administrator_Imp findBy = adminService.findByAcc(admin);

        if (!map.get("userpwd").equals("")) {
            admin.setAdministratorPassword(map.get("userpwd"));
            admin.setAdministratorNumber(map.get("usercode"));
            admin.setAdministratorName(map.get("username"));
            admin.setAdministratorType(map.get("usertype"));

            if (findBy != null) {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("该管理员账号已存在！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
            try {
                adminService.add(admin);
            } catch (Exception e) {
                e.printStackTrace();
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("管理员添加失败！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
            responseObj.setCode(ResponseObj.OK);
            responseObj.setMsg("管理员添加成功！");
            responseObj.setNextUrl("touser");
            result = new GsonUtils().toJson(responseObj);

        } else {

            findBy.setAdministratorNumber(map.get("usercode"));
            findBy.setAdministratorName(map.get("username"));
            findBy.setAdministratorType(map.get("usertype"));

            try {
                adminService.update(findBy);
            } catch (Exception e) {
                e.printStackTrace();
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("管理员信息修改失败！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
            responseObj.setCode(ResponseObj.OK);
            responseObj.setMsg("管理员信息修改成功！");
            responseObj.setNextUrl("touser");
            result = new GsonUtils().toJson(responseObj);
        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "changpwd", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object changpwd(@RequestBody Map<String,String> map,HttpSession httpSession) {
        Object result;
        responseObj = new ResponseObj<>();

        Users_Information ui = new Users_Information();
        ui.setUsersAccount((String) httpSession.getAttribute("useracc"));
        Users_Information findBy = uInfoService.findOneByPri(ui);
        if (findBy.getUsersPassword().equals(map.get("oldpwd"))) {
            findBy.setUsersPassword(map.get("newpwd"));
            uInfoService.update(findBy);

            responseObj.setCode(ResponseObj.OK);
            responseObj.setMsg("密码修改成功！");
            result = new GsonUtils().toJson(responseObj);
        } else {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("旧密码错误！");
            result = new GsonUtils().toJson(responseObj);
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "forget", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object forget(@RequestBody Map<String,String> map) {
        Object result;
        responseObj = new ResponseObj<>();

        Users_Information ui = new Users_Information();
        if (map.get("acc").contains("@")) {
            ui.setUsersAccount("%&" + map.get("acc"));
        } else {
            ui.setUsersAccount(map.get("acc") + "&%");
        }
        ui.setUsersPassword(map.get("pwd"));

        try {
            uInfoService.update(ui);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("密码修改失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("密码修改成功！是否立即登录？");
        responseObj.setNextUrl("tologin");
        result = new GsonUtils().toJson(responseObj);

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "forgetyz", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object forgetyz(@RequestBody Map<String,String> map) {
        Object result;
        responseObj = new ResponseObj<>();

        String msg ;
        Users_Information ui = new Users_Information();
        if (map.get("acc").contains("@")) {
            ui.setUsersAccount("%&" + map.get("acc"));
            msg ="验证码“111”已发送到你的邮箱，请注意查收。";
        } else {
            ui.setUsersAccount(map.get("acc") + "&%");
            msg ="验证码“111”已发送到你的手机，请注意查收。";
        }

        Users_Information findBy = uInfoService.findOneByPri(ui);
        if (findBy == null) {
            responseObj.setCode(ResponseObj.EMPUTY);
            responseObj.setMsg("该用户不存在！");
            result = new GsonUtils().toJson(responseObj);
        } else {
            responseObj.setCode(ResponseObj.OK);
            responseObj.setMsg(msg);
            result = new GsonUtils().toJson(responseObj);
        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "adminlogin", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object admin(@RequestBody Map<String,String> map, Model model) {
        Object result;
        responseObj = new ResponseObj<>();

        Administrator_Imp ad = new Administrator_Imp();
        ad.setAdministratorAccount(map.get("administratorAccount"));
        ad.setAdministratorPassword(map.get("administratorPassword"));

        Administrator_Imp findBy = adminService.findByAcc(ad);

        if (null == findBy) {
            responseObj.setCode(ResponseObj.EMPUTY);
            responseObj.setMsg("该用户不存在");
            result = new GsonUtils().toJson(responseObj);
        } else {
            if (ad.getAdministratorPassword().equals(findBy.getAdministratorPassword())) {
                model.addAttribute("adacc", findBy.getAdministratorAccount());
                model.addAttribute("adname", findBy.getAdministratorName());
                model.addAttribute("adtype", findBy.getAdministratorType());

                responseObj.setCode(ResponseObj.OK);
                responseObj.setMsg(ResponseObj.OK_STR);

                if (findBy.getAdministratorType().equals("user")) {
                    responseObj.setNextUrl("touser");

                } else if (findBy.getAdministratorType().equals("finance")) {
                    responseObj.setNextUrl("tofinance");

                } else {
                    responseObj.setNextUrl("toaircraft");
                }

                result = new GsonUtils().toJson(responseObj);
            } else {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("密码错误！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "editpass", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object edit(@RequestBody Map<String,String> map,Model model) {
        Object result;

        System.out.println(map.toString());

        responseObj = new ResponseObj<Passengers_Pnformation>();

        Passengers_Pnformation pp = new Passengers_Pnformation();
        pp.setIdNumber(map.get("csfz"));
        pp.setPassengersPhone(map.get("csj"));
        pp.setPassengersEmail(map.get("cyx"));

        String phone_mail = map.get("call") + "&" + map.get("mail");

        if (!map.get("call").equals(map.get("csj")) || !map.get("mail").equals(map.get("cyx"))) {

            List<Passengers_Pnformation> findBy = passPnfService.updateCheck(pp);
            if (!findBy.isEmpty()) {
                for (Passengers_Pnformation enty : findBy) {
                    if (enty.getPassengersPhone().equals(map.get("csj"))) {
                        responseObj.setCode(ResponseObj.FAILED);
                        responseObj.setMsg("该手机已经注册！");
                        result = new GsonUtils().toJson(responseObj);
                        return result;
                    }
                    if (enty.getPassengersEmail().equals(map.get("cyx"))) {
                        responseObj.setCode(ResponseObj.FAILED);
                        responseObj.setMsg("该邮箱已经注册！");
                        result = new GsonUtils().toJson(responseObj);
                        return result;
                    }
                }
            }

            Users_Information ui = new Users_Information();

            ui.setUsersAccount(map.get("csj") + "&" + map.get("cyx"));
            ui.setUsersPassword(phone_mail);
            System.out.println(ui.toString());
            System.out.println(pp.toString());
            /*uInfoService.updateAcc(ui);
            passPnfService.update(pp);*/
        } else {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("个人信息未修改，请勿提交！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("修改成功！");
        result = new GsonUtils().toJson(responseObj);

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "regger", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object reg(@RequestBody Map<String,String> map,Model model) {
        Object result;
        Passengers_Pnformation pp = new Passengers_Pnformation();
        Users_Information ui = new Users_Information();
        Users_Passengers_Relationship upr = new Users_Passengers_Relationship();

        pp.setIdNumber(map.get("idNumber"));
        pp.setIdType(map.get("idType"));
        pp.setPassengersName(map.get("passengersName"));
        pp.setPassengersPhone(map.get("passengersPhone"));
        pp.setPassengersEmail(map.get("passengersEmail"));
        pp.setIfUsers(1);

        String phone_mail = map.get("passengersPhone") + "&" + map.get("passengersEmail");

        ui.setUsersAccount(phone_mail);
        ui.setUsersPassword(map.get("usersPassword"));
        ui.setIntegral(0);

        upr.setUsersAccount(phone_mail);
        upr.setIdNumber(map.get("idNumber"));

        responseObj = new ResponseObj<Passengers_Pnformation>();

        List<Passengers_Pnformation> ppcheck = passPnfService.check(pp);
        if (!ppcheck.isEmpty()) {
            for (Passengers_Pnformation enty : ppcheck) {

                if (pp.getIdNumber().equals(enty.getIdNumber())) {
                    responseObj.setCode(ResponseObj.FAILED);
                    responseObj.setMsg("用户已经存在！");
                    result = new GsonUtils().toJson(responseObj);
                    return result;
                }
                if (pp.getPassengersPhone().equals(enty.getPassengersPhone()) ) {
                    responseObj.setCode(ResponseObj.FAILED);
                    responseObj.setMsg("该手机已经注册！");
                    result = new GsonUtils().toJson(responseObj);
                    return result;
                }
                if (pp.getPassengersEmail().equals(enty.getPassengersEmail())) {
                    responseObj.setCode(ResponseObj.FAILED);
                    responseObj.setMsg("该邮箱已经注册！");
                    result = new GsonUtils().toJson(responseObj);
                    return result;
                }
            }
        }

        try {
            passPnfService.add(pp);
            uInfoService.add(ui);
            uPassRsService.add(upr);
            model.addAttribute("user", pp.getPassengersName());
            model.addAttribute("user", pp.getPassengersName());
            model.addAttribute("userid", pp.getIdNumber());
            model.addAttribute("useracc", ui.getUsersAccount());
            model.addAttribute("chajia",0);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("注册失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("注册成功,是否跳转到系统首页？");
        responseObj.setNextUrl("tohome");
        result = new GsonUtils().toJson(responseObj);

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "login", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object login(@RequestBody Users_Information ui, Model model) {
        Object result;

        String aconnt = ui.getUsersAccount();
        if (aconnt.contains("@")) {
            ui.setUsersAccount("%&" + aconnt);
        } else {
            ui.setUsersAccount(aconnt + "&%");
        }
        responseObj = new ResponseObj<Users_Information>();
        Users_Information findBy = uInfoService.findOneByPri(ui);

        if (null == findBy) {
            responseObj.setCode(ResponseObj.EMPUTY);
            responseObj.setMsg("该用户不存在！");
            result = new GsonUtils().toJson(responseObj);
        } else {
            if (ui.getUsersPassword().equals(findBy.getUsersPassword())) {

                Users_Passengers_Relationship uprr = new Users_Passengers_Relationship();
                uprr.setUsersAccount(findBy.getUsersAccount());
                Users_Passengers_Relationship upr = uPassRsService.findOneByPri(uprr);

                Passengers_Pnformation pp = new Passengers_Pnformation();
                pp.setIdNumber(upr.getIdNumber());
                pp=passPnfService.findOneByPri(pp);

                model.addAttribute("user", pp.getPassengersName());
                model.addAttribute("userid", pp.getIdNumber());
                model.addAttribute("useracc", findBy.getUsersAccount());
                model.addAttribute("chajia",0);
                //拿到用户姓名

                responseObj.setCode(ResponseObj.OK);
                responseObj.setMsg(ResponseObj.OK_STR);
                responseObj.setNextUrl("tohome");
                //model.addAttribute("user",)
                result = new GsonUtils().toJson(responseObj);
            } else {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("密码错误！");
                result = new GsonUtils().toJson(responseObj);
            }
        }

        return result;
    }

}

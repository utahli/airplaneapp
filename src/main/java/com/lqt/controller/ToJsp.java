package com.lqt.controller;


import com.lqt.common.ResponseObj;
import com.lqt.entity.*;
import com.lqt.service.ipml.*;
import com.lqt.utils.GsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Utah on 2017/4/9.
 */
@Controller
@SessionAttributes({"chajia"})
public class ToJsp {

    @Resource
    private Passengers_Pnformation_Service_Ipml passgerService;

    @Resource
    private Users_Flight_Relationship_Service_Ipml ufService;

    @Resource
    private Administrator_Service_Ipml adService;

    @Resource
    private Financial_Information_Service_Ipml finanService;

    @Resource
    private Company_Information_Service_Ipml ciService;

    @Resource
    private Company_Flight_Relationship_Service_Ipml cfrService;

    @Resource
    private Voyage_Information_Service_Ipml viService;

    @Resource
    private Pay_Information_Service_Ipml payService;

    private ResponseObj responseObj;


    @RequestMapping(value = "tologin")
    public String toLogin(SessionStatus sessionStatus){
        //sessionStatus.setComplete();

        return "login";
    }


    @RequestMapping(value = "toforgetpwd")
    public String toForget() {
        return "forgetpwd";
    }

    @RequestMapping(value = "tohome")
    public String toHome() {
        return "query";
    }

    @RequestMapping(value = "toadminlogin")
    public String toAdmin(SessionStatus sessionStatus) {
        //sessionStatus.setComplete();
        return "adminlogin";
    }

    private static List<Company_Information> cilist = new ArrayList<>();
    @RequestMapping(value = "toaddair")
    public String toAddair(Model model) {
        if (cilist.isEmpty()) {
            cilist = ciService.findAll();
        }
        model.addAttribute("cilist", cilist);
        cilist = new ArrayList<>();

        return "addaiccraft";
    }


    @RequestMapping(value = "toaddcomp")
    public String toAddcomp(Model model) {

        return "addcomp";
    }


    private static List<Passengers_Pnformation> piList = new ArrayList<>();


    @ResponseBody
    @RequestMapping(value = "reggerfindby", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object reggerfindby(@RequestBody Map<String,String> map) {
        Object result;
        responseObj = new ResponseObj();
        System.out.println(map.toString());

        Passengers_Pnformation pi = new Passengers_Pnformation();

        if (!map.get("xm").equals("")) {
            pi.setPassengersName(map.get("xm"));
        }
        if (!map.get("sj").equals("")) {
            pi.setPassengersPhone(map.get("sj"));
        }
        if (!map.get("yx").equals("")) {
            pi.setPassengersEmail(map.get("yx"));
        }
        if (!map.get("sfz").equals("")) {
            pi.setIdNumber(map.get("sfz"));
        }

        System.out.println(pi.toString());

        piList = passgerService.adminfindBy(pi);
        if (piList.isEmpty()) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("未找到您要查询的用户信息，请重新选择查询条件！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("toregguser");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "reggerfindall",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object reggerfindall() {
        Object result;
        responseObj = new ResponseObj();

        piList = passgerService.findAll();

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("toregguser");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @RequestMapping(value = "toregguser")
    public String toRegguser(Model model) {

        if (!piList.isEmpty()) {
            model.addAttribute("find", 1);
            model.addAttribute("pilist", piList);
        }
        piList = new ArrayList<>();

        return "regguser";
    }


    private List<Pay_Information> payLists = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "adpayinfofindBy", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adpayinfofindBy(@RequestBody Map<String,String> map) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println("adpayinfofindBy==============="+map.toString());

        Pay_Information pi = new Pay_Information();

        if (!map.get("payacc").equals("")) {
            pi.setUsersAccount(map.get("payacc"));
        }
        if (!map.get("paytime").equals("")) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            pi.setPayTime(new java.sql.Timestamp(formatter.parse(map.get("paytime")).getTime()));
        }
        if (!map.get("paytype").equals("")) {
            if (map.get("paytype").equals("其他")) {
                pi.setPayType("其他");
                payLists = payService.adminFindByWithtype(pi);
            } else {
                pi.setPayType(map.get("paytype"));
                payLists = payService.adminFindByNotype(pi);
            }
        } else {
            payLists = payService.adminFindByNotype(pi);
        }

        System.out.println(pi.toString());

        if (payLists.isEmpty()) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("未找到您要查询的用户支付信息，请重新选择查询条件！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("topaycount");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "adpayinfofindall",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adpayinfofindall() {
        Object result;
        responseObj = new ResponseObj();

        payLists = payService.findAll();

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("topaycount");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @RequestMapping(value = "topaycount")
    public String toPaycount(Model model) {
        if (!payLists.isEmpty()) {
            model.addAttribute("find", 1);
            model.addAttribute("paylist", payLists);
        }
        payLists = new ArrayList<>();
        return "paycount";
    }


    private static List<Administrator_Imp> adList = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "aduserfindall",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object aduserfindall() {
        Object result;
        responseObj = new ResponseObj();

        adList = adService.findAll();

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("touser");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "aduserfindby", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object aduserfindby(@RequestBody Map<String,String> map) {
        Object result;
        responseObj = new ResponseObj();
        System.out.println(map.toString());

        Administrator_Imp admin = new Administrator_Imp();

        if (!map.get("name").equals("")) {
            admin.setAdministratorName(map.get("name"));
        }
        if (!map.get("acc").equals("")) {
            admin.setAdministratorAccount(map.get("acc"));
        }
        if (!map.get("type").equals("")) {
            admin.setAdministratorType(map.get("type"));
        }
        if (!map.get("code").equals("")) {
            admin.setAdministratorNumber(map.get("code"));
        }

        System.out.println(admin.toString());

        adList = adService.adminFindBy(admin);
        if (adList.isEmpty()) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("未找到您要查询的管理员信息，请重新选择查询条件！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("touser");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @RequestMapping(value = "touser")
    public String toUser(Model model) {

        if (!adList.isEmpty()) {
            model.addAttribute("find", 1);
            model.addAttribute("userlist", adList);
        }
        adList = new ArrayList<>();

        return "adminuser";
    }


    @ResponseBody
    @RequestMapping(value = "compinfoedit", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object compinfoedit(@RequestBody Map<String,String> map) {
        Object result;
        responseObj = new ResponseObj();
        System.out.println("compinfoedit==================="+map.toString());

        Company_Information ci = new Company_Information();
        ci.setCompanyCode(map.get("ecode"));
        ci.setCompanyInformation(map.get("einfo"));
        ci.setAddress(map.get("eadd"));
        ci.setWebsite(map.get("esite"));
        ci.setTelephone(map.get("ephone"));
        try {
            ciService.update(ci);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("公司信息修改失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }
        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("公司信息修改成功！请重新查询刷新信息。");
        responseObj.setNextUrl("tosearchcomp");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    private static List<Financial_Information> fiList = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "adfinancfindBy", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adfinancfindBy(@RequestBody Map<String,String> map) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println("adfinancfindBy==================="+map.toString());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        Financial_Information fi = new Financial_Information();

        if (!map.get("fname").equals("") && map.get("ffnum").equals("")) {
            fi.setFlightNumber(map.get("fname") + "%");
            if (!map.get("ftime").equals("")) {
                fi.setDate(new java.sql.Date(formatter.parse(map.get("ftime")).getTime()));
            }
            fiList = finanService.findListByCode(fi);
        } else {
            if (!map.get("ffnum").equals("")) {
                fi.setFlightNumber(map.get("ffnum"));
            }
            if (!map.get("ftime").equals("")) {
                fi.setDate(new java.sql.Date(formatter.parse(map.get("ftime")).getTime()));
            }
            fiList = finanService.findListByPri(fi);
        }

        System.out.println(fi.toString());

        if (fiList.isEmpty()) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("未找到您要查询的财务信息，请重新选择查询条件！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("tofinance");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "adfinancfindall",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adfinancfindall() {
        Object result;
        responseObj = new ResponseObj();

        fiList = finanService.findAll();

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("tofinance");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    private static List<Users_Flight_Relationship> ufrList = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "comptikyfindBy", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object comptikyfindBy(@RequestBody Map<String,String> map) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println("comptikyfindBy==================="+map.toString());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        Users_Flight_Relationship ufr = new Users_Flight_Relationship();

        if (!map.get("pname").equals("") && map.get("pfnum").equals("")) {
            ufr.setFlightNumber(map.get("pname") + "%");
            if (!map.get("ptime").equals("")) {
                ufr.setBookingTime(new java.sql.Timestamp(formatter.parse(map.get("ptime")).getTime()));
            }
            ufrList = ufService.adminFindLikeCode(ufr);
        } else {
            if (!map.get("pfnum").equals("")) {
                ufr.setFlightNumber(map.get("pfnum"));
            }
            if (!map.get("ptime").equals("")) {
                ufr.setBookingTime(new java.sql.Timestamp(formatter.parse(map.get("ptime")).getTime()));
            }
            ufrList = ufService.adminFindBy(ufr);
        }

        System.out.println(ufr.toString());

        if (ufrList.isEmpty()) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("未找到您要查询的航班售票信息信息，请重新选择查询条件！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("tocomptiky");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "totalcunt",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object totalcunt() {
        Object result;
        responseObj = new ResponseObj();

        float totalcuntpay = 0;
        int integral =0;
        List<Financial_Information> financialList = finanService.findAll();
        for (Financial_Information enty : financialList) {
            totalcuntpay+=enty.getSalesVolume();
            integral+=enty.getSalesIntegral();
        }


        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(String.valueOf(totalcuntpay));
        responseObj.setNextUrl(String.valueOf(integral));
        result = new GsonUtils().toJson(responseObj);
        totalcuntpay = 0;
        integral = 0;
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "comptikyfindAll",method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object comptikyfindAll() {
        Object result;
        responseObj = new ResponseObj();

        ufrList = ufService.findAll();

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("tocomptiky");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @RequestMapping(value = "tocomptiky")
    public String tocomptiky(Model model) {

        if (cilist.isEmpty()) {
            cilist = ciService.findAll();
        }
        model.addAttribute("cilist", cilist);
        cilist = new ArrayList<>();

        List<Company_Flight_Relationship> cfrlist = cfrService.findAll();
        model.addAttribute("cfrlist", cfrlist);

        if (!ufrList.isEmpty()) {
            model.addAttribute("find", 1);
            model.addAttribute("tikylist", ufrList);
        }
        ufrList = new ArrayList<>();
        return "searchcompfinance";
    }


    @RequestMapping(value = "tofinance")
    public String toFinance(Model model) {

        if (cilist.isEmpty()) {
            cilist = ciService.findAll();
        }
        model.addAttribute("cilist", cilist);
        cilist = new ArrayList<>();
        List<Company_Flight_Relationship> cfrlist = cfrService.findAll();
        model.addAttribute("cfrlist", cfrlist);

        if (!fiList.isEmpty()) {
            model.addAttribute("find", 1);
            model.addAttribute("financelist", fiList);
        }
        fiList = new ArrayList<>();
        return "adminfinance";
    }


    private static List<AdminFlightManeger> admaList = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "adcraftfindby", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adcraftfindBy(@RequestBody Map<String,String> map) throws ParseException {
        Object result;
        responseObj = new ResponseObj();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        AdminFlightManeger afm = new AdminFlightManeger();
        Voyage_Information vi = new Voyage_Information();

        System.out.println(map.toString());

        if ((!map.get("name").equals("")) && map.get("fnum").equals("")) {

            String[] str = map.get("name").split("-");

            afm.setCompanyName(str[0]);
            vi.setFlightNumber(str[1]+"%");

        } else {
            if (!map.get("fnum").equals("")) {
                vi.setFlightNumber(map.get("fnum"));
            }
        }

        if (!map.get("scity").equals("")) {
            vi.setStartPlace(map.get("scity"));
        }
        if (!map.get("lcity").equals("")) {
            vi.setLandPlace(map.get("lcity"));
        }
        if (!map.get("st").equals("") && map.get("st") != null) {
            vi.setStartTime(new java.sql.Timestamp(formatter.parse(map.get("st")).getTime()));
        }
        if (!map.get("lt").equals("") && map.get("lt") != null) {
            vi.setLandTime(new java.sql.Timestamp(formatter.parse(map.get("lt")).getTime()));
        }

        System.out.println(vi.toString());
        if ((!map.get("name").equals("")) && map.get("fnum").equals("")) {
            List<Voyage_Information> viList = viService.findListLikeCode(vi);//根据公司名称查询
            if (!viList.isEmpty()) {
                for (Voyage_Information enty : viList) {
                    afm.setFlight(enty);
                    admaList.add(afm);
                }
            } else {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("未找到您要查询的航班，请重新选择查询条件！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
        } else {
            List<Voyage_Information> viList = viService.findListIf(vi);
            if (!viList.isEmpty()) {
                for (Voyage_Information enty : viList) {
                    afm.setFlight(enty);
                    Company_Flight_Relationship cfr = new Company_Flight_Relationship();
                    cfr.setFlightNumber(enty.getFlightNumber());
                    Company_Flight_Relationship findBy = cfrService.findOneByNum(cfr);//查询航班所属的公司名称
                    afm.setCompanyName(findBy.getCompanyName());
                    admaList.add(afm);
                }
            } else {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("未找到您要查询的航班，请重新选择查询条件！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("toaircraft");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "adcraftfindall", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adminAirFindall() {
        Object result;
        responseObj = new ResponseObj();

        List<Company_Flight_Relationship> cfrList = cfrService.findAll();

        if (!cfrList.isEmpty()) {
            for (Company_Flight_Relationship enty : cfrList) {
                AdminFlightManeger afm = new AdminFlightManeger();
                afm.setCompanyName(enty.getCompanyName());
                Voyage_Information vi = new Voyage_Information();
                vi.setFlightNumber(enty.getFlightNumber());
                afm.setFlight(viService.findOneByNum(vi));
                admaList.add(afm);
            }
        }
        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("toaircraft");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    private static String compcode = "";

    @ResponseBody
    @RequestMapping(value = "searchcomp", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object Searchcomp(@RequestBody Map<String,String> map){
        Object result;
        responseObj = new ResponseObj();
        compcode = map.get("name");

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg(ResponseObj.OK_STR);
        responseObj.setNextUrl("tosearchcomp");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }



    @RequestMapping(value = "tosearchcomp")
    public String toSaerchcomp(Model model) {
        if (cilist.isEmpty()) {
            cilist = ciService.findAll();
        }
        model.addAttribute("cilist", cilist);

        if (compcode.equals("all")) {
            model.addAttribute("find", 1);
            model.addAttribute("findlist", cilist);
        } else if (!compcode.equals("")) {
            for (Company_Information enty : cilist) {
                if (enty.getCompanyCode().equals(compcode)) {
                    model.addAttribute("find", 1);
                    List<Company_Information> findLlist = new ArrayList<>();
                    findLlist.add(enty);
                    model.addAttribute("findlist", findLlist);
                }
            }
        }
        cilist = new ArrayList<>();
        compcode = "";

        return "searchcomp";
    }

    @ResponseBody
    @RequestMapping(value = "adcraftupdate", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object adcraftupdate(@RequestBody Map<String,String> map) throws ParseException {
        Object result;
        responseObj = new ResponseObj();

        System.out.println("adcraftupdate========"+map.toString());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(map.get("efnum"));
        vi.setVoyageNumber(map.get("ehangxian"));
        vi.setStartTime(new java.sql.Timestamp(formatter.parse(map.get("est")).getTime()));
        vi.setLandTime(new java.sql.Timestamp(formatter.parse(map.get("elt")).getTime()));
        vi.setStartPlace(map.get("escity"));
        vi.setLandPlace(map.get("elcity"));
        vi.setEconomicNumber(Integer.parseInt(map.get("eeseat")));
        vi.setEconomicPrice(Float.parseFloat(map.get("eeprice")));
        vi.setBusinessNumber(Integer.parseInt(map.get("ebseat")));
        vi.setBusinessPrice(Float.parseFloat(map.get("ebprice")));
        vi.setWaitTime(Integer.parseInt(map.get("ewait")));
        vi.setStatAirport(Saerch.Airport(map.get("escity")));
        vi.setLandAirport(Saerch.Airport(map.get("elcity")));
        vi.setFlightType(map.get("ejixin"));
        Float discount = Float.parseFloat(map.get("edisc"))*10;
        vi.setDiscountRate(discount.intValue());

        try {
            viService.update(vi);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("航班信息修改失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("航班信息修改成功！请重新查询刷新信息。");
        responseObj.setNextUrl("toaircraft");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @RequestMapping(value = "toaircraft")
    public String toAircraft(Model model) {
        if (cilist.isEmpty()) {
            cilist = ciService.findAll();
        }
        model.addAttribute("cilist", cilist);
        cilist = new ArrayList<>();
        if (!admaList.isEmpty()) {
            System.out.println(admaList.toString());
            model.addAttribute("find", 1);
            model.addAttribute("cflist", admaList);
        }
        admaList = new ArrayList<>();

        return "adminaiccraft";
    }


    @RequestMapping(value = "topersonal")
    public String toPersonal(Model model, HttpSession httpSession) {

        Passengers_Pnformation pp = new Passengers_Pnformation();
        pp.setIdNumber((String) httpSession.getAttribute("userid"));
        Passengers_Pnformation ppfind = passgerService.findOneByPri(pp);

        if (ppfind != null) {
            System.out.println(ppfind.toString());
            model.addAttribute("passenger", ppfind);
        }

        Users_Flight_Relationship uf = new Users_Flight_Relationship();
        uf.setUsersAccount((String) httpSession.getAttribute("useracc"));

        List<Users_Flight_Relationship> ufList = ufService.findListByPri(uf);

        System.out.println(ufList.toString());

        if (!ufList.isEmpty()) {
            for (Users_Flight_Relationship enty : ufList) {
                Date now = new Date();
                if (now.getTime() >= enty.getStartTime().getTime()) {
                    enty.setIfBuy(2);
                }
                if (enty.getIfBuy() == 5) {
                    enty.setIfBuy(1);
                    ufService.update(enty);
                    model.addAttribute("chajia",0);
                }
            }
            model.addAttribute("tikylist", ufList);
        }

        return "personal";
    }



    @RequestMapping(value = "topayesucc")
    public String toPayesucc() {
        return "payesucc";
    }


    @RequestMapping(value = "topaye",produces="text/plain;charset=UTF-8")
    public String toPaye(@RequestParam("flightNumber") String flightNumber, @RequestParam("phone") String phone,@RequestParam("humentype") String humentype,
                         @RequestParam("airName") String airName,@RequestParam("ctype") String ctype, @RequestParam("price") String price,
                         @RequestParam("chajia") String chajia,Model model,HttpSession httpSession) {

        model.addAttribute("user", httpSession.getAttribute("user"));
        model.addAttribute("userid", httpSession.getAttribute("userid"));
        model.addAttribute("phone", phone);
        model.addAttribute("airName", airName);
        model.addAttribute("ctype", ctype);
        model.addAttribute("price", price);
        model.addAttribute("chajia", chajia);
        model.addAttribute("humentype",humentype);
        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(flightNumber);
        Voyage_Information findBy = viService.findOneByNum(vi);
        model.addAttribute("tikit", findBy);

        return "paye";
    }


    @RequestMapping(value = "zhifu", produces="application/json;charset=UTF-8")
    private Object zhifu(@RequestParam("flightNumber") String flightNumber,@RequestParam("ctype") String ctype,@RequestParam("price") String price,
                         Model model,HttpSession httpSession){
        model.addAttribute("user", httpSession.getAttribute("user"));
        model.addAttribute("userid", httpSession.getAttribute("userid"));
        String acc = (String) httpSession.getAttribute("useracc");
        String[] str = acc.split("&");

        String code = flightNumber.substring(0, 1);

        model.addAttribute("phone", str[0]);
        model.addAttribute("airName", Saerch.Changcompname(code));
        model.addAttribute("ctype", ctype);
        model.addAttribute("price", price);
        model.addAttribute("chajia", 0);
        model.addAttribute("humentype","成人");
        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(flightNumber);
        Voyage_Information findBy = viService.findOneByNum(vi);
        model.addAttribute("tikit", findBy);
        return "paye";
    }

    @RequestMapping(value = "tobook",produces="text/plain;charset=UTF-8")
    public String toBook(@RequestParam("flightNumber") String flightNumber, @RequestParam("airName") String airName,
                         @RequestParam("ctype") String ctype, Model model,HttpSession httpSession) throws ParseException {

        String acc = (String) httpSession.getAttribute("useracc");
        if (acc == null) {
            model.addAttribute("error","error");
            return "login";
        }
        String[] accSplit = acc.split("&");
        model.addAttribute("user",httpSession.getAttribute("user"));
        model.addAttribute("userid",httpSession.getAttribute("userid"));
        model.addAttribute("userPhone",accSplit[0]);
       // model.addAttribute("userMail",accSplit[1]);
        model.addAttribute("airName",airName);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(flightNumber);
        Voyage_Information findBy = viService.findOneByNum(vi);
        model.addAttribute("tikit", findBy);

        System.out.println(findBy.toString());

        /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date startTimed = formatter.parse(findBy.getStartTime());
        Date landTimed = formatter.parse(landTime);*/

        SimpleDateFormat formatterd = new SimpleDateFormat("MM-dd");
        SimpleDateFormat formattert = new SimpleDateFormat("HH:mm");

        model.addAttribute("sdaye",formatterd.format(findBy.getStartTime()));
        model.addAttribute("ldaye",formatterd.format(findBy.getLandTime()));

        model.addAttribute("sweek",getWeek(findBy.getStartTime()));
        model.addAttribute("lweek",getWeek(findBy.getLandTime()));

        model.addAttribute("startTime",formattert.format(findBy.getStartTime()));
        model.addAttribute("landTime",formattert.format(findBy.getLandTime()));

        int price = 0;
        float dic = findBy.getDiscountRate();

        if (ctype.equals("econ")) {
            model.addAttribute("ctype", "经济舱");
            float en = findBy.getEconomicPrice();
            price = (int) (en * dic/100);

        } else {
            model.addAttribute("ctype", "头等舱");
            float bu = findBy.getBusinessPrice();
            price = (int) (bu * dic/100);
        }
        System.out.println("price===="+price);
        model.addAttribute("price", price);
        return "book";
    }


    private int getWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayForWeek = 0;
        if(c.get(Calendar.DAY_OF_WEEK) == 1){
            dayForWeek = 7;
        }else{
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }
        return dayForWeek;
    }

}

package com.lqt.controller;

import com.lqt.common.ResponseObj;
import com.lqt.entity.Financial_Information;
import com.lqt.entity.Pay_Information;
import com.lqt.entity.Users_Flight_Relationship;
import com.lqt.entity.Voyage_Information;
import com.lqt.service.ipml.Financial_Information_Service_Ipml;
import com.lqt.service.ipml.Pay_Information_Service_Ipml;
import com.lqt.service.ipml.Users_Flight_Relationship_Service_Ipml;
import com.lqt.service.ipml.Voyage_Information_Service_Ipml;
import com.lqt.utils.GsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Utah on 2017/5/11.
 */
@Controller
@RequestMapping("/paye")
@SessionAttributes({"chajia"})
public class BookTickit {

    private ResponseObj responseObj;

    @Resource
    private Voyage_Information_Service_Ipml voimfo;

    @Resource
    private Users_Flight_Relationship_Service_Ipml uflightR;

    @Resource
    private Pay_Information_Service_Ipml payService;

    @Resource
    private Financial_Information_Service_Ipml finanService;

    private static Users_Flight_Relationship gaiqiantiky;



    @ResponseBody
    @RequestMapping(value = "gaiqian", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object gaiqian(@RequestBody Map<String,String> map,Model model,HttpSession httpSession) throws ParseException {
        Object result;
        System.out.println(map.toString());
        responseObj = new ResponseObj();
        model.addAttribute("chajia", map.get("chajia"));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Users_Flight_Relationship ufr = new Users_Flight_Relationship();
        ufr.setBookingTime(new java.sql.Timestamp(formatter.parse(map.get("bookt")).getTime()));
        ufr.setUsersAccount((String) httpSession.getAttribute("useracc"));
        ufr.setIfBuy(5);
        try {
            uflightR.update(ufr);
            gaiqiantiky = uflightR.findTiky(ufr);//获取要改签的机票信息
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("改签失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setNextUrl("tohome");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }



    @ResponseBody
    @RequestMapping(value = "quxiao", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object quxiao(@RequestBody Map<String,String> map,HttpSession httpSession) throws ParseException {
        Object result;
        System.out.println(map.toString());
        responseObj = new ResponseObj();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Users_Flight_Relationship urf = new Users_Flight_Relationship();
        urf.setBookingTime(new java.sql.Timestamp(formatter.parse(map.get("bookt")).getTime()));
        urf.setUsersAccount((String) httpSession.getAttribute("useracc"));
        urf.setIfBuy(3);
        try {
            uflightR.update(urf);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("订单取消失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        Users_Flight_Relationship findBy = uflightR.findTiky(urf);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(findBy.getFlightNumber());
        Voyage_Information vifind = voimfo.findOneByNum(vi);

        if (findBy.getSeatType().equals("经济舱")) {
            vifind.setEconomicNumber(vifind.getEconomicNumber() + 1);
        } else {
            vifind.setBusinessNumber(vifind.getBusinessNumber()+1);
        }
        voimfo.update(vifind);

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("订单取消成功！");
        responseObj.setNextUrl("topersonal");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "tuipiao", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object tuipiao(@RequestBody Map<String,String> map,HttpSession httpSession) throws ParseException {
        Object result;
        System.out.println("退票====="+map.toString());
        responseObj = new ResponseObj();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Users_Flight_Relationship urf = new Users_Flight_Relationship();
        urf.setBookingTime(new java.sql.Timestamp(formatter.parse(map.get("bookt")).getTime()));
        urf.setUsersAccount((String) httpSession.getAttribute("useracc"));
        urf.setIfBuy(4);
        try {
            uflightR.update(urf);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("退票失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        Users_Flight_Relationship findBy = uflightR.findTiky(urf);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(findBy.getFlightNumber());
        Voyage_Information vifind = voimfo.findOneByNum(vi);

        if (findBy.getSeatType().equals("经济舱")) {
            vifind.setEconomicNumber(vifind.getEconomicNumber() + 1);
        } else {
            vifind.setBusinessNumber(vifind.getBusinessNumber()+1);
        }
        voimfo.update(vifind);

        Date now = new Date();
        java.sql.Date currentTime =new java.sql.Date(now.getTime());
        Financial_Information fi = new Financial_Information();
        fi.setFlightNumber(findBy.getFlightNumber());
        fi.setDate(currentTime);
        int tuikuan = Integer.parseInt(map.get("tuikuan"));
        fi.setSalesVolume(-tuikuan);
        finanService.update(fi);
        System.out.println(fi.toString());

        Pay_Information pay = new Pay_Information();
        java.sql.Timestamp currentTime2 = new java.sql.Timestamp(now.getTime());
        pay.setPayTime(currentTime2);
        pay.setUsersAccount((String) httpSession.getAttribute("useracc"));
        pay.setPayType("退款");
        pay.setPayMoney(tuikuan);
        try {
            payService.add(pay);
        } catch (Exception e) {
            e.printStackTrace();
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("退票成功！");
        responseObj.setNextUrl("topersonal");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "buy", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object paye(@RequestBody Map<String,String> map,Model model,HttpSession httpSession) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println(map.toString());

        Date now = new Date();
        java.sql.Date currentTime =new java.sql.Date(now.getTime());
        java.sql.Timestamp currentTime02 = new java.sql.Timestamp(now.getTime());
        System.out.println(currentTime);
        System.out.println(currentTime02);

        Financial_Information fi = new Financial_Information();
        fi.setDate(currentTime);
        fi.setFlightNumber(map.get("flightNumber"));
        fi.setSalesVolume(Float.parseFloat(map.get("price")));
        fi.setSalesIntegral(0);

        Pay_Information pay = new Pay_Information();
        pay.setPayTime(currentTime02);
        pay.setUsersAccount((String) httpSession.getAttribute("useracc"));
        pay.setPayType(map.get("paytype"));
        pay.setPayNumbers(map.get("bankCard"));
        pay.setPayMoney(Float.parseFloat(map.get("price")));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Users_Flight_Relationship uf = new Users_Flight_Relationship();
        uf.setUsersAccount((String) httpSession.getAttribute("useracc"));
        uf.setFlightNumber(map.get("flightNumber"));
        uf.setStartTime(new java.sql.Timestamp(formatter.parse(map.get("stime")).getTime()));


        //============================改签==================================
        if (!map.get("chajia").equals("0")) {
            System.out.println("改签=============================");
            Voyage_Information vi = new Voyage_Information();
            vi.setFlightNumber(map.get("flightNumber"));
            Voyage_Information findBy = voimfo.findOneByNum(vi);

            if (!chayupiao(findBy, map.get("ctype"))) {
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("抱歉，余票不足，请重新购票...");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }

            //删除原订单
            quxiaodd(gaiqiantiky.getFlightNumber());
            System.out.println("删除原订单=====" + gaiqiantiky);
            gaiqiantiky = new Users_Flight_Relationship();

            //新订单
            try {
                xindd(findBy, 0, map.get("ctype"), map.get("price"), httpSession);
            } catch (Exception e) {
                e.printStackTrace();
                responseObj.setCode(ResponseObj.FAILED);
                responseObj.setMsg("其他错误！");
                result = new GsonUtils().toJson(responseObj);
                return result;
            }

            model.addAttribute("chajia",0);

            uf.setIfBuy(6);
        } else {
            uf.setIfBuy(1);
        }
        //==============================改签===============================

        System.out.println(uf.toString());

        try {
            payService.add(pay);
            finanService.update(fi);
            uflightR.updateunpay(uf);
        } catch (Exception e) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("其他错误！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("支付成功！");
        responseObj.setNextUrl("topayesucc");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "gaiqiannocj", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object gaiqiannocj(@RequestBody Map<String,String> map, Model modelMap, HttpSession httpSession) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println(map.toString());

        //先查询余票
        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(map.get("flightNumber"));
        Voyage_Information findBy = voimfo.findOneByNum(vi);

        if (!chayupiao(findBy, map.get("ctype"))) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("抱歉，余票不足，请重新购票...");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        //删除原订单
        quxiaodd(gaiqiantiky.getFlightNumber());
        System.out.println("删除原订单====="+gaiqiantiky);
        gaiqiantiky = new Users_Flight_Relationship();

        try {
            xindd(findBy,6,map.get("ctype"),map.get("price"),httpSession);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("其他错误！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        modelMap.addAttribute("chajia",0);

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("改签成功，是否进入个人中心？");
        responseObj.setNextUrl("topersonal");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    private void xindd(Voyage_Information findBy,int ifbuy,String ctype,String price,HttpSession httpSession) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date now = new Date();

        Users_Flight_Relationship uf = new Users_Flight_Relationship();

        uf.setUsersAccount((String) httpSession.getAttribute("useracc"));
        uf.setFlightNumber(findBy.getFlightNumber());
        uf.setStartTime(findBy.getStartTime());

        uf.setBookingTime(new java.sql.Timestamp(now.getTime()));
        uf.setIdNumber((String) httpSession.getAttribute("userid"));
        uf.setDandTime(findBy.getLandTime());
        uf.setSeatType(ctype);
        uf.setTicketType("优惠票");
        uf.setSeatNumber(0);
        uf.setMoney(Float.parseFloat(price));
        uf.setIntegral(Integer.parseInt(price));
        uf.setIfBuy(ifbuy);
        uf.setStartPlace(findBy.getStartPlace());
        uf.setLandPlace(findBy.getLandPlace());
        uf.setStatAirport(findBy.getStatAirport());
        uf.setLandAirport(findBy.getLandAirport());



        uflightR.add(uf);
    }


    private void quxiaodd(String flightNumber) {
        uflightR.delete(gaiqiantiky);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(gaiqiantiky.getFlightNumber());
        Voyage_Information vifind = voimfo.findOneByNum(vi);

        if (gaiqiantiky.getSeatType().equals("经济舱")) {
            vifind.setEconomicNumber(vifind.getEconomicNumber() + 1);
        } else {
            vifind.setBusinessNumber(vifind.getBusinessNumber()+1);
        }
        voimfo.update(vifind);
    }


    @ResponseBody
    @RequestMapping(value = "payeoder", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    private Object book(@RequestBody Map<String,String> map, Model model,HttpSession httpSession) throws ParseException {
        Object result;
        responseObj = new ResponseObj();
        System.out.println(map.toString());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date now = new Date();

        /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm", Locale.ENGLISH);
        Date startTimed = formatter.parse(map.get("sdatetime"));*/

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("当前时间：" + sdf.format(d));*/

        Users_Flight_Relationship uf = new Users_Flight_Relationship();

        uf.setUsersAccount((String) httpSession.getAttribute("useracc"));
        uf.setFlightNumber(map.get("flightNumber"));
        uf.setStartTime(new java.sql.Timestamp(formatter.parse(map.get("startTime")).getTime()));

        Users_Flight_Relationship ufTiky = uflightR.findTikyNopri(uf);

        if (ufTiky != null && (ufTiky.getIfBuy() == 0 || ufTiky.getIfBuy() == 1|| ufTiky.getIfBuy() == 5 || ufTiky.getIfBuy() == 6)) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("您已经预定过了这张机票，请勿重复预定！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(map.get("flightNumber"));
        Voyage_Information findBy = voimfo.findOneByNum(vi);

        if (!chayupiao(findBy, map.get("ctype"))) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("抱歉，余票不足，请重新购票...");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        try {
            xindd(findBy,0,map.get("ctype"),map.get("price"),httpSession);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("其他错误！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("预订成功");
        responseObj.setNextUrl("topaye");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }

    private boolean chayupiao(Voyage_Information findBy,String ctype) {

        if (ctype.equals("经济舱")) {
            if (findBy.getEconomicNumber() > 0) {
                findBy.setEconomicNumber(findBy.getEconomicNumber() - 1);
                voimfo.update(findBy);
            } else {
                return false;
            }
        } else {
            if (findBy.getBusinessNumber() > 0) {
                findBy.setBusinessNumber(findBy.getBusinessNumber() - 1);
                voimfo.update(findBy);
            } else {
                return false;
            }
        }
        return true;
    }

}

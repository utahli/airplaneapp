package com.lqt.controller;

import com.lqt.common.ResponseList;
import com.lqt.common.ResponseObj;
import com.lqt.entity.Company_Flight_Relationship;
import com.lqt.entity.Company_Information;
import com.lqt.entity.Financial_Information;
import com.lqt.entity.Voyage_Information;
import com.lqt.service.ipml.Company_Flight_Relationship_Service_Ipml;
import com.lqt.service.ipml.Company_Information_Service_Ipml;
import com.lqt.service.ipml.Financial_Information_Service_Ipml;
import com.lqt.service.ipml.Voyage_Information_Service_Ipml;
import com.lqt.utils.GsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Utah on 2017/5/7.
 */
@Controller
@RequestMapping("/saerch")
public class Saerch {

    private ResponseList responseList;
    private ResponseObj responseObj;

    @Resource
    private Voyage_Information_Service_Ipml voimfo;

    @Resource
    private Voyage_Information_Service_Ipml viService;

    @Resource
    private Company_Flight_Relationship_Service_Ipml cfrService;

    @Resource
    private Company_Information_Service_Ipml ciService;

    @Resource
    private Financial_Information_Service_Ipml fiService;

    @ResponseBody
    @RequestMapping(value = "adminaddcomp", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object addcomp(@RequestBody Map<String,String> map) throws ParseException {
        Object result;

        System.out.println(map.toString());

        responseObj = new ResponseObj();

        Company_Information ci = new Company_Information();
        ci.setCompanyName(map.get("name"));
        ci.setCompanyCode(map.get("code"));
        ci.setCompanyInformation(map.get("info"));
        ci.setAddress(map.get("addr"));
        ci.setWebsite(map.get("site"));
        ci.setTelephone(map.get("phone"));

        if (ciService.findOneByCode(ci) != null) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("该航空公司已存在，请勿重复添加！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        try {
            ciService.add(ci);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("航空公司添加失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }
        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("航空公司添加成功，是否继续添加？");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    public static String Airport(String city) {
        String airport;
        switch (city) {
            case "长春":
                airport = "龙嘉国际机场";
                break;
            case "三亚":
                airport = "凤凰国际机场T2";
                break;
            case "北京":
                airport = "首都国际机场T1";
                break;
            case "上海":
                airport = "浦东国际机场";
                break;
            case "广州":
                airport = "白云国际机场";
                break;
            case "成都":
                airport = "双流国际机场";
                break;
            default:
                airport = "宝安国际机场";
                break;
        }
        return airport;
    }

    public static String Changcompname(String code) {
        String airName;
        switch (code) {
            case "3U":
                airName = "川航";
                break;
            case "8L":
                airName = "祥鹏航空";
                break;
            case "BK":
                airName = "奥凯航空";
                break;
            case "CA":
                airName = "国航";
                break;
            case "CN":
                airName = "新华航空";
                break;
            case "CZ":
                airName = "南航";
                break;
            case "DR":
                airName = "瑞丽航空";
                break;
            case "DZ":
                airName = "东海航空";
                break;
            case "EU":
                airName = "成都(鹰联)航空";
                break;
            case "FU":
                airName = "福州航空";
                break;
            case "GJ":
                airName = "长龙航空";
                break;
            case "GS":
                airName = "天津航空";
                break;
            case "GY":
                airName = "多彩贵州航空";
                break;
            case "HO":
                airName = "吉祥航空";
                break;
            case "HU":
                airName = "海航";
                break;
            case "JD":
                airName = "首都(金鹿)航空";
                break;
            case "JR":
                airName = "幸福航空";
                break;
            case "KN":
                airName = "联航";
                break;
            case "KY":
                airName = "昆航";
                break;
            case "MF":
                airName = "厦航";
                break;
            case "MU":
                airName = "东航";
                break;
            case "NS":
                airName = "河北航空";
                break;
            case "PN":
                airName = "西部航空";
                break;
            case "QW":
                airName = "青岛航空";
                break;
            case "SC":
                airName = "山东航空";
                break;
            case "TV":
                airName = "西藏航空";
                break;
            case "UQ":
                airName = "乌鲁木齐航空";
                break;
            case "Y8":
                airName = "扬子江快运航空";
                break;
            case "YI":
                airName = "英安航空";
                break;
            case "ZH":
                airName = "深航";
                break;
            case "G5":
                airName = "华夏航空";
                break;
            /*case "8C":
                airName = "";
                break;*/
            default:
                airName = "东星航空";
                break;
        }
        return airName;
    }


    @ResponseBody
    @RequestMapping(value = "adminaddair", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object addair(@RequestBody Map<String,String> map) throws ParseException {
        Object result;

        System.out.println(map.toString());

        responseObj = new ResponseObj();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Voyage_Information vi = new Voyage_Information();
        vi.setFlightNumber(map.get("fnum"));
        vi.setVoyageNumber(map.get("hangxian"));
        vi.setStartTime(new java.sql.Timestamp(formatter.parse(map.get("st")).getTime()));
        vi.setLandTime(new java.sql.Timestamp(formatter.parse(map.get("lt")).getTime()));
        vi.setStartPlace(map.get("scity"));
        vi.setLandPlace(map.get("lcity"));
        vi.setEconomicNumber(Integer.parseInt(map.get("eseat")));
        vi.setEconomicPrice(Float.parseFloat(map.get("eprice")));
        vi.setBusinessNumber(Integer.parseInt(map.get("bseat")));
        vi.setBusinessPrice(Float.parseFloat(map.get("bprice")));
        vi.setWaitTime(Integer.parseInt(map.get("wait")));
        vi.setStatAirport(Airport(map.get("scity")));
        vi.setLandAirport(Airport(map.get("lcity")));
        vi.setFlightType(map.get("jixin"));
        Float discount = Float.parseFloat(map.get("disc"))*10;
        vi.setDiscountRate(discount.intValue());

        Company_Flight_Relationship cfi = new Company_Flight_Relationship();
        cfi.setCompanyName(Changcompname( map.get("comp")));
        cfi.setFlightNumber(map.get("fnum"));

        Financial_Information fi = new Financial_Information();
        fi.setDate(new java.sql.Date(formatter.parse(map.get("st")).getTime()));
        fi.setFlightNumber(map.get("fnum"));
        fi.setSalesVolume(0);
        fi.setSalesIntegral(0);


        if (viService.findOneByNum(vi) != null) {
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("该航班已存在，请勿重复添加！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        try {
            viService.add(vi);
            cfrService.add(cfi);
            fiService.add(fi);
        } catch (Exception e) {
            e.printStackTrace();
            responseObj.setCode(ResponseObj.FAILED);
            responseObj.setMsg("航班添加失败！");
            result = new GsonUtils().toJson(responseObj);
            return result;
        }

        responseObj.setCode(ResponseObj.OK);
        responseObj.setMsg("航班添加成功，是否继续添加？");
        result = new GsonUtils().toJson(responseObj);
        return result;
    }


    @ResponseBody
    @RequestMapping(value = "query", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
    public Object data(@RequestBody Map<String,String> map) {
        Object result;
        System.out.println(map.toString());
        responseList = new ResponseList();

        Voyage_Information vi = new Voyage_Information();
        vi.setStartPlace(map.get("from"));
        vi.setLandPlace(map.get("to"));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            vi.setStartTime(new java.sql.Timestamp(df.parse(map.get("ftime")).getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Voyage_Information> viList;
        viList = voimfo.findListByPri(vi);

        if (!viList.isEmpty()) {
            for (Voyage_Information enty : viList) {


                System.out.println(enty.toString());
            }
            responseList.setData(viList);
            responseList.setCode(ResponseObj.OK);
            responseList.setMsg(ResponseObj.OK_STR);
            result = new GsonUtils().toJson(responseList);
        } else {
            responseList.setCode(ResponseObj.FAILED);
            responseList.setMsg("很抱歉，您搜索的日期"+map.get("ftime")+map.get("from")+"到"+map.get("to")+"没有直飞航班"+"！");
            result = new GsonUtils().toJson(responseList);
        }
        return result;
    }
}

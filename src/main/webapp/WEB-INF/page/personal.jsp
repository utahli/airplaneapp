<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→个人中心</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>

    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <span style=" margin-left:-15px; margin-right:20px;">企业商旅管理</span>-->
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li class="dropdown">
                    <a href="JavaScript:admin();" >管理员登录 </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，${sessionScope.user} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:login();">安全退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul>
                <li><a href="JavaScript:shouye();"><i class="icon iconfont icon-nav-block font24"  >&#xe626;</i>返回首页</a></li>
            </ul>
        </div>


    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>个人信息</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">
            <div class="col-sm-4">
                <div class="form-group" id="dxm">
                    <label for="apdiv" class="w90 text-right">姓名</label>
                    <input type="text" class="form-control input-sm" style=" width:90px; display:inline-block;" id="xm" value = "${passenger.passengersName}" readonly="true">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="dsj" >
                    <label for="apdiv" class="w90 text-right">手机</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sj" value = "${passenger.passengersPhone}" readonly="true">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="dyx">
                    <label for="apdiv" class="w90 text-right">邮箱</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="yx" value = "${passenger.passengersEmail}" readonly="true">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="dsfz">
                    <label for="apdiv" class="w90 text-right">身份证</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sfz" value = "${passenger.idNumber}" readonly="true">
                </div>
            </div>
        </div>
        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">
            <input type="button"
                   id="btn_login"
                   value="编辑个人信息"
                   onclick="Bianji();"
                   class="btn btn-danger btn-sm mar-right-20"/>
           <%-- <button type="submit" class="btn btn-danger btn-sm mar-right-20">编辑个人信息</button>--%>
            <%--<button type="submit" class="btn btn-default btn-sm mar-right-20">清空条件</button>--%>
        </div>
        <ol class="breadcrumb">
            <li>订单信息列表</li>
        </ol>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped font12 table-bordered v-align-top" >
            <thead>
            <tr >
                <th>创建时间</th>
                <th>乘机人</th>
                <th>联系人</th>
                <th>航班号</th>
                <th>舱位</th>
                <th>起飞行程</th>
                <th>到达行程</th>
                <th>结算总价</th>
                <th style="width:65px;">订单状态</th>
                <th>操作</th>
            </tr>
            </thead>

            <c:if test="${sessionScope.user != null}">


            <c:forEach items="${tikylist}" var="tiky">
                <%--<div>
                    <input id="sdatetime" type="hidden" value="${sdatetime}"></input>
                    <input id="ldatetime" type="hidden" value="${ldatetime}"></input>
                    <input id="airName" type="hidden" value="${airName}"></input>
                    <input id="flightNumber" type="hidden" value="${flightNumber}"></input>
                    <input id="from" type="hidden" value="${from}"></input>
                    <input id="to" type="hidden" value="${to}"></input>
                    <input id="statAirport" type="hidden" value="${statAirport}"></input>
                    <input id="landAirport" type="hidden" value="${landAirport}"></input>
                    <input id="ctype" type="hidden" value="${ctype}"></input>
                    <input id="price" type="hidden" value="${price + 80}"></input>
                </div>--%>
                <tr>
                    <td>${tiky.bookingTime}</td>
                    <td><p>${passenger.passengersName}</p></td>
                    <td>
                        <l>${passenger.passengersName}</l> </br>
                        <ll>${passenger.passengersPhone}</ll>
                    </td>
                    <td>${tiky.flightNumber}</td>
                    <td>${tiky.seatType}</td>
                    <td>
                        <div class="startbox">
                            <div class="startcity">
                                <span>${tiky.startPlace}</span>
                                <l>${tiky.statAirport}</l></br>
                                <ll>${tiky.startTime}</ll>
                            </div>
                        </div></td>
                    <td><div class="startbox">

                        <div class="startcity">
                            <span>${tiky.landPlace}</span>
                            <l>${tiky.landAirport}</l><br/>
                            <ll>${tiky.dandTime}</ll>
                        </div>
                    </div></td>
                    <td>${tiky.money}</td>

                    <c:choose>
                        <c:when test="${tiky.ifBuy == 0}">
                            <td>未支付</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a><br>
                                <a href="#" class="qxdd">【取消订单】</a><br>
                                <a href="#" class="zhifu" >【立即支付】</a></td>

                        </c:when>

                        <c:when test="${tiky.ifBuy == 1}">
                            <td>已支付</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a><br>
                                <a href="#" class="gaiqian">【改签】</a><br>
                                <a href="#" class="tuipiao">【退票】</a></td>
                        </c:when>

                        <c:when test="${tiky.ifBuy == 2}">
                            <td>已出票</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a></td>
                        </c:when>

                        <c:when test="${tiky.ifBuy == 3}">
                            <td>已取消</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a></td>
                        </c:when>

                        <c:when test="${tiky.ifBuy == 4}">
                            <td>已退票</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a></td>
                        </c:when>

                        <c:otherwise>
                            <td>已改签</td>
                            <td><a href="#" class="xiangqing">【查看详情】</a><br>
                            <a href="#" class="tuipiao">【退票】</a></td>
                        </c:otherwise>

                    </c:choose>

                </tr>
            </c:forEach>
            </c:if>
        </table>

        <div class="clearfix"></div>
    </div>
</div>
<!-- main end -->
</div>
<!-- 管理模板 结束 -->
<div id="srev" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">姓名</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="cxm" readonly="true" value = "${passenger.passengersName}">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">身份证</label>
            <input name="input" type="text" class="form-control input-sm w200" style=" display: inline-block;" id="csfz" readonly="true" value="${passenger.idNumber}" >
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">手机</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="csj" placeholder="手机">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">邮箱</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="cyx" placeholder="邮箱">
        </div>
    </div>

    <div class="modal-footer mar-top-5" style=' text-align:center;'>
        <button type="button" class="btn btn-primary" onClick="edet();">确认</button>
        <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
    </div>
</div>

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/changpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            ok: function () {}
                        }).showModal();
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>

<script>

    function login() {
        window.location.href = "tologin";
    }

    function checkEdit() {


    }

    $(".gaiqian").click(function()
    {

    });

    function chackkkk() {
        var sqq = /^1[34578]\d{9}$/;
        if (!sqq.test($('#csj').val())
            || $('#csj').val().length < 11
            || $('#csj').val().length > 14
            || $('#csj').val() == "") {
            $('#csj').focus();
            layer.tips('手机格式不正确', $("#csj"),{bg: '#AE81FF',time: 2000});
            return false;
        }

        var mail = /^([a-z0-9A-Z]+[-|_|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
        if (!mail.test($('#cyx').val())
            || $('#cyx').val().length < 11
            || $('#cyx').val().length > 30
            || $('#cyx').val() == "") {
            $('#cyx').focus();
            layer.tips('邮箱格式不正确', $("#cyx"),{bg: '#AE81FF',time: 2000});
            return false;
        };
        return true;
    }

    function edet() {
        if (chackkkk()) {

            var cxm = $("#cxm").val();
            var csj = $("#csj").val();
            var cyx = $("#cyx").val();
            var csfz = $("#csfz").val();

            var call = $("#sj").val();
            var mail = $("#yx").val();

            var pass = {
                "cxm": cxm,
                "csj": csj,
                "cyx":cyx,
                "csfz":csfz,
                "call":call,
                "mail":mail
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/editpass',
                async : true,
                contentType:'application/json;charset=utf-8',
                data: JSON.stringify(pass),
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        OutPutBody();
                        dialog({
                            width: '20em',
                            cancel: false,
                            title: '提示...',
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    }else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        /*layer.closeAll();*/
                        /*dialog({
                            width: '20em',
                            cancel:false,
                            title: '提示...',
                            content: data.msg,
                            okValue: '确定',
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }


        function OutPutBody () {
            $("#dxm").html("");
            $("#dsj").html("");
            $("#dyx").html("");
            $("#dsfz").html("");

            $("#dxm").append('<label for="apdiv" class="w90 text-right">姓名</label>'+
                '<input type="text" class="form-control input-sm" style=" width:90px; display:inline-block;" id="xm" value = "'+cxm+'" readonly="true">');
            $("#dsj").append('<label for="apdiv" class="w90 text-right">手机</label>'+
                '<input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sj" value = "'+csj+'" readonly="true">');
            $("#dyx").append('<label for="apdiv" class="w90 text-right">邮箱</label>'+
                '<input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="yx" value = "'+cyx+'" readonly="true">');
            $("#dsfz").append('<label for="apdiv" class="w90 text-right">身份证</label>'+
                '<input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sfz" value = "'+csfz+'" readonly="true">');
        };
    };

    function Bianji() {

        //document.getElementById("cxm").value = $("#xm").val();
        document.getElementById("csj").value = $("#sj").val();
        document.getElementById("cyx").value =$("#yx").val();
        //document.getElementById("csfz").value =$("#sfz").val();

        layer.open({
            type: 1,
            title: '编辑个人信息',
            area: ['400px', 'auto'],
            fix: false,
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#srev'),
        });

    }

    function admin() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要登录管理员系统吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "toadminlogin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }

    function shouye() {
        window.location.href = "tohome";
    }

    $(document).ready(function() {


        $(".tuipiao").click(function()
        {
            var bookt=$(this).parents("tr").children("td").eq(0).html();
            var price=$(this).parents("tr").children("td").eq(7).html();
            var tuikuan = parseInt(parseInt(price) - parseInt(price)*0.1)
            obj={
                "bookt":bookt,
                "tuikuan":tuikuan
            };

            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "您的退款费用为<strong class='rmb orange-f60 font16'>￥"+parseInt(parseInt(price)*0.1)+
                    "</strong><br><div class='text' style=' text-align:center;'><h4>您确定要取消该订单？</h4></div>",
                okValue: '确定',
                ok: function () {
                    $.ajax({
                        type: "POST",
                        url: 'paye/tuipiao',
                        async : true,
                        contentType:'application/json;charset=utf-8',
                        data:JSON.stringify(obj) ,
                        dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                        cache: false,
                        success: function (data) {
                            if (data.code == 1) {
                                dialog({
                                    width: '20em',
                                    title: '提示...',
                                    cancel:false,
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    okValue: '确定',
                                    ok: function () {
                                        window.location.href = data.nextUrl;
                                    },

                                }).showModal();
                            } else {
                                dialog({
                                    width: '20em',
                                    title: '提示...',
                                    cancel:false,
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    ok: function () {}
                                }).showModal();
                            }
                        }
                    });
                },
                button:[
                    {
                        value:'取消'
                    }
                ]
            }).showModal();


        });


        $(".zhifu").click(function(){
            var flightNumber=$(this).parents("tr").children("td").eq(3).html();
            var ctype=$(this).parents("tr").children("td").eq(4).html();
            var price=$(this).parents("tr").children("td").eq(7).html();

            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"您确定要支付吗？"+"</div>",
                okValue: '确定',
                ok: function () {
                    window.location.href = "zhifu?flightNumber="+flightNumber+"&ctype="+ctype+"&price="+price;
                },
                button:[
                    {
                        value:'取消'
                    }
                ]
            }).showModal();
        });


        $(".gaiqian").click(function(){
            var feiyong=$(this).parents("tr").children("td").eq(7).html();
            var bookt=$(this).parents("tr").children("td").eq(0).html();
            var chajia = parseInt(parseInt(feiyong)-parseInt(feiyong)*0.05);

            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "您的改签费用为<strong class='rmb orange-f60 font16'>￥"+parseInt(parseInt(feiyong)*0.05)+
                "</strong><br><div class='text' style=' text-align:center;'><h4>您确定要改签吗？</h4></div>",
                okValue: '确定',
                ok: function () {
                    obj={
                        "bookt":bookt,
                        "chajia":chajia
                    };
                    $.ajax({
                        type: "POST",
                        url: 'paye/gaiqian',
                        async : true,
                        contentType:'application/json;charset=utf-8',
                        data:JSON.stringify(obj) ,
                        dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                        cache: false,
                        success: function (data) {
                            if (data.code == 1) {
                                window.location.href = data.nextUrl;
                            } else {
                                dialog({
                                    width: '20em',
                                    title: '提示...',
                                    cancel:false,
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    ok: function () {}
                                }).showModal();
                            }
                        }
                    });
                },
                button:[
                    {
                        value:'取消'
                    }
                ]
            }).showModal();

        });


            $(".qxdd").click(function(){  //取消订单
            var bookt=$(this).parents("tr").children("td").eq(0).html();
            var bookt2=$(this).parents("tr").children("td").eq(3).html();
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"您确定要取消该订单？"+"</div>",
                okValue: '确定',
                ok: function () {

                    obj={
                        "bookt":bookt
                    };
                    $.ajax({
                        type: "POST",
                        url: 'paye/quxiao',
                        async : true,
                        contentType:'application/json;charset=utf-8',
                        data:JSON.stringify(obj) ,
                        dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                        cache: false,
                        success: function (data) {
                            if (data.code == 1) {
                                dialog({
                                    width: '20em',
                                    title: '提示...',
                                    cancel:false,
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    okValue: '确定',
                                    ok: function () {
                                        window.location.href = data.nextUrl;
                                    }
                                }).showModal();
                            } else {
                                dialog({
                                    width: '20em',
                                    title: '提示...',
                                    cancel:false,
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    ok: function () {}
                                }).showModal();
                            }
                        }
                    });

                },
                button:[
                    {
                        value : '取消',
                    }
                ]
            }).showModal();
        });

        $(".xiangqing").click(function(){
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"详情......"+"</div>",
                ok: function () {}
            }).showModal();
        });

    });
</script>
</body>
</html>

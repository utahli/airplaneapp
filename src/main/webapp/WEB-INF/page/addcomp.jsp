<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
    java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

    java.util.Date currentTime = new java.util.Date();//得到当前系统时间

    String str_date1 = formatter.format(currentTime); //将日期时间格式化


%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→添加航空公司</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>


    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li>
                    <a href="JavaScript:vistaaa();">用户登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，航班管理员-${sessionScope.adname} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:qiehuan();">切换用户</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:fanhuii();"><span class="glyphicon glyphicon-cog font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px; margin-bottom:2px;"></span>返回</a></li>
                <li ><a href="JavaScript:quanxian();"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i> 用户管理</a></li>
                <li ><a href="JavaScript:quanxian();"><span class="glyphicon glyphicon-yen font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px;  margin-bottom:2px;"></span>账务管理</a></li>
                <li class="active"><a href="#"><i class="icon iconfont icon-nav-block font24"  >&#xe623;</i>航班管理</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>航班信息</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">

            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">公司名称</label>
                    <input id="name" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="川航" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">航空编号</label>
                    <input id="code" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="3U" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">公司信息</label>
                    <input id="info" type="text" class="form-control input-sm w200" style=" display: inline-block;"   >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">地址</label>
                    <input id="addr" type="text" class="form-control input-sm w200" style=" display: inline-block;"   >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">网址</label>
                    <input id="site" type="text" class="form-control input-sm w200" style=" display: inline-block;"   >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">电话</label>
                    <input id="phone" type="text" class="form-control input-sm w200" style=" display: inline-block;"   >
                </div>
            </div>
        </div>

        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">
            <input type="button"
                   id="btn_login1"
                   value="确认添加"
                   onclick="addair();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="清空条件"
                   onclick="qingkongcomp();"
                   class="btn btn-default btn-sm mar-right-20"/>

        </div>

        <ol class="breadcrumb">
        <div class="tuigaiqian" style="cursor:pointer; color:#337ab7;">航空公司一览表</div>
        </ol>
        <div class="instruction">
            <table class="table table-bordered" style="height:150px;">
                <tr>
                    <td>3U-川航</td>
                    <td>8C-东星航空</td>
                    <td>8L-祥鹏</td>
                    <td>9C-春秋航空</td>
                    <td>BK-奥凯航空</td>
                    <td>CA-国航</td>
                    <td>CN-新华航空</td>
                    <td>CZ-南航</td>
                    <td>ZH-深航</td>
                </tr>
                <tr>
                    <td>DR-瑞丽航空</td>
                    <td>DZ-东海航空</td>
                    <td>EU-成都(鹰联)航空</td>
                    <td>FM-上航</td>
                    <td>FU-福州航空</td>
                    <td>G5-华夏航空</td>
                    <td>GJ-长龙航空</td>
                    <td>GS-天津航空</td>
                    <td>YI-英安航空</td>
                </tr>
                <tr>
                    <td>GX-广西航空</td>
                    <td>GY-多彩贵州航空</td>
                    <td>HO-吉祥航空</td>
                    <td>HU-海航</td>
                    <td>JD-首都(金鹿)航空</td>
                    <td>JR-幸福航空</td>
                    <td>KN-联航</td>
                    <td>KY-昆航</td>
                    <td>Y8-扬子江快运航空</td>
                </tr>
                <tr>
                    <td>MF-厦航</td>
                    <td>MU-东航</td>
                    <td>NS-河北航空</td>
                    <td>PN-西部航空</td>
                    <td>QW-青岛航空</td>
                    <td>SC-山东航空</td>
                    <td>TV-西藏航空</td>
                    <td>UQ-乌鲁木齐航空</td>
                    <td>VD-鲲鹏航空</td>
                </tr>

            </table>
        </div>

        <div class="clearfix"></div>
    </div>
    <div>
        <input id="airadmin" type="hidden" value="${sessionScope.adname}"></input>

    </div>
</div>

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/adminchangpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        /*dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: data.msg,
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>

    <script>

        function cheackempty() {
            if ("" == $('#name') .val()) {
                $("#name").tips({
                    side: 2,
                    msg: '公司名称不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#name").focus();
                return false;
            }
            if ("" == $('#code') .val()) {
                $("#code").tips({
                    side: 2,
                    msg: '航空编码不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#code").focus();
                return false;
            }
            if ("" == $('#info') .val()) {
                $("#info").tips({
                    side: 2,
                    msg: '公司信息不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#info").focus();
                return false;
            }
            if ("" == $('#addr') .val()) {
                $("#addr").tips({
                    side: 2,
                    msg: '地址不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#addr").focus();
                return false;
            }
            if ("" == $('#site') .val()) {
                $("#site").tips({
                    side: 2,
                    msg: '网址不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#site").focus();
                return false;
            }
            if ("" == $('#phone') .val()) {
                $("#phone").tips({
                    side: 2,
                    msg: '电话不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#phone").focus();
                return false;
            }
            /*电话正则表达式验证 /^0\d{2,3}-?\d{7,8}$/*/
            /*网址正则表达式验证 /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/*/
            return true;

        }

        function cheackinfo() {
            if ($('#name').val() == '瑞丽航空') {
                if($('#code').val() != 'DR') {
                    return false;
                }
            }
            if ($('#name').val() == '东海航空') {
                if($('#code').val() != 'DZ') {
                    return false;
                }
            }
            if ($('#name').val() == '成都(鹰联)航空') {
                if($('#code').val() != 'EU') {
                    return false;
                }
            }
            if ($('#name').val() == '上航') {
                if($('#code').val() != 'FM') {
                    return false;
                }
            }
            if ($('#name').val() == '福州航空') {
                if($('#code').val() != 'FU') {
                    return false;
                }
            }
            if ($('#name').val() == '华夏航空') {
                if($('#code').val() != 'G5') {
                    return false;
                }
            }
            if ($('#name').val() == '长龙航空') {
                if($('#code').val() != 'GJ') {
                    return false;
                }
            }
            if ($('#name').val() == '天津航空') {
                if($('#code').val() != 'GS') {
                    return false;
                }
            }
            if ($('#name').val() == '英安航空') {
                if($('#code').val() != 'YI') {
                    return false;
                }
            }
            if ($('#name').val() == '川航') {
                if($('#code').val() != '3U') {
                    return false;
                }
            }
            if ($('#name').val() == '东星航空') {
                if($('#code').val() != '8C') {
                    return false;
                }
            }
            if ($('#name').val() == '祥鹏航空') {
                if($('#code').val() != '8L') {
                    return false;
                }
            }
            if ($('#name').val() == '春秋航空') {
                if($('#code').val() != '9C') {
                    return false;
                }
            }
            if ($('#name').val() == '奥凯航空') {
                if($('#code').val() != 'BK') {
                    return false;
                }
            }
            if ($('#name').val() == '国航') {
                if($('#code').val() != 'CA') {
                    return false;
                }
            }
            if ($('#name').val() == '新华航空') {
                if($('#code').val() != 'CN') {
                    return false;
                }
            }
            if ($('#name').val() == '南航') {
                if($('#code').val() != 'CZ') {
                    return false;
                }
            }
            if ($('#name').val() == '深航') {
                if($('#code').val() != 'ZH') {
                    return false;
                }
            }
            if ($('#name').val() == '广西航空') {
                if($('#code').val() != 'GX') {
                    return false;
                }
            }
            if ($('#name').val() == '多彩贵州航空') {
                if($('#code').val() != 'GY') {
                    return false;
                }
            }
            if ($('#name').val() == '吉祥航空') {
                if($('#code').val() != 'HO') {
                    return false;
                }
            }
            if ($('#name').val() == '海航') {
                if($('#code').val() != 'HU') {
                    return false;
                }
            }
            if ($('#name').val() == '首都(金鹿)航空') {
                if($('#code').val() != 'JD') {
                    return false;
                }
            }
            if ($('#name').val() == '幸福航空') {
                if($('#code').val() != 'JR') {
                    return false;
                }
            }
            if ($('#name').val() == '联航') {
                if($('#code').val() != 'KN') {
                    return false;
                }
            }
            if ($('#name').val() == '昆航') {
                if($('#code').val() != 'KY') {
                    return false;
                }
            }
            if ($('#name').val() == '扬子江快运航空') {
                if($('#code').val() != 'Y8') {
                    return false;
                }
            }
            if ($('#name').val() == '厦航') {
                if($('#code').val() != 'MF') {
                    return false;
                }
            }
            if ($('#name').val() == '东航') {
                if($('#code').val() != 'MU') {
                    return false;
                }
            }
            if ($('#name').val() == '河北航空') {
                if($('#code').val() != 'NS') {
                    return false;
                }
            }
            if ($('#name').val() == '西部航空') {
                if($('#code').val() != 'PN') {
                    return false;
                }
            }
            if ($('#name').val() == '青岛航空') {
                if($('#code').val() != 'QW') {
                    return false;
                }
            }
            if ($('#name').val() == '山东航空') {
                if($('#code').val() != 'SC') {
                    return false;
                }
            }
            if ($('#name').val() == '西藏航空') {
                if($('#code').val() != 'TV') {
                    return false;
                }
            }
            if ($('#name').val() == '乌鲁木齐航空') {
                if($('#code').val() != 'UQ') {
                    return false;
                }
            }
            if ($('#name').val() == '鲲鹏航空') {
                if($('#code').val() != 'VD') {
                    return false;
                }
            }

            return true;
        }

        function qingkongcomp() {
            document.getElementById('name').value="";
            document.getElementById('code').value="";
            document.getElementById('info').value="";
            document.getElementById('addr').value="";
            document.getElementById('site').value="";
            document.getElementById('phone').value="";

        }

        function addair() {
            if ($('#airadmin').val() == '') {
                dialog({
                    width: '20em',
                    cancel: false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"请先登录！"+"</div>"  ,
                    okValue: '确定',
                    ok: function () {
                    }
                }).showModal();
                return;
            }
            if(cheackempty()) {
                if (cheackinfo()) {

                     var name = $('#name').val();
                     var code = $("#code").val();
                     var info = $("#info").val();
                     var addr = $("#addr").val();
                     var site = $("#site").val();
                     var phone = $("#phone").val();

                     var dd = {
                     "name": name,
                     "code": code,
                     "info": info,
                     "addr": addr,
                     "site": site,
                     "phone": phone,
                     }

                    $.ajax({
                        type: "POST",
                        url: 'saerch/adminaddcomp',
                        async: true,
                        contentType: 'application/json;charset=utf-8',
                        data: JSON.stringify(dd),
                        dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                        cache: false,
                        success: function (data) {
                            if (data.code == 1) {

                                dialog({
                                    width: '20em',
                                    cancel: false,
                                    title: '提示...',
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    okValue: '确定',
                                    ok: function () {

                                    },
                                    button: [
                                        {
                                            value: "取消",
                                            callback: function (){
                                                window.history.back();
                                            }
                                        }
                                    ]
                                }).showModal();
                            } else {
                                dialog({
                                    width: '20em',
                                    cancel: false,
                                    title: '提示...',
                                    content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                    okValue: '确定',
                                    ok: function () {
                                    }
                                }).showModal();
                            }
                        }
                    });
                } else {
                    dialog({
                        width: '20em',
                        cancel: false,
                        title: '提示...',
                        content: "<div class='text' style=' text-align:center;'>"+"航空公司与航空编码不匹配！"+"</div>" ,
                        okValue: '确定',
                        ok: function () {
                        }
                    }).showModal();
                }
            }

        }

        function fanhuii() {
            window.location.href = "toaircraft";
            //window.history.back();
        }

        function vistaaa() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要登录用户系统吗？"+"</div>" ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "tologin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }


        function qiehuan() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要切换用户吗？"+"</div>" ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "toadminlogin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }

        $('.instruction').hide();
        $('.tuigaiqian').click(
            function(){
                $('.instruction').toggle();
            }
        )

        function quanxian() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您没有该管理员权限！"+"</div>" ,
                okValue: '确定',
                ok: function () {
                },
            }).showModal();
        }

        $(function() {
            $('.bubbleinfo').each(function() {
                var distance = 10;
                var time = 10;
                var hideDelay = 100;

                var hideDelayTimer = null;

                var beingShown = false;
                var shown = false;
                var trigger = $('.trigger', this);
                var info = $('.popup', this).css('opacity', 0);


                $([trigger.get(0), info.get(0)]).mouseover(function() {
                    if (hideDelayTimer) clearTimeout(hideDelayTimer);
                    if (beingShown || shown) {
                        // don't trigger the animation again
                        return;
                    } else {
                        // reset position of info box
                        beingShown = true;

                        info.css({
                            top: 30,
                            left:0,
                            display: 'block'
                        }).animate({
                            top: '-=' + distance + 'px',
                            opacity: 1
                        }, time, 'swing', function() {
                            beingShown = false;
                            shown = true;
                        });
                    }

                    return false;
                }).mouseout(function() {
                    if (hideDelayTimer) clearTimeout(hideDelayTimer);
                    hideDelayTimer = setTimeout(function() {
                        hideDelayTimer = null;
                        info.animate({
                            top: '-=' + distance + 'px',
                            opacity: 0
                        }, time, 'swing', function() {
                            shown = false;
                            info.css('display', 'none');
                        });

                    }, hideDelay);

                    return false;
                });
            });
        });

    </script>
</body>
</html>

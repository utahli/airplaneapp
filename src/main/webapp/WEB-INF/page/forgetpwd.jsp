<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- 上面这两行是java代码的引用 --%>
<%
    request.setAttribute("domain", "http://localhost:8080/AirPleanApp/");
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/login.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
   /* document.onreadystatechange = function () {
        if(document.readyState=="complete") {
            dialog({
                width: '20em',
                title: '提示...',
                content: error,
                okValue: '确定',
                ok: function () {}
            }).showModal();
        }
    }*/

   function souye() {
       window.history.back();
   }

    function phyanzma() {

        var sqq = /^1[34578]\d{9}$/;
        if (!sqq.test($('#phone').val())
            || $('#phone').val().length < 11
            || $('#phone').val().length > 14
            || $('#phone').val() == "") {

            $('#phone').focus();
            $("#phone").tips({
                side: 2,
                msg: '手机号格式不正确',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }

        var obj = {};
        obj['acc'] = $("#phone").val();

        $.ajax({
            type: "POST",
            url: 'logAndReg/forgetyz',
            async : true,
            contentType:'application/json;charset=utf-8',
            data:JSON.stringify(obj) ,
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '确定',
                        ok: function () {
                           // window.location.href = data.nextUrl;
                        }
                    }).showModal();
                } else {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        ok: function () {}
                    }).showModal();
                }
            }
        });
    }

   function mayanzma() {
       var mail = /^([a-z0-9A-Z]+[-|_|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
       if (!mail.test($('#mail').val())
           || $('#mail').val().length < 11
           || $('#mail').val().length > 30
           || $('#mail').val() == "") {
           $('#mail').focus();
           $("#mail").tips({
               side: 2,
               msg: '邮箱格式不正确',
               bg: '#AE81FF',
               time: 2
           });
           return false;
       }

       var obj = {};
       obj['acc'] = $("#mail").val();

       $.ajax({
           type: "POST",
           url: 'logAndReg/forgetyz',
           async : true,
           contentType:'application/json;charset=utf-8',
           data:JSON.stringify(obj) ,
           dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
           cache: false,
           success: function (data) {
               if (data.code == 1) {
                   dialog({
                       width: '20em',
                       title: '提示...',
                       cancel:false,
                       content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                       okValue: '确定',
                       ok: function () {
                           // window.location.href = data.nextUrl;
                       }
                   }).showModal();
               } else {
                   dialog({
                       width: '20em',
                       title: '提示...',
                       cancel:false,
                       content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                       ok: function () {}
                   }).showModal();
               }
           }
       });
   }

    function phchang() {

        if ($('#phyzm').val() != '111') {
            $('#phyzm').focus();
            $("#phyzm").tips({
                side: 2,
                msg: '验证码错误！',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }

        if ($('#phpwd').val().length < 6) {
            $('#phpwd').focus();
            $("#phpwd").tips({
                side: 2,
                msg: '密码不能小于6位',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }
        if ($('#rphpwd').val() != $('#phpwd').val()) {
            $('#rphpwd').focus();
            $("#rphpwd").tips({
                side: 2,
                msg: '两次密码不一致',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }

        var obj = {};
        obj['acc'] = $("#phone").val();
        obj['pwd'] = $("#phpwd").val();

        $.ajax({
            type: "POST",
            url: 'logAndReg/forget',
            async : true,
            contentType:'application/json;charset=utf-8',
            data:JSON.stringify(obj) ,
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '确定',
                        ok: function () {
                            window.location.href = data.nextUrl;
                        }
                    }).showModal();
                } else {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        ok: function () {}
                    }).showModal();
                }
            }
        });
    }

   function machang() {

       if ($('#mayz').val() != '111') {
           $('#mayz').focus();
           $("#mayz").tips({
               side: 2,
               msg: '验证码错误！',
               bg: '#AE81FF',
               time: 2
           });
           return false;
       }

       if ($('#mapwd').val().length < 6) {
           $('#mapwd').focus();
           $("#mapwd").tips({
               side: 2,
               msg: '密码不能小于6位',
               bg: '#AE81FF',
               time: 2
           });
           return false;
       }
       if ($('#rmapwd').val() != $('#mapwd').val()) {
           $('#rmapwd').focus();
           $("#rmapwd").tips({
               side: 2,
               msg: '两次密码不一致',
               bg: '#AE81FF',
               time: 2
           });
           return false;
       }


       var obj = {};
        obj['acc'] = $("#mail").val();
        obj['pwd'] = $("#mapwd").val();

       $.ajax({
           type: "POST",
           url: 'logAndReg/forget',
           async : true,
           contentType:'application/json;charset=utf-8',
           data:JSON.stringify(obj) ,
           dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
           cache: false,
           success: function (data) {
               if (data.code == 1) {
                   dialog({
                       width: '20em',
                       title: '提示...',
                       cancel:false,
                       content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                       okValue: '确定',
                       ok: function () {
                           window.location.href = data.nextUrl;
                       }
                   }).showModal();
               } else {
                   dialog({
                       width: '20em',
                       title: '提示...',
                       cancel:false,
                       content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                       ok: function () {}
                   }).showModal();
               }
           }
       });
   }
</script>
<head>
    <title>航空票务系统→忘记密码</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="<%=basePath%>/static/css/login2.css" rel="stylesheet" type="text/css"/>
</head>
<html>
<body>

<h1>忘记密码<sup>2017</sup></h1>

<div class="login" style="margin-top:50px;">

    <div class="header">
        <div class="switch" id="switch"><a class="switch_btn_focus" id="switch_qlogin" href="javascript:void(0);"
                                           tabindex="7">手机验证</a>
            <a class="switch_btn" id="switch_login" href="javascript:void(0);" tabindex="8">邮箱验证</a>

            <div class="switch_bottom" id="switch_bottom" style="position: absolute; width: 64px; left: 0px;"></div>
        </div>
    </div>


    <%--<div class="web_qr_login" id="web_qr_login" style="display: block; height: 235px;">

        <!--登录-->
        <div class="web_login" id="web_login">


            <div class="login-box">


                <div class="login_form">
                    <form name="form2" id="regUser2" accept-charset="utf-8" action=""
                          method="post">
                        <input type="hidden" name="to" value="reg"/>
                        <input type="hidden" name="did" value="0"/>
                        <ul class="reg_form" id="reg-ul2">
                            &lt;%&ndash;<div id="userCue" class="cue">快速注册请注意格式</div>&ndash;%&gt;
                            <li>

                                <label for="passengersName" class="input-tips2">手机：</label>

                                <div class="inputOuter2">
                                    <input type="text" id="passengersName4" name="passengersName" maxlength="16" class="inputstyle2"/>
                                </div>

                            </li>
                            <li>
                                <div class="inputArea">
                                    <input type="button"
                                           id="reg13"
                                           onclick="webReg()"
                                           style="margin-top:10px;margin-left:85px;"
                                           class="button_blue"
                                           value="发送验证码"/>
                                </div>

                            </li>
                            <li>

                                <label for="idNumber" class="input-tips2">验证码：</label>

                                <div class="inputOuter2">
                                    <input type="text" id="idNumber4" name="idNumber" maxlength="18" placeholder="填写验证码" class="inputstyle2"/>
                                </div>

                            </li>

                            <li>
                                <label for="usersPassword" class="input-tips2">密码：</label>

                                <div class="inputOuter2">
                                    <input type="password" id="usersPassword33" name="pwd" maxlength="16" placeholder="密码不能小于6位" class="inputstyle2"/>
                                </div>

                            </li>
                            <li>
                                <label for="usersPassword2" class="input-tips2">确认密码：</label>

                                <div class="inputOuter2">
                                    <input type="password" id="usersPassword3" name="" maxlength="16" class="inputstyle2"/>
                                </div>

                            </li>

                            <li>
                                <div class="inputArea">
                                    <input type="button"
                                           id="reg5"
                                           onclick="webReg()"
                                           style="margin-top:10px;margin-left:85px;"
                                           class="button_blue"
                                           value="修改密码"/>
                                </div>

                            </li>
                    </form>
                </div>

            </div>

        </div>
        <!--登录end-->
    </div>--%>
    <div class="web_qr_login" id="web_qr_login" style="display: block; ">

        <div class="web_login">
            <form name="form2" id="regUser1" accept-charset="utf-8" action=""
                  method="post">
                <input type="hidden" name="to" value="reg"/>
                <input type="hidden" name="did" value="0"/>
                <ul class="reg_form" id="reg-ul1">
                    <%--<div id="userCue" class="cue">快速注册请注意格式</div>--%>
                    <li>

                        <label for="phone" class="input-tips2">手机：</label>

                        <div class="inputOuter2">
                            <input type="text" id="phone" name="passengersName" maxlength="16" class="inputstyle2"/>
                        </div>

                    </li>
                    <li>
                        <div class="inputArea">
                            <input type="button"
                                   id="reg111"
                                   onclick="phyanzma()"
                                   style="margin-top:10px;margin-left:85px;"
                                   class="button_blue"
                                   value="发送验证码"/>
                        </div>

                    </li>
                    <li>

                        <label for="phyzm" class="input-tips2">验证码：</label>

                        <div class="inputOuter2">
                            <input type="text" id="phyzm" name="idNumber" maxlength="18" placeholder="填写验证码" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <label for="phpwd" class="input-tips2">新密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="phpwd" name="pwd" maxlength="16" placeholder="密码不能小于6位" class="inputstyle2"/>
                        </div>

                    </li>
                    <li>
                        <label for="rphpwd" class="input-tips2">确认密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="rphpwd" name="" maxlength="16" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <div class="inputArea">
                            <input type="button"
                                   id="reg3"
                                   onclick="phchang()"
                                   style="margin-top:10px;margin-left:85px;"
                                   class="button_blue"
                                   value="修改密码"/>
                            <a href="JavaScript:souye();" class="zcxy">返回首页</a>
                        </div>

                    <div class="cl"></div>
                </ul>
            </form>


        </div>


    </div>

    <!--注册-->
    <div class="qlogin" id="qlogin" style="display: none; ">

        <div class="web_login">
            <form name="form2" id="regUser" accept-charset="utf-8" action=""
                  method="post">
                <input type="hidden" name="to" value="reg"/>
                <input type="hidden" name="did" value="0"/>
                <ul class="reg_form" id="reg-ul">
                    <%--<div id="userCue" class="cue">快速注册请注意格式</div>--%>
                    <li>

                        <label for="mail" class="input-tips2">邮箱：</label>

                        <div class="inputOuter2">
                            <input type="text" id="mail" name="passengersName" maxlength="30" class="inputstyle2"/>
                        </div>

                    </li>
                        <li>
                            <div class="inputArea">
                                <input type="button"
                                       id="reg1"
                                       onclick="mayanzma()"
                                       style="margin-top:10px;margin-left:85px;"
                                       class="button_blue"
                                       value="发送验证码"/>
                            </div>

                        </li>
                    <li>

                        <label for="mayz" class="input-tips2">验证码：</label>

                        <div class="inputOuter2">
                            <input type="text" id="mayz" name="idNumber" maxlength="18" placeholder="填写验证码" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <label for="mapwd" class="input-tips2">新密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="mapwd" name="pwd" maxlength="16" placeholder="密码不能小于6位" class="inputstyle2"/>
                        </div>

                    </li>
                    <li>
                        <label for="rmapwd" class="input-tips2">确认密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="rmapwd" name="" maxlength="16" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <div class="inputArea">
                            <input type="button"
                                   id="reg"
                                   onclick="machang()"
                                   style="margin-top:10px;margin-left:85px;"
                                   class="button_blue"
                                   value="修改密码"/>
                            <a href="JavaScript:souye();" class="zcxy">返回首页</a>
                        </div>

                    </li>
                    <div class="cl"></div>
                </ul>
            </form>


        </div>


    </div>
    <!--注册end-->
</div>

</body>
</html>

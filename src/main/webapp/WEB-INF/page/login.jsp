<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- 上面这两行是java代码的引用 --%>
<%
    request.setAttribute("domain", "http://localhost:8080/AirPleanApp/");
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/login.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<script type="text/javascript">
   /* document.onreadystatechange = function () {
        if(document.readyState=="complete") {
            dialog({
                width: '20em',
                title: '提示...',
                content: error,
                okValue: '确定',
                ok: function () {}
            }).showModal();
        }
    }*/


   function forget() {
       window.location.href = "toforgetpwd";
   }

    function checkLoginInfo() {
        if ("" == $("#u").val()) {
            $("#u").tips({
                side: 2,
                msg: '用户名不得为空',
                bg: '#AE81FF',
                time: 2
            });
            $("#u").focus();
            return false;
        }
        if ($("#p").val() == "") {

            $("#p").tips({
                side: 2,
                msg: '密码不得为空',
                bg: '#AE81FF',
                time: 2
            });
            $("#p").focus();
            return false;
        }
        return true;
    }

    function webLogin() {
        if (checkLoginInfo()) {
            var loginname = $("#u").val();
            var password = $("#p").val();
            var log = {
                "usersAccount": loginname,
                "usersPassword": password
            };
            $.ajax({
                type: "POST",
                url: 'logAndReg/login',
                async : true,
                contentType:'application/json;charset=utf-8',
                data: JSON.stringify(log),
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {

                        window.location.href = data.nextUrl;
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {}
                        }).showModal();

                       /* var d = dialog({
                            title: '提示',
                            content: data.msg,
                            /!*cancel: false,*!/
//                            ok: function () {}
                        });
                        d.show();*/
                        //$("#u").focus();
                    }
                }
            });
        }
    }

    function webReg() {

        if ($('#passengersName').val().length < 2
                || $('#passengersName').val().length > 16
                || $('#passengersName').val() == "") {

            $('#passengersName').focus();

            $("#passengersName").tips({
                side: 2,
                msg: '用户姓名必须为4-16位字符',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }


        var cardid = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
        if (!cardid.test($('#idNumber').val())
            || $('#idNumber').val().length < 14
            || $('#idNumber').val().length > 19
            || $('#idNumber').val() == "") {
            $('#idNumber').focus();
            $("#idNumber").tips({
                side: 2,
                msg: '身份证格式不正确',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }


        if ($('#usersPassword').val().length < 6) {
            $('#usersPassword').focus();
            $("#usersPassword").tips({
                side: 2,
                msg: '密码不能小于6位',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }
        if ($('#usersPassword2').val() != $('#usersPassword').val()) {
            $('#usersPassword2').focus();
            $("#usersPassword2").tips({
                side: 2,
                msg: '两次密码不一致',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }

        var sqq = /^1[34578]\d{9}$/;
        if (!sqq.test($('#passengersPhone').val())
                || $('#passengersPhone').val().length < 11
                || $('#passengersPhone').val().length > 14
                || $('#passengersPhone').val() == "") {

            $('#passengersPhone').focus();
            $("#passengersPhone").tips({
                side: 2,
                msg: '手机号格式不正确',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }

        var mail = /^([a-z0-9A-Z]+[-|_|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
        if (!mail.test($('#passengersEmail').val())
            || $('#passengersEmail').val().length < 11
            || $('#passengersEmail').val().length > 30
            || $('#passengersEmail').val() == "") {
            $('#passengersEmail').focus();
            $("#passengersEmail").tips({
                side: 2,
                msg: '邮箱格式不正确',
                bg: '#AE81FF',
                time: 2
            });
            return false;
        }
        var obj = {};
        obj['passengersName'] = $("#passengersName").val();
        obj['idType'] = $("#idType").val();
        obj['idNumber'] = $("#idNumber").val();
        obj['usersPassword'] = $("#usersPassword").val();
        obj['passengersPhone'] = $("#passengersPhone").val();
        obj['passengersEmail'] = $("#passengersEmail").val();
        $.ajax({
            type: "POST",
            url: 'logAndReg/regger',
            async : true,
            contentType:'application/json;charset=utf-8',
            data:JSON.stringify(obj) ,
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: data.msg,
                        okValue: '确定',
                        ok: function () {
                            window.location.href = data.nextUrl;
                        },
                        button:[
                            {
                                value:"取消",
                                callback:function () {
                                    window.location.href = "tologin";
                                }
                            }
                        ]
                    }).showModal();
                } else {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        ok: function () {}
                    }).showModal();
                }
            }
        });
    }

</script>
<head>
    <title>航空票务系统→用户登录</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="<%=basePath%>/static/css/login2.css" rel="stylesheet" type="text/css"/>
</head>
<html>
<body>
<c:if test="${error != null}">
<script type="text/javascript">
    dialog({
        width: '20em',
        title: '提示...',
        cancel:false,
        content: "<div class='text' style=' text-align:center;'>"+'抱歉，您尚未登录，请先登录再购票...'+"</div>"    ,
        okValue: '确定',
        ok: function () {}
    }).showModal();
</script>
</c:if>
<h1>用户登陆注册<sup>2017</sup></h1>

<div class="login" style="margin-top:50px;">

    <div class="header">
        <div class="switch" id="switch"><a class="switch_btn_focus" id="switch_qlogin" href="javascript:void(0);"
                                           tabindex="7">快速登录</a>
            <a class="switch_btn" id="switch_login" href="javascript:void(0);" tabindex="8">快速注册</a>

            <div class="switch_bottom" id="switch_bottom" style="position: absolute; width: 64px; left: 0px;"></div>
        </div>
    </div>


    <div class="web_qr_login" id="web_qr_login" style="display: block; height: 235px;">

        <!--登录-->
        <div class="web_login" id="web_login">


            <div class="login-box">


                <div class="login_form">
                    <form action="" name="loginform"
                          accept-charset="utf-8" id="login_form" class="loginForm"
                          method="post"><input type="hidden" name="did" value="0"/>
                        <input type="hidden" name="to" value="log"/>

                        <div class="uinArea" id="uinArea">
                            <label class="input-tips" for="u">帐号：</label>

                            <div class="inputOuter" id="uArea">

                                <input type="text" id="u" name="loginId" placeholder="手机或邮箱" class="inputstyle"/>
                            </div>
                        </div>
                        <div class="pwdArea" id="pwdArea">
                            <label class="input-tips" for="p">密码：</label>

                            <div class="inputOuter" id="pArea">

                                <input type="password" id="p" name="pwd" class="inputstyle"/>
                            </div>
                        </div>

                        <div style="padding-left:50px;margin-top:20px;">
                            <input type="button"
                                   id="btn_login"
                                   value="登 录"
                                   onclick="webLogin();"
                                   style="width:150px;"
                                   class="button_blue"/>
                            <a href="JavaScript:forget();" class="zcxy">忘记密码</a>
                        </div>
                    </form>
                </div>

            </div>

        </div>
        <!--登录end-->
    </div>

    <!--注册-->
    <div class="qlogin" id="qlogin" style="display: none; ">

        <div class="web_login">
            <form name="form2" id="regUser" accept-charset="utf-8" action=""
                  method="post">
                <input type="hidden" name="to" value="reg"/>
                <input type="hidden" name="did" value="0"/>
                <ul class="reg_form" id="reg-ul">
                    <div id="userCue" class="cue">快速注册请注意格式</div>
                    <li>

                        <label for="passengersName" class="input-tips2">姓名：</label>

                        <div class="inputOuter2">
                            <input type="text" id="passengersName" name="passengersName" maxlength="16" placeholder="长度为4-16位字符" class="inputstyle2"/>
                        </div>

                    </li>
                    <li>

                        <label for="idType" class="input-tips2">证件类型：</label>

                        <div class="inputOuter2">
                            <input type="text" id="idType" name="idType" value = "身份证" readonly="true" maxlength="18"  class="inputstyle2"/>
                        </div>

                    </li>
                    <li>

                        <label for="idNumber" class="input-tips2">证件号码：</label>

                        <div class="inputOuter2">
                            <input type="text" id="idNumber" name="idNumber" maxlength="18"  class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <label for="usersPassword" class="input-tips2">密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="usersPassword" name="pwd" maxlength="16" placeholder="密码不能小于6位" class="inputstyle2"/>
                        </div>

                    </li>
                    <li>
                        <label for="usersPassword2" class="input-tips2">确认密码：</label>

                        <div class="inputOuter2">
                            <input type="password" id="usersPassword2" name="" maxlength="16" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <label for="passengersPhone" class="input-tips2">手机号：</label>

                        <div class="inputOuter2">

                            <input type="text" id="passengersPhone" name="passengersPhone" maxlength="18" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <label for="passengersEmail" class="input-tips2">邮箱：</label>

                        <div class="inputOuter2">

                            <input type="text" id="passengersEmail" name="passengersEmail" maxlength="30" class="inputstyle2"/>
                        </div>

                    </li>

                    <li>
                        <div class="inputArea">
                            <input type="button"
                                   id="reg"
                                   onclick="webReg()"
                                   style="margin-top:10px;margin-left:85px;"
                                   class="button_blue"
                                   value="同意协议并注册"/>
                            <a href="#" class="zcxy">注册协议</a>
                        </div>

                    </li>
                    <div class="cl"></div>
                </ul>
            </form>


        </div>


    </div>
    <!--注册end-->
</div>

</body>
</html>

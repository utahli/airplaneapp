<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→查询航空公司</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>

    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li>
                    <a href="JavaScript:vistaaa();">用户登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，航班管理员-${sessionScope.adname} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:qiehuan();">切换用户</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:fanhuiii();"><span class="glyphicon glyphicon-cog font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px; margin-bottom:2px;"></span>返回</a></li>
                <li ><a href="JavaScript:quanxian();"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i> 用户管理</a></li>
                <li ><a href="JavaScript:quanxian();"><span class="glyphicon glyphicon-yen font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px;  margin-bottom:2px;"></span>账务管理</a></li>
                <li class="active"><a href="#"><i class="icon iconfont icon-nav-block font24"  >&#xe623;</i>航班管理</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>查询条件</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">

            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">公司名称</label>
                    <select id="name" class="form-control input-sm"  style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <c:forEach items="${cilist}" var="ci">
                            <option value="${ci.companyCode}">${ci.companyName}-${ci.companyCode}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

        </div>
        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">

            <input type="button"
                   id="btn_login"
                   value="立即查询"
                   onclick="findd();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="查询所有"
                   onclick="findalll();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="清空条件"
                   onclick="clearaaa();"
                   class="btn btn-default btn-sm mar-right-20"/>
        </div>

        <ol class="breadcrumb">
            <li>航空公司信息列表</li>
        </ol>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped font12 table-bordered v-align-top" >
            <thead>
            <tr >
                <th>序号</th>
                <th>公司名称</th>
                <th>航空编码</th>
                <th>公司信息</th>
                <th>地址</th>
                <th>网址</th>
                <th>电话</th>
                <th>操作</th>

            </tr>
            </thead>
            <%!
                int count =1;
            %>
            <%--<c:if test="${sessionScope.adname != null}">--%>
            <c:if test="${find ==1}">

                <c:forEach items="${findlist}" var="ci">

                    <tr>
                        <td><%=count++%></td>
                        <td>${ci.companyName}</td><%--公司--%>
                        <td>${ci.companyCode}</td>
                        <td>${ci.companyInformation}</td>
                        <td>${ci.address}</td>
                        <td>${ci.website}</td>
                        <td>${ci.telephone}</td>
                        <td>
                            <a href="#" class="comopedit">【编辑】</a><br>
                            <a href="#" class="shanchu">【删除】</a><br>
                        </td>
                    </tr>

                </c:forEach>

            </c:if>
            <%-- </c:if>--%>
            <%
            count =1;
            %>
        </table>


        <div class="clearfix"></div>
    </div>
</div>
<!-- main end -->
</div>

<%--编辑信息--%>
<div id="compinfoedit" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">公司名称</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" readonly="readonly" id="ecompname" >
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">航空编码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" readonly="readonly" id="ecode" >
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">公司信息</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="einfo" placeholder="">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">地址</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="eadd" placeholder="">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">网址</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="esite" placeholder="">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">电话</label>
            <input type="text" class="form-control input-sm w200" style=" display: inline-block;" id="ephone" placeholder="">
        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="useredett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>


<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/adminchangpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        /*dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: data.msg,
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>


<script>

    function clearaaa() {
        document.getElementById('name').value="";
    }


    function findalll() {
        dd = {
            "name":"all",
        };
        $.ajax({
            type: "POST",
            url: 'searchcomp',
            async : true,
            contentType:'application/json;charset=utf-8',
            data: JSON.stringify(dd),
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    window.location.href = data.nextUrl;
                }
            }
        });
    }
    function quanxian() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您没有该管理员权限！"+"</div>" ,
            okValue: '确定',
            ok: function () {
            },
        }).showModal();
    }

    function findd() {
        if ($('#name').val() == "") {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"查询条件不能为空！"+"</div>",
                okValue: '确定',
                ok: function () {
                },
            }).showModal();
            return;
        }
        dd = {
            "name":$('#name').val(),
        };
        $.ajax({
            type: "POST",
            url: 'searchcomp',
            async : true,
            contentType:'application/json;charset=utf-8',
            data: JSON.stringify(dd),
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    window.location.href = data.nextUrl;
                }
            }
        });

    }

    function fanhuiii() {
        window.location.href = "toaircraft";
        //window.history.back();
    }

    function vistaaa() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要登录用户系统吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "tologin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }

    function qiehuan() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要切换用户吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "toadminlogin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }


    function useredcheck() {
        if ($('#einfo').val() == '') {
            $('#einfo').focus();
            layer.tips('公司信息不能为空', $("#einfo"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#eadd').val() == '') {
            $('#eadd').focus();
            layer.tips('公司地址不能为空', $("#eadd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#esite').val() == '') {
            $('#eadd').focus();
            layer.tips('公司网址不能为空', $("#eadd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#ephone').val() == '') {
            $('#ephone').focus();
            layer.tips('公司电话不能为空', $("#ephone"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function useredett() {
        if (useredcheck()) {
            dd = {
               // "ecompname":$('#ecompname').val(),
                "ecode":$('#ecode').val(),
                "einfo":$('#einfo').val(),
                "eadd":$('#eadd').val(),
                "esite":$('#esite').val(),
                "ephone":$('#ephone').val(),
            };

            $.ajax({
                type: "POST",
                url: 'compinfoedit',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                                //window.location.href = data.nextUrl;
                            }
                        }).showModal();
                    } else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        //layer.closeAll();
                        /*dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: data.msg,
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }
    }

    $(document).ready(function() {

        $(".comopedit").click(function()
        {

            document.getElementById("ecompname").value = $(this).parents("tr").children("td").eq(1).html();
            document.getElementById("ecode").value = $(this).parents("tr").children("td").eq(2).html();;
            document.getElementById("einfo").value = $(this).parents("tr").children("td").eq(3).html();
            document.getElementById("eadd").value = $(this).parents("tr").children("td").eq(4).html();
            document.getElementById("esite").value = $(this).parents("tr").children("td").eq(5).html();
            document.getElementById("ephone").value = $(this).parents("tr").children("td").eq(6).html();

            layer.open({
                type: 1,
                title: '公司信息修改',
                area: ['400px', 'auto'],
                fix: false,
                //skin: 'layui-layer-rim', //加上边框
                closeBtn: 0, //不显示关闭按钮
                maxmin: false,
                shift: 4, //动画类型
                content: $('#compinfoedit'),
            });

        });


        $(".shanchu").click(function(){
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"删除......"+"</div>",
                ok: function () {}
            }).showModal();
        });
    });




</script>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Utah
  Date: 2017/5/9
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→预订</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!--<link href="../../../js/icheck/skins/all.css" rel="stylesheet" type="text/css">-->
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <!-- 日历控件 -->
    <script language="javascript" type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>
</head>
<body >
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li class="dropdown">
                    <a href="JavaScript:adminn();" >管理员登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，${sessionScope.user} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:loginn();">安全退出</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:toperr();"><i class="icon iconfont icon-nav-block font24" >&#xe620;</i>个人中心</a></li>

            </ul>
        </div><!-- /.navbar-collapse -->

    </div>
</nav>
<!-- header end -->

<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">
    <div class="col-sm-7 container bg-white  pad-bottom-20">
        <!-- 航班信息 -->
        <div style="overflow:hidden;">
            <h3 class="mar-bottom-20" style=" display:block; float:left;">乘机人</h3>
            <a href="" style=" display:block;float:left; margin-left:310px; line-height:70px; ">历史乘机人</a>
        </div>
        <div>
            <input name="input" type="text" class="form-control input-sm w300" style=" display: inline-block;" id="user" value="${user}" readonly="true" placeholder="姓名，与登机所持证件中的姓名一致">
            <select name="select" id="humentype" class="form-control input-sm w150" style=" display: inline-block;">
                <option value="成人" selected="selected">成人</option>
                <option value="儿童">儿童（2-12岁）</option>
                <option value="婴儿">婴儿（14天-2岁）</option>
            </select>
        </div>
        <div class="mar-top-10">
            <select name="select" class="form-control input-sm w150" style=" display: inline-block;">
                <option value="身份证" selected="selected">身份证</option>
            </select>
            <input name="input" type="text" class="form-control input-sm w300" style=" display: inline-block;" id="userid" value="${userid}" readonly="true" placeholder="证件号码">
        </div>

        <div class=" mar-top-10">
            <input type="button" value="添加乘机人" class="btn btn-danger zw2" >
        </div>

        <!-- 航班信息 结束 -->

        <!-- 保险 -->
        <div style="position:relative;">
            <h3 class="mar-bottom-20">保险</h3>
            <a href="" class="mar-right-10 safe">航意险</a>
            <select name="select" class="form-control input-sm w150 mar-right-10" style=" display: inline-block;">
                <option value="保险" selected="selected">￥30/份X1</option>

            </select>
            <span>保额￥260万/份</span>

            <!--航意险说明-->
            <div style="width:460px; border:1px solid #C4C4C4; padding:5px; position:absolute; left:0px; top:75px; background-color:#eee;" class="explain">
                <h5>航意险说明</h5>
                <p>1.保险费：<i class="rmb">¥</i>30/份。保险金额及保险责任：<i class="rmb">¥</i>260万/份。航空意外险由太平财产保险有限公司深圳分公司承保。</p>
                <p>2.保险购买即生效，不可单独退保。机票退款后，自动退保。机票改签后，保险责任保护到改签后航班。提供保险定额发票作为报销凭证。</p>
            </div>
        </div>

        <!-- 联系人 -->
        <h3 class="mar-bottom-20">联系人</h3>
        <input name="input" type="text" class="form-control input-sm" style=" display: inline-block; width:460px;" id="linkuser" value="${user}" readonly="true" placeholder="姓名">
        <div class="mar-top-10">
            <select name="select" id="phonehead" class="form-control input-sm w110 mar-right-10" style=" display: inline-block;">
                <option value="国家" selected="selected">中国&nbsp;&nbsp;+86</option>
            </select>
            <input name="input" type="text" class="form-control input-sm" style=" display: inline-block; width:337px;" id="phone" value="${userPhone}" readonly="true" placeholder="手机号码，接收航班信息">
        </div>


        <!-- 报销 -->
        <h3 class="mar-bottom-20">报销</h3>
        <input type="checkbox" name="choice">&nbsp;需要报销凭证<span style=" color:#999;">（含：行程单和差额发票，快递费发票）</span>
    </div>
    <div class="col-sm-5 bg-gray-eee">
        <div style=" margin-left:-15px;">
            <h4 style="overflow:hidden; margin-left:10px;">
                <span style="display:block; float:left;">${sdaye}&nbsp;&nbsp;周${sweek}</span>
                <span style="display:block; float:left; margin-left:82px;">&nbsp;&nbsp;${tikit.startPlace}——${tikit.landPlace}</span>
                <%--<span style=" display:block; float:right;color:#999; font-size:14px; margin-right:5px;">${ldaye}&nbsp;&nbsp;周${lweek}</span>--%>
                <span style="display:block; float:left; margin-left:75px;">${ldaye}&nbsp;&nbsp;周${lweek}</span>
            </h4>
        </div>
        <div style=" overflow:hidden; margin-bottom:20px;">
            <div class="flight-from" style="float:left;">
                <span class="time text-center">${startTime}</span><br/>
                <span class="text-center">${tikit.statAirport}</span>
            </div>
            <div class="flight-info" style="float:left; margin-left:100px;">
                <%--<img src="<%=basePath%>/static/img/a.png">
                <span class="font12 gray-999">xxxxkm</span>--%>
                <img src="<%=basePath%>/static/img/b.png"><br/>
                <span class="text-center font12 gray-999 mar-left-12">${airName} ${tikit.flightNumber}</span><br/>
                <img src="<%=basePath%>/static/img/c.png" class="mar-left-12">
                <span class="text-center font12 gray-999">${ctype}</span><br/>
                <span class="airline-direct"></span>
                <span class="arrow"></span>
            </div>
            <div class="flight-to" style="float:right;">
                <span class="time text-center">${landTime}</span><br/>
                <span class="text-center">${tikit.landAirport}</span>
            </div>
        </div>
        <div>
            <div class="tuigaiqian" style="cursor:pointer; color:#337ab7;">退改签说明</div>
            <div class="instruction">
                <table class="table table-bordered" style="height:150px;">
                    <tr>
                        <td>成人票</td>
                        <td>退票扣费</td>
                        <td>改期加收手续费</td>
                        <td>签转</td>
                    </tr>
                    <tr>
                        <td>起飞前24小时之前</td>
                        <td>￥169/人</td>
                        <td>￥368/人</td>
                        <td>可以签转</td>
                    </tr>
                    <tr>
                        <td>起飞前24小时之后</td>
                        <td>￥338/人</td>
                        <td>￥368/人</td>
                        <td>可以签转</td>
                    </tr>
                </table>
            </div>
        </div>

        <div style=" margin-left:-15px; overflow:hidden;">
            <h4 class="mar-left-10" style="display:block; float:left;">订单总额</h4>
            <h4 class="red" style=" display:block; float:right; font-weight:bold;">￥${price +80}</h4>
        </div>
        <table class="table">
            <tr>
                <td>成人票</td>
                <td>￥${price}/人</td>
                <td>x1</td>
            </tr>
            <tr>
                <td>机建费</td>
                <td>￥50/成人</td>
                <td>x1</td>
            </tr>
            <tr>
                <td>航意险</td>
                <td>￥30/人</td>
                <td>x1</td>
            </tr>
        </table>
        <c:if test="${sessionScope.chajia != 0}">
            <div style=" margin-left:-15px; overflow:hidden;">
                <h3 class="mar-left-10" style="display:block; float:left;">改签差价</h3>
                <c:choose>
                    <c:when test="${sessionScope.chajia < (price +80)}">
                        <h3 class="red" style=" display:block; float:right; font-weight:bold;">￥${(price +80) - sessionScope.chajia}</h3>
                    </c:when>
                    <c:otherwise>
                        <h3 class="red" style=" display:block; float:right; font-weight:bold;">￥0</h3>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:if>

        <div class="order-discount">
        </div>
    </div>
</div>

<div>
    <input id="chajia" type="hidden" value="${sessionScope.chajia}"></input>
</div>

<div class="text-center mar-top-10">
    <input type="checkbox" id="xieyi" name="choice">&nbsp;我已阅读并接受免责条款、费用扣除、退保等在内的重要事项，其中包括 <a href="">《网络电子客票协议》</a> <a href="">《航意险说明》</a> <a href="">《延误险说明》</a> <a href="">《保险经纪委托协议》</a><br/>

    <c:choose>
        <c:when test="${sessionScope.chajia != 0}">
            <c:choose>
                <c:when test="${sessionScope.chajia < (price +80)}">
                    <input type="button" value="改签" class="btn btn-danger mar-top-20" onclick="buchajia();" >
                </c:when>
                <c:otherwise>
                    <input type="button" value="改签" class="btn btn-danger mar-top-20" onclick="gaiqian();" >
                </c:otherwise>
            </c:choose>
        </c:when>

        <c:otherwise>
            <input type="button" value="提交订单" class="btn btn-danger mar-top-20" onclick="bye();" >
        </c:otherwise>

    </c:choose>

</div>



<!-- 添加乘机人弹框 -->
<div id="rev" style="display:none">
    <div class="modal-body" >
        <div>
            <input name="input" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="姓名">
            <select name="select" class="form-control input-sm w150" style=" display: inline-block;">
                <option value="成人" selected="selected">成人</option>
                <option value="儿童">儿童（2-12岁）</option>
                <option value="婴儿">婴儿（14天-2岁）</option>
            </select>
        </div>
        <div class="mar-top-10">
            <select name="select" class="form-control input-sm w150" style=" display: inline-block;">
                <option value="身份证" selected="selected">身份证</option>
            </select>
            <input name="input" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="证件号码">
        </div>
    </div>

    <div class="modal-footer mar-top-5">
        <button type="button" class="btn btn-primary" onClick="">确认添加</button>
        <button type="button" class="btn btn-default" onClick="">取消</button>
    </div>
</div>
<div>
    <input id="airName" type="hidden" value="${airName}"></input>
    <input id="flightNumber" type="hidden" value="${tikit.flightNumber}"></input>
    <input id="startTime" type="hidden" value="${tikit.startTime}"></input>
    <input id="ctype" type="hidden" value="${ctype}"></input>
    <input id="price" type="hidden" value="${price + 80}"></input>
</div>

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5">
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/changpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: <div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: <div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            ok: function () {}
                        }).showModal();
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>

<script type="text/javascript">

    function gaiqian() {

        var startTime = $("#startTime").val();
        var flightNumber = $("#flightNumber").val();
        var ctype = $("#ctype").val();
        var price = $("#price").val();

        var datat = {
            "flightNumber": flightNumber,
            "ctype": ctype,
            "startTime": startTime,
            "price": price,

        };

        $.ajax({
            type: "POST",
            url: 'paye/gaiqiannocj',
            async : true,
            contentType:'application/json;charset=utf-8',
            data: JSON.stringify(datat),
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '确定',
                        ok: function () {
                            window.location.href = "topersonal";
                        },
                        cancel:false,
                        button: [
                            {
                                value: '取消',
                                callback: function () {
                                    window.location.href = "tohome";
                                },
                            }
                        ]
                    }).showModal();
                } else {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '返回首页',
                        ok: function () {
                            window.location.href = "tohome";
                        }
                    }).showModal();
                }
            }
        });

    }

    function buchajia() {
        var cha = $("#chajia").val();
        var phone = $("#phone").val();
        var startTime = $("#startTime").val();
        var airName = $("#airName").val();
        var flightNumber = $("#flightNumber").val();
        var ctype = $("#ctype").val();
        var price = $("#price").val();
        var chajia = price -cha;
        var humentype = $('#humentype option:selected') .val();

        window.location.href = "topaye?&phone="+phone+"&airName="+airName+"&flightNumber="+flightNumber+
            "&ctype="+ctype+"&price="+price+"&chajia="+chajia+"&humentype="+humentype;

    }


    function adminn() {

            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要登录管理员系统吗？"+"</div>"  ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "toadminlogin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();


    }

    function toperr () {
        window.location.href = "topersonal";
    }

    function loginn() {
        window.location.href = "tologin";
    }

    function bye() {

        if(!$('#xieyi').is(':checked')) {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"请先同意相关协议！"+"</div>"   ,
                okValue: '确定',
                ok: function () {
                },

            }).showModal();
            return false;
        }


        var chajia = $("#chajia").val();
        var phone = $("#phone").val();
        var startTime = $("#startTime").val();
        var airName = $("#airName").val();
        var flightNumber = $("#flightNumber").val();
        var ctype = $("#ctype").val();
        var price = $("#price").val();
        var humentype = $('#humentype option:selected') .val();

        var datat = {
            "flightNumber": flightNumber,
            "ctype": ctype,
            "startTime": startTime,
            "price": price,

        };

        $.ajax({
            type: "POST",
            url: 'paye/payeoder',
            async : true,
            contentType:'application/json;charset=utf-8',
            data: JSON.stringify(datat),
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"，是否立即支付？</div>",
                        okValue: '确定',
                        ok: function () {
                            window.location.href = data.nextUrl+"?&phone="+phone+"&airName="+airName+"&flightNumber="+flightNumber+
                                "&ctype="+ctype+"&price="+price+"&chajia="+chajia+"&humentype="+humentype;
                        },
                        cancel:false,
                        button: [
                            {
                                value: '取消',
                                callback: function () {
                                    window.location.href = "topersonal";
                                },
                            }
                        ]
                    }).showModal();
                } else {
                    dialog({
                        width: '20em',
                        title: '提示...',
                        cancel:false,
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '返回首页',
                        ok: function () {
                            window.location.href = "tohome";
                        }
                    }).showModal();
                }
            }
        });
    }

</script>

<script type="text/javascript">

    $(function() {
        $('.bubbleinfo').each(function() {
            var distance = 10;
            var time = 10;
            var hideDelay = 100;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);

            $([trigger.get(0), info.get(0)]).mouseover(function() {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 30,
                        left:0,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function() {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function() {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function() {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });


</script>





<%--<script>
    $(document).ready(function() {


        $('.zw2').on('click', function(){  //添加乘机人弹框
            layer.open({
                type: 1,
                title: '添加乘机人',
                area: ['400px', 'auto'],
                fix: false, //不固定
                maxmin: false,
                content: $('#rev'),
            });
        });

    });
</script>--%>

<script>
    $('.explain').hide();
    $('.safe').mouseenter(
        function(){
            $('.explain').show();
        }
    ).mouseleave(
        function(){
            $('.explain').hide();
        }
    )

    $('.instruction').hide();
    $('.tuigaiqian').click(
        function(){
            $('.instruction').toggle();
        }
    )
</script>




</body>
</html>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→注册用户信息</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>

    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li>
                    <a href="JavaScript:vistaaa();">用户登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，用户管理员-${sessionScope.adname} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:change();">切换用户</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:fanhui();"><span class="glyphicon glyphicon-cog font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px; margin-bottom:2px;"></span>返回</a></li>
                <li class="active"><a href="#"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i> 用户管理</a></li>
                <li ><a href="JavaScript:quanxian();"><span class="glyphicon glyphicon-yen font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px;  margin-bottom:2px;"></span>账务管理</a></li>
                <li><a href="JavaScript:quanxian();"><i class="icon iconfont icon-nav-block font24"  >&#xe623;</i>航班管理</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>查询条件</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">用户姓名</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="xm" placeholder="空" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group"  >
                    <label for="apdiv" class="w90 text-right">手机</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sj" placeholder="空" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">邮箱</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="yx" placeholder="空" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">身份证</label>
                    <input type="text" class="form-control input-sm" style=" width:200px; display:inline-block;" id="sfz" placeholder="空" >
                </div>
            </div>
        </div>
        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">

            <input type="button"
                   id="btn_login2"
                   value="立即查询"
                   onclick="find();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login1"
                   value="查询所有"
                   onclick="findall();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="清空条件"
                   onclick="clearw();"
                   class="btn btn-default btn-sm mar-right-20"/>
        </div>

        <ol class="breadcrumb">
            <li>注册用户信息列表</li>
        </ol>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped font12 table-bordered v-align-top" >
            <thead>
            <tr >
                <th>序号</th>
                <th>姓名</th>
                <th>证件号码</th>
                <th>手机</th>
                <th>邮箱</th>
                <th>类型</th>
                <th>操作</th>
            </tr>
            </thead>
            <%!
            int count =1;
            %>
            <%--<c:if test="${sessionScope.adname != null}">--%>
            <c:if test="${find ==1}">
                <c:forEach items="${pilist}" var="pi">
                    <tr>
                        <td><%=count++%></td>
                        <td>${pi.passengersName}</td>
                        <td>${pi.idNumber}</td>
                        <td>${pi.passengersPhone}</td>
                        <td>${pi.passengersEmail}</td>
                        <td>注册用户</td>
                        <td>
                            <a href="#" class="shanchu">【删除】</a><br>
                        </td>

                    </tr>
                </c:forEach>
            </c:if>
            <%-- </c:if>--%>
            <%
                count =1;
            %>
        </table>

        <div class="clearfix"></div>
    </div>
</div>
<!-- main end -->
</div>

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>
<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/adminchangpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        /*dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: data.msg,
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>

<script>

    function cheackit() {

        var sqq = /^1[34578]\d{9}$/;
        if (!sqq.test($('#sj').val())
            || $('#sj').val().length < 11
            || $('#sj').val().length > 14
            || $('#sj').val() == "") {

            $('#sj').focus();
            layer.tips('手机格式不正确', $("#sj"),{bg: '#AE81FF',time: 2000});
            return false;
        }

        var mail = /^([a-z0-9A-Z]+[-|_|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
        if (!mail.test($('#yx').val())
            || $('#yx').val().length < 11
            || $('#yx').val().length > 30
            || $('#yx').val() == "") {
            $('#yx').focus();
            layer.tips('邮箱格式不正确', $("#yx"),{bg: '#AE81FF',time: 2000});
            return false;
        }

        var cardid = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
        if (!cardid.test($('#sfz').val())
            || $('#sfz').val().length < 14
            || $('#sfz').val().length > 19
            || $('#sfz').val() == "") {
            $('#sfz').focus();
            layer.tips('身份证格式不正确', $("#sfz"),{bg: '#AE81FF',time: 2000});
            return false;
        }

        return true;
    };

    function clearw() {
        document.getElementById('xm').value="";
        document.getElementById('sj').value="";
        document.getElementById('yx').value="";
        document.getElementById('sfz').value="";
    }

    function find() {
        if ($('#xm').val() == "" &&$('#sj').val()==""&&$('#yx').val()==""&&$('#sfz').val()=="") {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"查询条件不能为空！"+"</div>",
                okValue: '确定',
                ok: function () {
                },
            }).showModal();
            return;
        }
        dd = {
            "xm":$('#xm').val(),
            "sj":$('#sj').val(),
            "yx":$('#yx').val(),
            "sfz":$('#sfz').val(),
        };
        $.ajax({
            type: "POST",
            url: 'reggerfindby',
            async : true,
            contentType:'application/json;charset=utf-8',
            data: JSON.stringify(dd),
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    window.location.href = data.nextUrl;
                }else {
                    dialog({
                        width: '20em',
                        cancel:false,
                        title: '提示...',
                        content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                        okValue: '确定',
                        ok: function () {
                        },
                    }).showModal();
                }
            }
        });

    }

    function findall() {

        $.ajax({
            type: "POST",
            url: 'reggerfindall',
            async : true,
            contentType:'application/json;charset=utf-8',
            dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
            cache: false,
            success: function (data) {
                if (data.code == 1) {
                    window.location.href = data.nextUrl;
                }
            }
        });
    }


    function vistaaa() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要登录用户系统吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "tologin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }


    function fanhui() {
        window.location.href = "touser";

    }

    function change() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要切换用户吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "toadminlogin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }

    function shouye() {
        window.location.href = "tohome";
    }
    function quanxian() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您没有该管理员权限！"+"</div>" ,
            okValue: '确定',
            ok: function () {
            },
        }).showModal();
    }
    $(document).ready(function() {

        $(".shanchu").click(function(){
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"删除......"+"</div>",
                ok: function () {}
            }).showModal();
        });

    });
</script>
</body>
</html>

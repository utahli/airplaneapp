<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    request.setAttribute("domain", "http://localhost:8080/AirPleanApp/");
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
    java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");

    java.util.Date currentTime = new java.util.Date();//得到当前系统时间

    String str_date1 = formatter.format(currentTime); //将日期时间格式化


%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>

<script type="text/javascript" >


    function admin() {
        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要登录管理员系统吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "toadminlogin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();

    }

    function login() {
        window.location.href = "tologin";
    }

    function toper() {
        if($("#uuu").val() == ""){
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"请先登录..."+"</div>",
                okValue: '登录',
                ok: function () {
                    window.location.href = "tologin";
                },
                button: [
                    {
                        value: '取消',
                    }]
            }).showModal();
        }else {
            window.location.href = "topersonal";
        }

    }

    function Search() {

            var to = $('#toselect option:selected') .val();
            var ftime = $("#ftime").val();
            var fff=$('#fromselect option:selected') .val();
            var query = {
                "from": fff,
                "to": to,
                "ftime":ftime
            };
            var tbody = "#airlist";

        $.ajax({
                type: "POST",
                url: 'saerch/query',
                async : true,
                contentType:'application/json;charset=utf-8',
                data: JSON.stringify(query),
                dataType: 'json',
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        $(tbody).html("");
                        $.each(data.data,function (index,item) {
                            OutPutBody(tbody, item);
                        });
                    } else {
                        $(tbody).html("");
                        dialog({
                            width: '20em',
                            cancel:false,
                            title: '提示...',
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {}
                        }).showModal();
                    }
                }
            });

        function isContains(str, substr) {
            return str.indexOf(substr) >= 0;
        }

        //var hrefj="<%=basePath%>/WEB-INF/page/book.jsp?ss=saa";
        function OutPutBody(tbody, item) {

            var a = item.flightNumber;
            var pug;
            var airName;
            switch (true) {

                case isContains(a,"3U"):
                    pug = "3U.png";
                    airName = "川航";
                    break;
                case isContains(a,"8L"):
                    pug = "8L.png";
                    airName = "祥鹏航空";
                    break;
                case isContains(a,"BK"):
                    pug = "BK.png";
                    airName = "奥凯航空";
                    break;
                case isContains(a,"CA"):
                    pug = "CA.png";
                    airName = "国航";
                    break;
                case isContains(a,"CN"):
                    pug = "CN.png";
                    airName = "新华航空";
                    break;
                case isContains(a,"CZ"):
                    pug = "CZ.png";
                    airName = "南航";
                    break;
                case isContains(a,"DR"):
                    pug = "DR.png";
                    airName = "瑞丽航空";
                    break;
                case isContains(a,"DZ"):
                    pug = "DZ.png";
                    airName = "东海航空";
                    break;
                case isContains(a,"EU"):
                    pug = "EU.png";
                    airName = "成都(鹰联)航空";
                    break;
                case isContains(a,"FU"):
                    pug = "FU.png";
                    airName = "福州航空";
                    break;
                case isContains(a,"GJ"):
                    pug = "GJ.png";
                    airName = "长龙航空";
                    break;
                case isContains(a,"GS"):
                    pug = "GS.png";
                    airName = "天津航空";
                    break;
                case isContains(a,"GY"):
                    pug = "GY.png";
                    airName = "多彩贵州航空";
                    break;
                case isContains(a,"HO"):
                    pug = "HO.png";
                    airName = "吉祥航空";
                    break;
                case isContains(a,"HU"):
                    pug = "HU.png";
                    airName = "海航";
                    break;
                case isContains(a,"JD"):
                    pug = "JD.png";
                    airName = "首都(金鹿)航空";
                    break;
                case isContains(a,"JR"):
                    pug = "JR.png";
                    airName = "幸福航空";
                    break;
                case isContains(a,"KN"):
                    pug = "KN.png";
                    airName = "联航";
                    break;
                case isContains(a,"KY"):
                    pug = "KY.png";
                    airName = "昆航";
                    break;
                case isContains(a,"MF"):
                    pug = "MF.png";
                    airName = "厦航";
                    break;
                case isContains(a,"MU"):
                    pug = "MU.png";
                    airName = "东航";
                    break;
                case isContains(a,"NS"):
                    pug = "NS.png";
                    airName = "河北航空";
                    break;
                case isContains(a,"PN"):
                    pug = "PN.png";
                    airName = "西部航空";
                    break;
                case isContains(a,"QW"):
                    pug = "QW.png";
                    airName = "青岛航空";
                    break;
                case isContains(a,"SC"):
                    pug = "SC.png";
                    airName = "山东航空";
                    break;
                case isContains(a,"TV"):
                    pug = "TV.png";
                    airName = "西藏航空";
                    break;
                case isContains(a,"UQ"):
                    pug = "UQ.png";
                    airName = "乌鲁木齐航空";
                    break;
                case isContains(a,"Y8"):
                    pug = "Y8.png";
                    airName = "扬子江快运航空";
                    break;
                case isContains(a,"YI"):
                    pug = "YI.png";
                    airName = "英安航空";
                    break;
                case isContains(a,"ZH"):
                    pug = "ZH.png";
                    airName = "深航";
                    break;
                case isContains(a,"G5"):
                    pug = "G5.png";
                    airName = "华夏航空";
                default:
                    break;
            }

            var startTime = new Date(item.startTime);
            var landTime = new Date(item.landTime);


            /*var hrefec="tobook?flightNumber="+item.flightNumber+"&airName="+airName+"&startTime="+item.startTime+
                "&landTime="+item.landTime+"&from="+fff+"&to="+to+"&statAirport="+item.statAirport+"&landAirport="+item.landAirport+"&ctype=econ"+
                "&price="+item.economicPrice*0.9;

            var hrefeb="tobook?flightNumber="+item.flightNumber+"&airName="+airName+"&startTime="+item.startTime+
                "&landTime="+item.landTime+"&from="+fff+"&to="+to+"&statAirport="+item.statAirport+"&landAirport="+item.landAirport+"&ctype=busin"+
                "&price="+item.businessPrice*0.9;*/
            var hrefec="tobook?flightNumber="+item.flightNumber+"&airName="+airName+"&ctype=econ";

            var hrefeb="tobook?flightNumber="+item.flightNumber+"&airName="+airName+"&ctype=busin";

            $(tbody).append("<ul class='list-inline bor-bottom-solid-1  '>"+
                "<li class='w-percentage-25'><img src='<%=basePath%>/static/img/air/"+pug+"' width='24' height='25'> <strong>"+ airName+
                    "</strong>"+item.flightNumber+"<span class='gray-999 font12 mar-left-10'>"+item.flightType+"</span></li>"+
                    "<li class='text-right w80'> <strong class='time '>"+startTime.getHours()+":"+startTime.getMinutes()+"</strong></li>"+
                    "<li class=''> —— </li>"+
                    "<li class='w80'> <strong class='time '>"+landTime.getHours()+":"+landTime.getMinutes()+"</strong></li>"+
                    "<li class='w150 text-right'> "+item.statAirport+"</li>"+
                    "<li class=''> —— </li>"+
                    "<li class='w150'>" +item.landAirport+"</li>"+
                    "<li class='w-percentage-20 text-center'> <span class='gray-999 '>"+"机建 + 燃油："+"</span><span class='gray-999 rmb font12'>"+"￥50 + ￥20"+"</span></li>"+
                    "</ul>"+
                    "<div class='collapse' id='collapseExample' style=' display:block;'>"+
                    "<div class='hangbanlist-body ' style=' background-color:#FEFCFC;'>"+
                    "<ul class='list-inline'>"+
                    "<li class='w-percentage-20'><strong class='blue-0093dd'>经济舱</strong></li>"+
                    "<li  class='w-percentage-25'>座位数："+item.economicNumber+"</li>"+
                    "<li  class='w-percentage-25'>票面价：<span class='rmb'>￥"+item.economicPrice+"</span></li>"+
                    "<li  class='w-percentage-20 '>优惠价：<strong class='rmb orange-f60 font16'>￥"+parseInt(item.economicPrice*parseInt(item.discountRate)/100)+"</strong> <span class='gray font12'>"+item.discountRate/10+"折</span></li>"+
                    '<li class="pull-right "><button type="button" class="btn btn-danger btn-sm" onClick="window.location.href ='+"'"+hrefec+"'"+';">订票</button></li>'+
                    /*'<li class="pull-right "><button type="button" class="btn btn-danger btn-sm" onClick="window.open('+"'"+hrefec+"'"+');">订票</button></li>'+*/
                    "</ul>"+
                    "<ul class='list-inline'>"+
                    "<li class='w-percentage-20'><strong class=' red'>头等舱</strong></li>"+
                    "<li  class='w-percentage-25'>座位数："+item.businessNumber+"</li>"+
                    "<li  class='w-percentage-25'>票面价：<span class='rmb'>￥"+item.businessPrice+"</span></li>"+
                    "<li  class='w-percentage-20 '>优惠价：<strong class='rmb orange-f60 font16'>￥"+parseInt(item.businessPrice*parseInt(item.discountRate)/100)+"</strong><span class='gray font12'>"+item.discountRate/10+"折</span></li>"+
                    '<li class="pull-right "><button type="button" class="btn btn-danger btn-sm" onClick="window.location.href ='+"'"+hrefeb+"'"+';">订票</button></li>'+

 /*
                    '<li class="pull-right "><button type="button" class="btn btn-danger btn-sm" onClick="window.open('+"'"+hrefeb+"'"+');">订票</button></li>'+
*/
                    "</ul>"+
                    "</div>"+
                    "</div>"
            );
        }

    }

</script>

<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→首页</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!--<link href="../../../js/icheck/skins/all.css" rel="stylesheet" type="text/css">-->
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <!-- 日历控件 -->
    <script language="javascript" type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <span style=" margin-left:-15px; margin-right:20px;">企业商旅管理</span>-->
            </div>

            <c:choose>
                <c:when test="${sessionScope.user != null}">
                    <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                        <li class="dropdown">
                            <a href="JavaScript:admin();" >管理员登录</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，${sessionScope.user} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                                <li><a href="JavaScript:login();">安全退出</a></li>
                            </ul>
                        </li>
                    </ul>
                </c:when>

                <c:otherwise>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                    <li><a href="JavaScript:admin();" >管理员登录</a></li>
                    <li><a href="JavaScript:login();" >用户登录</a></li>
            </ul>
                </c:otherwise>

            </c:choose>
            <%--<c:if test="${sessionScope.user}">

            </c:if>--%>

        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!--
                        <li  class="active"><a href="查询列表.html"><i class="icon iconfont icon-nav-block font24" >&#xe620;</i> 在线预订</a></li>
                -->
                <li><a href="JavaScript:toper();"><i class="icon iconfont icon-nav-block font24" >&#xe620;</i>个人中心</a></li>
               <%-- <li ><a href="JavaScript:toper();"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i>个人中心</a></li>--%>

            </ul>
        </div><!-- /.navbar-collapse -->

    </div>
</nav>

<div>
    <input id="uuu" type="hidden" value="${sessionScope.user}"></input>
</div>

<!-- header end -->
<!-- 搜索 -->
<div class="index-wall white " style=" ">
    <div class="container" style=" position:relative;">
        <div style="text-align:center;">
        <form class="form-inline" action="" method="post" accept-charset="utf-8">
            <div class="form-group">
                <select name="" disabled class="form-control">
                    <option selected>单程</option>
                    <option>往返</option>
                    <option>多程</option>
                </select>
            </div>
            <div class="form-group mar-left-10">
                <label for="">出发城市</label>
                <%--<input type="text" class="form-control" style="width:85px;" id="from" name='from' value="成都" placeholder="">--%>
                <select id = 'fromselect' class="form-control">
                    <option value="长春" selected>长春</option>
                    <option value="三亚">三亚</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                    <option value="广州">广州</option>
                    <option value="深圳">深圳</option>
                    <option value="成都">成都</option>
                </select>
            </div>
            <div class="form-group">
                <label for=""> &nbsp <a href="#" class="huan"></a> &nbsp </label>
            </div>
            <div class="form-group">
                <label for="">到达城市</label>
                <%--<input type="text" class="form-control" style="width:85px;" id="to" name='to' value="北京" placeholder="">--%>
                <select id = 'toselect' class="form-control">
                    <option value="三亚" selected>三亚</option>
                    <option value="长春">长春</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                    <option value="广州">广州</option>
                    <option value="深圳">深圳</option>
                    <option value="成都">成都</option>
                </select>
            </div>
            <div class="form-group mar-left-10">
                <label for="">出发日期</label>
                <input type="text" class="form-control " style="width:110px;" id="ftime" name='ftime' value="<%=str_date1%>" placeholder="" onClick="WdatePicker()">
            </div>
            <div class="form-group mar-left-10">
                <label for="">返回日期</label>
                <input type="text" disabled class="form-control " id="" style="width:110px;" onClick="WdatePicker()" value="<%=str_date1%>" placeholder="">
            </div>
            <div class="form-group mar-left-10">
                <select id="dropAirlines" disabled class="form-control" style=" width:120px;">
                    <option value="">国内航班</option>
                    <option value="">国际航班</option>
                    <%--<option value="3U">3U-川航</option>
                    <option value="8C">8C-东星</option>
                    <option value="8L">8L-祥鹏</option>
                    <option value="9C">9C-春秋</option>
                    <option value="BK">BK-奥凯</option>
                    <option value="CA">CA-国航</option>
                    <option value="CN">CN-新华</option>
                    <option value="CZ">CZ-南航</option>
                    <option value="DR">DR-瑞丽航空</option>
                    <option value="DZ">DZ-东海</option>
                    <option value="EU">EU-成都(鹰联)</option>
                    <option value="FM">FM-上航</option>
                    <option value="FU">FU-福州航空</option>
                    <option value="G5">G5-华夏</option>
                    <option value="GJ">GJ-长龙航空</option>
                    <option value="GS">GS-天津</option>
                    <option value="GX">GX-广西航空</option>
                    <option value="GY">GY-多彩贵州航空</option>
                    <option value="HO">HO-吉祥</option>
                    <option value="HU">HU-海航</option>
                    <option value="JD">JD-首都(金鹿)</option>
                    <option value="JR">JR-幸福</option>
                    <option value="KN">KN-联航</option>
                    <option value="KY">KY-昆航</option>
                    <option value="MF">MF-厦航</option>
                    <option value="MU">MU-东航</option>
                    <option value="NS">NS-河北</option>
                    <option value="PN">PN-西部</option>
                    <option value="QW">QW-青岛</option>
                    <option value="SC">SC-山东</option>
                    <option value="TV">TV-西藏</option>
                    <option value="UQ">UQ-乌鲁木齐航空</option>
                    <option value="VD">VD-鲲鹏</option>
                    <option value="Y8">Y8-扬子江快运航空</option>
                    <option value="YI">YI-英安</option>
                    <option value="ZH">ZH-深航</option>--%>
                </select>
            </div>
            &nbsp;
            <%--<button type="submit" class="btn btn-warning mar-left-10">搜索</button>--%>
            <input type="button"
                   id="btn_login"
                   value="搜索"
                   onclick="Search();"
                   class="btn btn-warning mar-left-10"/>
        </form>
        </div>
    </div>
</div>


<!-- 列表开始 -->
<div class="container mar-bottom-30 ">
    <div class="hangbanlist">
        <div id = "airlist">

        </div>



    </div>




</div>
<!-- 列表结束 -->

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/changpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            ok: function () {}
                        }).showModal();
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>
</body>
</html>


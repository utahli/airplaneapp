<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<!-- IE 浏览器运行最新的渲染模式-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- 启用响应式特性 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 双核使用webkit内核 -->
<meta name="renderer" content="webkit">
<title>航空票务系统→支付</title>
<link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
<link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
<link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">

<link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>/static/js/bootstrap.min.js"></script>
<!-- 兼容IE8 -->
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=basePath%>/static/js/html5shiv.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/respond.min.js"></script>
<![endif]-->
<!-- layer弹框 2.1 -->
<script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
<!-- 日历控件 -->
<script language="javascript" type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>

<!-- TAB -->
<script type="text/javascript" src="<%=basePath%>/static/js/base.js"></script>
<script>
$(document).ready(function() {

	$("#bank-change li").first().prepend("<span class='ico-bank-change'></span>");
    $("#bank-change li").click(function(){
		$(this).prepend("<span class='ico-bank-change'></span>");
		$(this).siblings().children("span.ico-bank-change").remove();
		$(this)
		});
    $("#bank-change2 li").click(function(){
		$(this).prepend("<span class='ico-bank-change'></span>");
		$(this).siblings().children("span.ico-bank-change").remove();
		$(this)
		})
	   
	
	
	
});						   
</script>
</head>
<body >
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li class="dropdown">
                    <a href="JavaScript:guanli();" >管理员登录</a>
                </li>
                <li class="dropdown">
                    您好，${sessionScope.user}
                    <%--<ul class="dropdown-menu">
                        <li><a href="../系统管理/修改密码.html">修改密码</a></li>
                        <li><a href="JavaScript:tuichu();">安全退出</a></li>
                    </ul>--%>
                </li>
            </ul>

        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:toperrr();"><i class="icon iconfont icon-nav-block font24" >&#xe620;</i>个人中心</a></li>

            </ul>
        </div><!-- /.navbar-collapse -->

    </div>
</nav>
<!-- header end -->

<div class="container bg-white box-shadow pad-15 mar-bottom-30" style="margin-top:120px;">
<!-- 航班信息 -->
  <h3 class="mar-bottom-20">航班信息 <span class="label label-primary font12 mar-left-10" style=" vertical-align:middle;">单程</span></h3>
  <div class="lh30 pad-10 bg-gray-f5">
    <ul class="list-inline ">
      <li class="w-percentage-20"><b class="font16">${tikit.startPlace} — ${tikit.landPlace}</b></li>
      <li class="w-percentage-15">${airName} ${tikit.flightNumber}</li>
      <li class="w-percentage-15">${ctype}</li>
      <li class="w-percentage-20"><b class="font16">${tikit.startTime}</b> 起飞</li>
      <li class="w-percentage-20"><b class="font16">${tikit.landTime}</b> 到达</li>
    </ul>
  </div>
<!-- 航班信息 结束 -->

<!-- 旅客信息 -->
  <h3 class="mar-bottom-20">订单信息 <small class="mar-left-20 font14">乘客人数：1 人</small></h3>
  <div class="lh30 ">
    <div class=" bor-bottom-solid-d8-1 bor-top-solid-1">
      <ul class="list-inline ">
        <li class="w-percentage-25">${humentype}1：${user}</li>
        <li class="w-percentage-25">身份证：${userid}</li>
        <li class="">手机：${phone}</li>
      </ul>
      <ul class="list-inline ">
        <li class="w-percentage-25">结算价：${price - 80}</li>
        <li class="w-percentage-25">机建/燃油/服务费：50/0/0</li>
        <li class=""><b>中国人保 30元旅游航意险（10天）</b> 30元</li>
          <c:choose>
              <c:when test="${chajia != 0}">
                  <li class="pull-right">改签差价：<b>${chajia}</b></li>
              </c:when>
              <c:otherwise>
                  <li class="pull-right">合计：<b>${price}</b></li>
              </c:otherwise>
          </c:choose>
      </ul>
    </div>

      <c:choose>
          <c:when test="${chajia != 0}">
              <div class=" pull-right mar-top-10" >支付金额：<span class="rmb orange-f60">￥</span><span class=" orange-f60 text-right font24">${chajia}</span></div>
          </c:when>
          <c:otherwise>
              <div class=" pull-right mar-top-10" >支付金额：<span class="rmb orange-f60">￥</span><span class=" orange-f60 text-right font24">${price}</span></div>
          </c:otherwise>
      </c:choose>


    <div class="clearfix"></div>
  </div>

<!-- 旅客信息 结束 -->

<!-- 支付选择 -->
<div class="tabbable" >
  <ul class="nav nav-tabs mar-bottom-20">
  <li class="active"><a href="#tab2" data-toggle="tab">第三方支付</a></li>
  <li ><a href="#tab1" data-toggle="tab" >银行卡</a></li>
  <!--<li><a href="#tab3" data-toggle="tab">pos机信息维护</a></li>-->
</ul>

 <!--list 01-->
 <div class="tab-content">
  <div class="tab-pane bank-block-list2" id="tab1">
   <ul id="bank-change" style=" width:90%; margin-left:auto; margin-right:auto;">
    <li value="中国农业银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/1.gif"></li>
    <li value="中国建设银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/2.gif"></li>
    <li value="中国光大银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/3.gif"></li>
    <li value="中国民生银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/4.gif"></li>
    <li value="交通银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/5.gif"></li>
    <li value="广东发展银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/6.gif"></li>
    <li value="中信银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/7.gif"></li>
    <li value="深圳发展银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/8.gif"></li>
    <li value="上海浦东发展银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/9.gif"></li>
    <li value="中国工商银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/10.gif"></li>
    <li value="北京银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/11.gif"></li>
    <li value="中国银行"><img src="<%=basePath%>/static/img/wallet/ico-bank/16.gif"></li>
   </ul>
   <div class="clearfix"></div>
   <div class="mar-top-10 mar-bottom-30 pad-15" style=" width:90%; margin-left:auto; margin-right:auto; border:1px solid #EAE4B5; background-color:#FFFBE7;">
     <form class="form-horizontal">
  <div class="form-group">
    <label for="" class="col-sm-5 control-label">银行卡号</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="bankCard" placeholder="输入卡号">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-5 control-label">姓名</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="" placeholder="银行卡开户姓名">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-5 control-label">证件</label>
    <div class="col-sm-4">
      <select name="" class="form-control" style="display:inline-block; width:95px;">
        <option>身份证</option>
        <option>护照</option>
        <option>台胞证</option>
        <option>港澳通行证</option>
        <option>其它</option>
      </select>
      <input type="text" class="form-control" style="display:inline-block; width:205px;" id="" placeholder="输入证件号">
    </div>
    
  </div>
  <div class="form-group">
    <label for="" class="col-sm-5 control-label">银行预留手机号</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="" placeholder="输入手机号">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-5 control-label">短信验证码</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" style=" display:inline-block; width:100px; " id="" placeholder="输入验证码"> <input type="button" value="免费获取" class="btn btn-primary btn-sm"  > 
    </div>
  </div>
</form>
   </div>
  </div>

  <!--list 02--> 
  <div class="tab-pane active bank-block-list2" id="tab2">
   <ul id="bank-change2" style=" width:90%; margin-left:auto; margin-right:auto;">
    <li value="支付宝"><img src="<%=basePath%>/static/img/wallet/ico-bank/zhifubao.jpg"></li>
    <li value="财付通"><img src="<%=basePath%>/static/img/wallet/ico-bank/caifutong.jpg"></li>
   </ul>
   <div class="clearfix"></div>
   <div class="mar-top-10 mar-bottom-30 font16 pad-10" style=" width:90%; margin-left:auto; margin-right:auto; border:1px solid #EAE4B5; background-color:#FFFBE7;">
     跳转至平台页面支付！
   </div>
  </div>

<!--list 03-->
  <div class="tab-pane" id="tab3">pos机信息维护···
   </div>
   
</div>
</div>
<!-- 支付选择 结束 -->



<div class="clearfix"></div>
<div class="text-center mar-bottom-10">
  <input type="button" value="立即支付" class="btn btn-danger" onClick="paye();" >
</div>

</div>

<div>
    <input id="flightNumber" type="hidden" value="${tikit.flightNumber}"></input>
    <input id="price" type="hidden" value="${price}"></input>
    <input id="chajia" type="hidden" value="${chajia}"></input>
    <input id="ctype" type="hidden" value="${ctype}"></input>
    <input id="stime" type="hidden" value="${tikit.startTime}"></input>
</div>
<script type="text/javascript">

    var paytype = 'kong';

    window.onload = function(){
        var obj_lis = document.getElementById("bank-change2").getElementsByTagName("li");
        for(i=0;i<obj_lis.length;i++){
            obj_lis[i].onclick = function(){
                paytype = this.getAttribute("value");
                //alert(paytype);
            }
        }
        var bobj_lis = document.getElementById("bank-change").getElementsByTagName("li");
        for(i=0;i<bobj_lis.length;i++){
            bobj_lis[i].onclick = function(){
                paytype = this.getAttribute("value");
                //alert(paytype);
            }
        }

    }


    function guanli() {

        dialog({
            width: '20em',
            cancel:false,
            title: '提示...',
            content: "<div class='text' style=' text-align:center;'>"+"您确定要登录管理员系统吗？"+"</div>",
            okValue: '确定',
            ok: function () {
                window.location.href = "toadminlogin";
            },
            button:[
                {
                    value:"取消"
                }
            ]
        }).showModal();


    }

    function toperrr () {
        window.location.href = "topersonal";
    }


    function tuichu() {
        window.location.href = "tologin";
    }

    function paye() {
        var flightNumber = $("#flightNumber").val();
        var price = $("#price").val();
        var chajia = $("#chajia").val();
        var bankCard = $("#bankCard").val();
        var stime = $("#stime").val();
        var ctype = $("#ctype").val();


        var datat = {
            "flightNumber": flightNumber,
            "price": price,
            "paytype":paytype,
            "bankCard":bankCard,
            "stime":stime,
            "chajia":chajia,
            "ctype":ctype
        };

        if (paytype == 'kong'){
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"请选择支付方式..."+"</div>",
                okValue: '确定',
                ok: function () {}
            }).showModal();
        }else {
            $.ajax({
                type: "POST",
                url: 'paye/buy',
                async : true,
                contentType:'application/json;charset=utf-8',
                data: JSON.stringify(datat),
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        window.location.href = data.nextUrl;
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content:"<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {}
                        }).showModal();
                    }
                }
            });
        }


    }
</script>

<!-- 修改联系人弹框 -->
<div id="contact-info" style="display:none">

	<div class="modal-body" >
    <form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">联系人</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" id="" placeholder="请输入姓名">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">手机号</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" id="" placeholder="请输入手机号">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">座机号</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" id="" placeholder="请输入座机号">
    </div>
  </div>
  
</form>
</div>
			
				<div class="modal-footer ">
				  <label class="mar-right-30" style=" font-weight:normal;"><input name="" type="checkbox" value="" checked> 下次继续使用该联系人</label>
				
				  <button type="button" class="btn btn-primary" onClick="layer.closeAll()">确 定</button>	<button type="button" class="btn btn-default" onClick="layer.closeAll()">关 闭</button>
				</div>
			</div>

<!-- 提交订单弹框 -->
<div id="rev" style="display:none">
				
				<div class="modal-body" >
         <div class="pad-10 ">  <span class="red"> 您购买的保险份数，少于实际预订乘客人数，请确认是否有乘客遗漏了保险。</span></div>
             
             </div>
			
				<div class="modal-footer mar-top-5">
				
					<button type="button" class="btn btn-primary" onClick="layer.closeAll()">返回，核实保险</button>	<button type="button" class="btn btn-default" onClick="location.href='支付.html'">没有问题，继续下一步</button>
				</div>
			</div>





<script type="text/javascript">

    $(function() {
        $('.bubbleinfo').each(function() {
            var distance = 10;
            var time = 10;
            var hideDelay = 100;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function() {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 30,
                        left:0,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function() {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function() {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function() {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
  
</script>


    
    
    
<script>
$(document).ready(function() {	
	
	$(".clk01").click(function(){  //组织架构
	   layer.open({
       type: 2,
       shift: 2,  //出场动画
       area: ['800px', '60%'],
	   title :'组织架构',
       shadeClose: true, //开启遮罩关闭
       content: '../企业客户管理/新组织架构.html'
       });
	});

    $('.zw1').on('click', function(){
      layer.open({
	  type: 1,
	  title: '修改联系人信息',
      area: ['400px', 'auto'],
      fix: false, //不固定
      maxmin: false,
      content: $('#contact-info'),
      });
    });
	
	$('.zw2').on('click', function(){  //提交订单弹框
      layer.open({
	  type: 1,
	  title: '提示',
      area: ['400px', 'auto'],
      fix: false, //不固定
      maxmin: false,
      content: $('#rev'),
      });
    });

});
</script>
</body>
</html>

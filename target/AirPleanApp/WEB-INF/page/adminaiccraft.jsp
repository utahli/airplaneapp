<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→航班管理</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>

    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li>
                    <a href="JavaScript:vistaaa();">用户登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，航班管理员-${sessionScope.adname} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii()">修改密码</a></li>
                        <li><a href="JavaScript:qiehuan();">切换用户</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="JavaScript:quanxian();"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i> 用户管理</a></li>
                <li ><a href="JavaScript:quanxian();"><span class="glyphicon glyphicon-yen font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px;  margin-bottom:2px;"></span>账务管理</a></li>
                <li class="active"><a href="#"><i class="icon iconfont icon-nav-block font24"  >&#xe623;</i>航班管理</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>查询条件</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">

            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">公司名称</label>
                    <select id="name" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <c:forEach items="${cilist}" var="ci">
                            <option value="${ci.companyName}-${ci.companyCode}">${ci.companyName}-${ci.companyCode}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">出发城市</label>
                    <select id="scity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <option value="长春">长春</option>
                        <option value="三亚">三亚</option>
                        <option value="北京">北京</option>
                        <option value="上海">上海</option>
                        <option value="广州">广州</option>
                        <option value="深圳">深圳</option>
                        <option value="成都">成都</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">到达城市</label>
                    <select id="lcity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <option value="长春">长春</option>
                        <option value="三亚">三亚</option>
                        <option value="北京">北京</option>
                        <option value="上海">上海</option>
                        <option value="广州">广州</option>
                        <option value="深圳">深圳</option>
                        <option value="成都">成都</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">航班号</label>
                    <input id="fnum" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="CA2038" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">起飞时间</label>
                    <input id="st" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onClick="WdatePicker()" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">到达时间</label>
                    <input id="lt" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onClick="WdatePicker()" >
                </div>
            </div>

        </div>
        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">

            <input type="button"
                   id="btn_login"
                   value="立即查询"
                   onclick="find();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="查询所有"
                   onclick="findall();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="清空条件"
                   onclick="clearair();"
                   class="btn btn-default btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="添加航班"
                   onclick="tianjaair();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="添加航空公司"
                   onclick="tianjacomp();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   id="btn_login"
                   value="查询航空公司"
                   onclick="chahkgs();"
                   class="btn btn-danger btn-sm mar-right-20"/>

        </div>

        <ol class="breadcrumb">
            <li>航班信息列表</li>
        </ol>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped font12 table-bordered v-align-top" >
            <thead>
            <tr >
                <th>序号</th>
                <th>公司</th>
                <th>航班</th><%--包含机型、航次--%>
                <th>起飞行程</th>
                <th>到达行程</th><%--包含机场--%>
                <th>经济舱</th><%--座位信息及票额--%>
                <th>头等舱</th><%--座位信息及票额--%>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <%!
                int count =1;
            %>

           <%--<c:if test="${sessionScope.adname != null}">--%>
            <c:if test="${find ==1}">

                <c:forEach items="${cflist}" var="afm">
                    <%--<div>
                        <input id="sdatetime" type="hidden" value="${sdatetime}"></input>
                        <input id="ldatetime" type="hidden" value="${ldatetime}"></input>
                        <input id="airName" type="hidden" value="${airName}"></input>
                        <input id="flightNumber" type="hidden" value="${flightNumber}"></input>
                        <input id="from" type="hidden" value="${from}"></input>
                        <input id="to" type="hidden" value="${to}"></input>
                        <input id="statAirport" type="hidden" value="${statAirport}"></input>
                        <input id="landAirport" type="hidden" value="${landAirport}"></input>
                        <input id="ctype" type="hidden" value="${ctype}"></input>
                        <input id="price" type="hidden" value="${price + 80}"></input>
                    </div>--%>
                    <tr>
                        <td><%=count++%></td><%--序号--%>
                        <td>${afm.companyName}</td><%--公司--%>
                        <td><div class="startbox"><%--航班---包含机型、航次--%>
                            <div class="startcity">
                                <span>${afm.flight.flightNumber}</span></br>
                                <l>${afm.flight.flightType}</l></br>
                                <ll>航次：<l>${afm.flight.voyageNumber}</l></ll>
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox"><%--起飞行程--%>
                            <div class="startcity">
                                <span>${afm.flight.startPlace}</span>
                                <l>${afm.flight.statAirport}</l></br>
                                <ll>${afm.flight.startTime}</ll></br>
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox"><%--到达行程--%>
                            <div class="startcity">
                                <span>${afm.flight.landPlace}</span>
                                <l>${afm.flight.landAirport}</l></br>
                                <ll>${afm.flight.landTime}</ll></br>
                                <lll>经停：<l>${afm.flight.waitTime}</l>分钟</lll>
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox"><%--舱位--%>
                            <div class="startcity">
                                <span>座位数：<l>${afm.flight.economicNumber}</l></span> &nbsp<lll>${afm.flight.discountRate/10}</lll>折 </br>
                                <ll>票面价：￥<l>${afm.flight.economicPrice}</l></ll></br>
                                优惠价：￥${Math.ceil(afm.flight.economicPrice*(afm.flight.discountRate/100))}
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox"><%--舱位--%>
                            <div class="startcity">
                                <span>座位数：<l>${afm.flight.businessNumber}</l></span> &nbsp${afm.flight.discountRate/10}折 </br>
                                <l>票面价：￥<l>${afm.flight.businessPrice}</l></l></br>
                                优惠价：￥${Math.ceil(afm.flight.businessPrice*(afm.flight.discountRate/100))}
                            </div>
                        </div>
                        </td>
                        <td>计划中</td>
                        <td><a href="#" class="flightedit">【编辑】</a><br>
                            <a href="#" class="dhanchu">【删除】</a><br>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
           <%-- </c:if>--%>
            <%
                count =1;
            %>
        </table>


        <div class="clearfix"></div>
    </div>
</div>
<!-- main end -->
</div>

<%--修改航班--%>
<div id="editflight" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">公司名称</label>
            <input id="ecompname" type="text" class="form-control input-sm w200" style=" display: inline-block;" readonly="readonly">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">航班号</label>
            <input id="efnum" type="text" class="form-control input-sm w200" style=" display: inline-block;" readonly="readonly">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">出发城市</label>
            <select id="escity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                <option value="">空</option>
                <option value="长春">长春</option>
                <option value="三亚">三亚</option>
                <option value="北京">北京</option>
                <option value="上海">上海</option>
                <option value="广州">广州</option>
                <option value="深圳">深圳</option>
                <option value="成都">成都</option>
            </select>
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">到达城市</label>
            <select id="elcity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                <option value="">空</option>
                <option value="长春">长春</option>
                <option value="三亚">三亚</option>
                <option value="北京">北京</option>
                <option value="上海">上海</option>
                <option value="广州">广州</option>
                <option value="深圳">深圳</option>
                <option value="成都">成都</option>
            </select>
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">起飞时间</label>
            <input id="est" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" >
        </div>
        <div >
            <label for="apdiv" class="w90 text-right">到达时间</label>
            <input id="elt" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" >
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">航次</label>
            <select id="ehangxian" class="form-control input-sm" style=" width:200px; display:inline-block;">
                <option value="">空</option>
                <option value="1800E">1800E</option>
                <option value="1800A">1800A</option>
                <option value="1900C">1900C</option>
            </select>
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">机型</label>
            <select id="ejixin" class="form-control input-sm" style=" width:200px; display:inline-block;">
                <option value="">空</option>
                <option value="波音 787-9(大型)">波音 787-9(大型)</option>
                <option value="波音 737(中型)">波音 737(中型)</option>
                <option value="空客 A330(大型)">空客 A330(大型)</option>
                <option value="空客 A321(中型)">空客 A321(中型)</option>
            </select>
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">经停</label>
            <input id="ewait" type="text" class="form-control input-sm w200" style=" display: inline-block;">
        </div>
        <div >
            <label for="apdiv" class="w90 text-right">经济舱位数</label>
            <input id="eeseat" type="text" class="form-control input-sm w200" style=" display: inline-block;">
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">经济舱位票价</label>
            <input id="eeprice" type="text" class="form-control input-sm w200" style=" display: inline-block;">
        </div>

        <div>
            <label for="apdiv" class="w90 text-right">头等舱位数</label>
            <input id="ebseat" type="text" class="form-control input-sm w200" style=" display: inline-block;">
        </div>
        <div >
            <label for="apdiv" class="w90 text-right">头等舱位票价</label>
            <input id="ebprice" type="text" class="form-control input-sm w200" style=" display: inline-block;"  >
        </div>
        <div>
            <label for="apdiv" class="w90 text-right">折扣</label>
            <input id="edisc" type="text" class="form-control input-sm w200" style=" display: inline-block;">
        </div>
        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="flightedett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>
</div>

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>
<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/adminchangpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            ok: function () {}
                        }).showModal();
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#userinfo'),
        });

    }

</script>

    <script>
        function clearair() {
            document.getElementById('name').value="";
            document.getElementById('scity').value="";
            document.getElementById('lcity').value="";
            document.getElementById('fnum').value="";
            document.getElementById('st').value="";
            document.getElementById('lt').value="";
        }

        function findall() {

            $.ajax({
                type: "POST",
                url: 'adcraftfindall',
                async : true,
                contentType:'application/json;charset=utf-8',
                //data: JSON.stringify(pass),
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        window.location.href = data.nextUrl;
                    }
                }
            });
        }

        function find() {
            if ($('#name').val() == "" &&$('#scity').val()==""&&$('#lcity').val()==""&&$('#fnum').val()==""&&$('#st').val()==""&&$('#lt').val()=="") {
                dialog({
                    width: '20em',
                    cancel:false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"查询条件不能为空！"+"</div>"  ,
                    okValue: '确定',
                    ok: function () {
                    },
                }).showModal();
                return;
            }
            dd = {
                "name":$('#name').val(),
                "scity":$('#scity').val(),
                "lcity":$('#lcity').val(),
                "fnum":$('#fnum').val(),
                "st":$('#st').val(),
                "lt":$('#lt').val(),
            };
            $.ajax({
                type: "POST",
                url: 'adcraftfindby',
                async : true,
                contentType:'application/json;charset=utf-8',
                data: JSON.stringify(dd),
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        window.location.href = data.nextUrl;
                    }else {
                        dialog({
                            width: '20em',
                            cancel:false,
                            title: '提示...',
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            },
                        }).showModal();
                    }
                }
            });

        }

/*航班信息修改===============================================================================================*/

        function cheackinfo() {

            if ("" == $("#escity option:selected").val()) {
                $("#escity").focus();
                layer.tips('出发城市不能为空', $("#escity"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#elcity option:selected").val()) {
                $("#elcity").focus();
                layer.tips('到达城市不得为空', $("#elcity"),{bg: '#AE81FF',time: 2000});
                return false;
            }

            if($("#escity option:selected").val() ==$("#elcity option:selected").val()) {
                layer.alert("<div class='text' style=' text-align:center;'>"+'出发城市不能与到达城市相同！'+"</div>"  , {
                    skin: 'layui-layer-lan'
                    ,closeBtn: 0
                    ,shift: 4 //动画类型
                });
                return false;
            }


            if ("" == $("#est").val()) {
                $("#est").focus();
                layer.tips('起飞时间不得为空', $("#est"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            var stime = new Date(Date.parse($("#est").val()));
            var now = new Date();
            now.setTime(now.getTime() + 12 * 60 * 60 * 1000);
            if (stime < now) {
                layer.alert("<div class='text' style=' text-align:center;'>"+'起飞时间应与当前时间相差12小时'+"</div>"  , {
                    skin: 'layui-layer-lan'
                    ,closeBtn: 0
                    ,shift: 4 //动画类型
                });
                return false;
            }
            if ("" == $("#elt").val()) {
                $("#elt").focus();
                layer.tips('到达时间不得为空', $("#elt"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            var ltime = new Date(Date.parse($("#elt").val()));
            if(ltime <= stime) {
                layer.alert("<div class='text' style=' text-align:center;'>"+'到达时间不得小于起飞时间'+"</div>", {
                    skin: 'layui-layer-lan'
                    ,closeBtn: 0
                    ,shift: 4 //动画类型
                });
                return false;
            }
            if ("" == $("#ejixin option:selected").val()) {
                $("#ejixin").focus();
                layer.tips('机型不得为空', $("#ejixin"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#ehangxian option:selected").val()) {

                $("#ehangxian").focus();
                layer.tips('航线不得为空', $("#ehangxian"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#ewait").val()) {

                $("#ewait").focus();
                layer.tips('经停时分不得为空', $("#ewait"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#eeseat").val()) {

                $("#eeseat").focus();
                layer.tips('经济舱位数不得为空', $("#eeseat"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#eeprice").val()) {

                $("#eeprice").focus();
                layer.tips('经济舱票价不得为空', $("#eeprice"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#ebseat").val()) {

                $("#ebseat").focus();
                layer.tips('头等舱位数不得为空', $("#ebseat"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#ebprice").val()) {
                $("#ebprice").focus();
                layer.tips('头等舱票价不得为空', $("#ebprice"),{bg: '#AE81FF',time: 2000});
                return false;
            }
            if ("" == $("#edisc").val()) {
                $("#edisc").focus();
                layer.tips('折扣不得为空', $("#edisc"),{bg: '#AE81FF',time: 2000});
                return false;
            }

            return true;
        }



        function flightedett() {

            if (cheackinfo()) {
                dd = {
                   // "ecompname":$('#ecompname').val(),
                    "efnum":$('#efnum').val(),
                    "escity":$('#escity option:selected').val(),
                    "elcity":$('#elcity option:selected').val(),
                    "est":$('#est').val(),
                    "elt":$('#elt').val(),
                    "ehangxian":$('#ehangxian option:selected').val(),
                    "ejixin":$('#ejixin option:selected').val(),
                    "ewait":$('#ewait').val(),
                    "eeseat":$('#eeseat').val(),
                    "eeprice":$('#eeprice').val(),
                    "ebseat":$('#ebseat').val(),
                    "ebprice":$('#ebprice').val(),
                    "edisc":$('#edisc').val(),
                };

                $.ajax({
                    type: "POST",
                    url: 'adcraftupdate',
                    async : true,
                    contentType:'application/json;charset=utf-8',
                    data:JSON.stringify(dd) ,
                    dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                    cache: false,
                    success: function (data) {
                        if (data.code == 1) {
                            layer.closeAll();
                            dialog({
                                width: '20em',
                                title: '提示...',
                                cancel:false,
                                content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                okValue: '确定',
                                ok: function () {
                                    //window.location.href = data.nextUrl;
                                }
                            }).showModal();
                        } else {
                            layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                                skin: 'layui-layer-lan'
                                ,closeBtn: 0
                                ,shift: 4 //动画类型
                            });
                           // layer.closeAll();
                            /*dialog({
                                width: '20em',
                                title: '提示...',
                                cancel:false,
                                content: data.msg,
                                ok: function () {}
                            }).showModal();*/
                        }
                    }
                });

            }
        }

        $(document).ready(function() {

            $(".flightedit").click(function() {

                document.getElementById("ecompname").value = $(this).parents("tr").children("td").eq(1).html();//公司名称
                document.getElementById("efnum").value = $(this).parents("tr").children("td").eq(2).find("div").find("div").find("span").html();//航班号
                document.getElementById("escity").value = $(this).parents("tr").children("td").eq(3).find("div").find("div").find("span").html();//出发城市
                document.getElementById("elcity").value = $(this).parents("tr").children("td").eq(4).find("div").find("div").find("span").html();//出发城市
                document.getElementById("est").value = $(this).parents("tr").children("td").eq(3).find("div").find("div").find("ll").html();//起飞时间
                document.getElementById("elt").value = $(this).parents("tr").children("td").eq(4).find("div").find("div").find("ll").html();//到达时间
                document.getElementById("ehangxian").value = $(this).parents("tr").children("td").eq(2).find("div").find("div").find("ll").find("l").html();//航次
                document.getElementById("ejixin").value = $(this).parents("tr").children("td").eq(2).find("div").find("div").find("l").html();//机型
                document.getElementById("ewait").value = $(this).parents("tr").children("td").eq(4).find("div").find("div").find("lll").find("l").html();//经停
                document.getElementById("eeseat").value = $(this).parents("tr").children("td").eq(5).find("div").find("div").find("span").find("l").html();//经济舱位数
                document.getElementById("eeprice").value = $(this).parents("tr").children("td").eq(5).find("div").find("div").find("ll").find("l").html();//经济舱位票价
                document.getElementById("ebseat").value = $(this).parents("tr").children("td").eq(6).find("div").find("div").find("span").find("l").html();//头等舱位数
                document.getElementById("ebprice").value = $(this).parents("tr").children("td").eq(6).find("div").find("div").find("l").find("l").html();//头等舱位票价
                document.getElementById("edisc").value = $(this).parents("tr").children("td").eq(5).find("div").find("div").find("lll").html();//折扣

                layer.open({
                    type: 1,
                    title: '航班信息修改',
                    area: ['400px', 'auto'],
                    fix: false,
                    //skin: 'layui-layer-rim', //加上边框
                    closeBtn: 0, //不显示关闭按钮
                    maxmin: false,
                    shift: 4, //动画类型
                    content: $('#editflight'),
                });
            });

        });

        $(".dhanchu").click(function() {
            dialog({
                width: '20em',
                title: '提示...',
                cancel:false,
                content: "<div class='text' style=' text-align:center;'>"+"删除......"+"</div>"  ,
                ok: function () {}
            }).showModal();
        });


        function chahkgs() {
            window.location.href = "tosearchcomp";
        }


        function tianjaair() {
            window.location.href = "toaddair";
        }

        function tianjacomp() {
            window.location.href = "toaddcomp";
        }

        function vistaaa() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要登录用户系统吗？"+"</div>"  ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "tologin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }
        function quanxian() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您没有该管理员权限！"+"</div>" ,
                okValue: '确定',
                ok: function () {
                },
            }).showModal();
        }

        function qiehuan() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要切换用户吗？"+"</div>"  ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "toadminlogin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }


        function shouye() {
            window.location.href = "tohome";
        }


    </script>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<script type="text/javascript" src="<%=basePath%>/static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/dialog.js"></script>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <!-- IE 浏览器运行最新的渲染模式-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 启用响应式特性 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 双核使用webkit内核 -->
    <meta name="renderer" content="webkit">
    <title>航空票务系统→添加航班</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/common.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/corptravel.css" rel="stylesheet">
    <link href="<%=basePath%>/static/css/enterprise.css" rel="stylesheet">
    <!-- IconFont图标 -->
    <link href="<%=basePath%>/static/css/iconfont.css" rel="stylesheet">
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <!-- 兼容IE8 -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=basePath%>/static/html5shiv.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/respond.min.js"></script>
    <![endif]-->
    <!-- layer弹框 2.1 -->
    <script type="text/javascript" src="<%=basePath%>/static/js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/jquery.tips.js"></script>


    <style>
        .tabs { width:35px; padding:3px; ; background-color:#db514f; color:#fff; text-align:center; margin-top:2px; margin-bottom:2px; border-radius:3px; }
        .startbox { overflow:hidden; margin-bottom:5px; }
        .start { float:left; background-color:#f9a60a; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination { float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .startcity { float:left; font-size:12px; color:#666; }
        .startcity span { font-size:14px; color:#000; font-weight:bold; }
        .destination1 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
        .destination2 {float:left; background-color:#0096de; padding:2px; color:#fff; border-radius:2px; margin-right:5px; }
    </style>
</head>
<body class="bg-body">
<!-- header -->
<nav class="navbar navbar-default navbar-fixed-top bg-white" >
    <!-- 小导航 -->
    <nav class="navbar navbar-default" style=" min-height:30px; line-height:30px; margin-bottom:0px; border-radius:0;">
        <div class="container font12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-nav nav-top-small" style="margin-left:-15px;" >
                <li>
                    <a href="JavaScript:vistaaa();">用户登录</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">您好，航班管理员-${sessionScope.adname} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="JavaScript:Bianjii();">修改密码</a></li>
                        <li><a href="JavaScript:qiehuan();">切换用户</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- 小导航结束 -->

    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="JavaScript:fanhuii();"><span class="glyphicon glyphicon-cog font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px; margin-bottom:2px;"></span>返回</a></li>
                <li ><a href="JavaScript:quanxian();"><i class="icon iconfont icon-nav-block font24" >&#xe61c;</i> 用户管理</a></li>
                <li ><a href="JavaScript:quanxian();"><span class="glyphicon glyphicon-yen font24 icon-nav-block" aria-hidden="true" style="margin-top:-2px;  margin-bottom:2px;"></span>账务管理</a></li>
                <li class="active"><a href="#"><i class="icon iconfont icon-nav-block font24"  >&#xe623;</i>航班管理</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- header end -->
<!-- 管理模板 -->
<div class="container bg-gray-eee box-shadow mar-bottom-30" style="padding-right:0px; padding-left:0px; position:relative; margin-top:120px;">

    <!-- main -->
    <div class="rightCon" style="">
        <!-- 引导 -->
        <ol class="breadcrumb">
            <li>航班信息</li>
        </ol>
        <!-- 引导结束 -->
        <div class="row" id="user">

            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">公司名称</label>
                    <select id="comp" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <c:forEach items="${cilist}" var="ci">
                            <option value="${ci.companyCode}">${ci.companyName}-${ci.companyCode}</option>
                        </c:forEach>

                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                <label for="apdiv" class="w90 text-right">出发城市</label>
                <select id="scity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                    <option value="">空</option>
                    <option value="长春">长春</option>
                    <option value="三亚">三亚</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                    <option value="广州">广州</option>
                    <option value="深圳">深圳</option>
                    <option value="成都">成都</option>
                </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                <label for="apdiv" class="w90 text-right">到达城市</label>
                <select id="lcity" class="form-control input-sm" style=" width:200px; display:inline-block;">
                    <option value="">空</option>
                    <option value="长春">长春</option>
                    <option value="三亚">三亚</option>
                    <option value="北京">北京</option>
                    <option value="上海">上海</option>
                    <option value="广州">广州</option>
                    <option value="深圳">深圳</option>
                    <option value="成都">成都</option>

                </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">航班号</label>
                    <input id="fnum" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="CA2038" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">起飞时间</label>
                    <input id="st" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">到达时间</label>
                    <input id="lt" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="yyyy-MM-dd" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="apdiv" class="w90 text-right">航次</label>
                    <select id="hangxian" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <option value="1800E">1800E</option>
                        <option value="1800A">1800A</option>
                        <option value="1900C">1900C</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">机型</label>
                    <select id="jixin" class="form-control input-sm" style=" width:200px; display:inline-block;">
                        <option value="">空</option>
                        <option value="波音 787-9(大型)">波音 787-9(大型)</option>
                        <option value="波音 737(中型)">波音 737(中型)</option>
                        <option value="空客 A330(大型)">空客 A330(大型)</option>
                        <option value="空客 A321(中型)">空客 A321(中型)</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">经停</label>
                    <input id="wait" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="分钟" >
                </div>
            </div>
        </div>
        <div class="row" id="user2">
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">经济舱位数</label>
                    <input id="eseat" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="200" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">经济舱位票价</label>
                    <input id="eprice" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="￥" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">折扣</label>
                    <input id="disc" type="text" class="form-control input-sm w200" style=" display: inline-block;" placeholder="9.5" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">头等舱位数</label>
                    <input id="bseat" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="10" >
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" >
                    <label for="apdiv" class="w90 text-right">头等舱位票价</label>
                    <input id="bprice" type="text" class="form-control input-sm w200" style=" display: inline-block;"  placeholder="￥" >
                </div>
            </div>

        </div>


        <div class="text-center mar-top-10 bor-top-solid-1 pad-top-10 mar-bottom-10">
            <input type="button"
                   value="确认添加"
                   onclick="addair();"
                   class="btn btn-danger btn-sm mar-right-20"/>
            <input type="button"
                   value="清空条件"
                   onclick="qingkongair();"
                   class="btn btn-default btn-sm mar-right-20"/>
        </div>

       <%-- <ol class="breadcrumb">
            <li>航班信息列表</li>
        </ol>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-hover table-striped font12 table-bordered v-align-top" >
            <thead>
            <tr >
                <th>序号</th>
                <th>公司</th>
                <th>航班</th>&lt;%&ndash;包含机型、航次&ndash;%&gt;
                <th>起飞行程</th>
                <th>到达行程</th>&lt;%&ndash;包含机场&ndash;%&gt;
                <th>经济舱</th>&lt;%&ndash;座位信息及票额&ndash;%&gt;
                <th>头等舱</th>&lt;%&ndash;座位信息及票额&ndash;%&gt;
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <%!
                int count =1;
            %>
           <c:if test="${sessionScope.adname != null}">
                <c:if test="${find ==1}">


            <c:forEach items="${cflist}" var="afm">
                &lt;%&ndash;<div>
                    <input id="sdatetime" type="hidden" value="${sdatetime}"></input>
                    <input id="ldatetime" type="hidden" value="${ldatetime}"></input>
                    <input id="airName" type="hidden" value="${airName}"></input>
                    <input id="flightNumber" type="hidden" value="${flightNumber}"></input>
                    <input id="from" type="hidden" value="${from}"></input>
                    <input id="to" type="hidden" value="${to}"></input>
                    <input id="statAirport" type="hidden" value="${statAirport}"></input>
                    <input id="landAirport" type="hidden" value="${landAirport}"></input>
                    <input id="ctype" type="hidden" value="${ctype}"></input>
                    <input id="price" type="hidden" value="${price + 80}"></input>
                </div>&ndash;%&gt;
                <c:forEach items="${afm.flightList}" var="vi">

                    <tr>
                        <td><%=count++%></td>&lt;%&ndash;序号&ndash;%&gt;
                        <td>${afm.companyName}</td>&lt;%&ndash;公司&ndash;%&gt;
                        <td><div class="startbox">&lt;%&ndash;航班---包含机型、航次&ndash;%&gt;
                            <div class="startcity">
                                <span>${vi.flightNumber}</span></br>
                                <l>${vi.flightType}</l></br>
                                <ll>航次：${vi.voyageNumber}</ll>
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox">&lt;%&ndash;起飞行程&ndash;%&gt;
                            <div class="startcity">
                                <span>${vi.startPlace}</span>
                                <l>${vi.statAirport}</l></br>
                                <ll>${vi.startTime}</ll></br>
                                <ll>经停：${vi.waitTime}分钟</ll>
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox">&lt;%&ndash;到达行程&ndash;%&gt;
                            <div class="startcity">
                                <span>${vi.landPlace}</span>
                                <l>${vi.landAirport}</l></br>
                            <ll>${vi.landTime}</ll></br>
                        </div>
                        </div>
                        </td>
                        <td><div class="startbox">&lt;%&ndash;舱位&ndash;%&gt;
                            <div class="startcity">
                                <span>座位数：${vi.economicNumber}</span> &nbsp${vi.discountRate/10}折 </br>
                                <l>票面价：￥${vi.economicPrice}</l></br>
                                优惠价：￥${Math.ceil(vi.economicPrice*(vi.discountRate/100))}
                            </div>
                        </div>
                        </td>
                        <td><div class="startbox">&lt;%&ndash;舱位&ndash;%&gt;
                            <div class="startcity">
                                <span>座位数：${vi.businessNumber}</span> &nbsp${vi.discountRate/10}折 </br>
                                <l>票面价：￥${vi.businessPrice}</l></br>
                                优惠价：￥${Math.ceil(vi.businessPrice*(vi.discountRate/100))}
                            </div>
                        </div>
                        </td>
                        <td>计划中</td>
                        <td><a href="#">【编辑】</a><br>
                            <a href="#" class="qxdd">【删除】</a><br>
                        </td>

                    </tr>

                </c:forEach>
            </c:forEach>
                </c:if>
            </c:if>
            <%
                count =1;
            %>
        </table>--%>


        <div class="clearfix"></div>
    </div>
</div>

<!-- main end -->

<%--修改密码--%>
<div id="xgmm" style="display:none">
    <div class="modal-body" >
        <div>
            <label for="apdiv" class="w90 text-right">原始密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="oldpwd" >

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">新密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="newpwd" placeholder="密码不能小于6位">

        </div>
        <div>
            <label for="apdiv" class="w90 text-right">确认密码</label>
            <input type="password" class="form-control input-sm w200" style=" display: inline-block;" id="rpwd" placeholder="">

        </div>

        <div class="modal-footer mar-top-5" style=' text-align:center;'>
            <button type="button" class="btn btn-primary" onClick="edett();">确认修改</button>
            <button type="button" class="btn btn-default" onClick="layer.closeAll()">取消</button>
        </div>
    </div>

</div>

<script>

    function edett() {
        if (cheackiii()) {
            dd = {
                "oldpwd":$('#oldpwd').val(),
                "newpwd":$('#newpwd').val()
            };

            $.ajax({
                type: "POST",
                url: 'logAndReg/adminchangpwd',
                async : true,
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify(dd) ,
                dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                cache: false,
                success: function (data) {
                    if (data.code == 1) {
                        layer.closeAll();
                        dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                            okValue: '确定',
                            ok: function () {
                            }
                        }).showModal();
                    } else {
                        layer.alert("<div class='text' style=' text-align:center;'>"+data.msg+"</div>", {
                            skin: 'layui-layer-lan'
                            ,closeBtn: 0
                            ,shift: 4 //动画类型
                        });
                        /*dialog({
                            width: '20em',
                            title: '提示...',
                            cancel:false,
                            content: data.msg,
                            ok: function () {}
                        }).showModal();*/
                    }
                }
            });
        }
    }

    function cheackiii() {

        if ($('#oldpwd').val().length < 6) {
            $('#oldpwd').focus();
            layer.tips('密码不能小于6位', $("#oldpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#newpwd').val().length < 6) {
            $('#newpwd').focus();
            layer.tips('密码不能小于6位', $("#newpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        if ($('#rpwd').val() != $('#newpwd').val()) {
            $('#rpwd').focus();
            layer.tips('两次密码不一致', $("#rpwd"),{bg: '#AE81FF',time: 2000});
            return false;
        }
        return true;
    }

    function Bianjii() {
        document.getElementById("oldpwd").value = "";
        document.getElementById("newpwd").value = "";
        document.getElementById("rpwd").value = "";

        layer.open({
            type: 1,
            title: '修改密码',
            area: ['400px', 'auto'],
            fix: false,
            //skin: 'layui-layer-rim', //加上边框
            closeBtn: 0, //不显示关闭按钮
            maxmin: false,
            shift: 4, //动画类型
            content: $('#xgmm'),
        });

    }
</script>

    <script>


        function cheackinfo() {
            if ("" == $('#comp option:selected') .val()) {
                $("#comp").tips({
                    side: 2,
                    msg: '公司名称不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#comp").focus();
                return false;
            }
            if ("" == $("#scity option:selected").val()) {
                $("#scity").tips({
                    side: 2,
                    msg: '出发城市不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#scity").focus();
                return false;
            }
            if ("" == $("#lcity option:selected").val()) {
                $("#lcity").tips({
                    side: 2,
                    msg: '到达城市不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#lcity").focus();
                return false;
            }

            if($("#scity option:selected").val() ==$("#lcity option:selected").val()) {
                dialog({
                    width: '20em',
                    cancel: false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"出发城市不能与到达城市相同！"+"</div>" ,
                    okValue: '确定',
                    ok: function () {
                    }
                }).showModal();
                return false;
            }

            if ("" == $("#fnum").val()) {
                $("#fnum").tips({
                    side: 2,
                    msg: '航班号不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#fnum").focus();
                return false;
            }

            var fn = $("#fnum").val();
            var cop = $('#comp option:selected') .val();
            if(fn.indexOf(cop)<0) {
                dialog({
                    width: '20em',
                    cancel: false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"航班号与航空公司编号不匹配"+"</div>" ,
                    okValue: '确定',
                    ok: function () {
                    }
                }).showModal();
                return false;
            }
            if ("" == $("#st").val()) {
                $("#st").tips({
                    side: 2,
                    msg: '起飞时间不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#st").focus();
                return false;
            }
            var stime = new Date(Date.parse($("#st").val()));
            var now = new Date();
            now.setTime(now.getTime() + 12 * 60 * 60 * 1000);
            if (stime < now) {
                dialog({
                    width: '20em',
                    cancel: false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"起飞时间应与当前时间相差12小时"+"</div>" ,
                    okValue: '确定',
                    ok: function () {
                    }
                }).showModal();

                return false;
            }
            if ("" == $("#lt").val()) {
                $("#lt").tips({
                    side: 2,
                    msg: '到达时间不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#lt").focus();
                return false;
            }
            var ltime = new Date(Date.parse($("#lt").val()));
            if(ltime <= stime) {
                dialog({
                    width: '20em',
                    cancel: false,
                    title: '提示...',
                    content: "<div class='text' style=' text-align:center;'>"+"到达时间不得小于起飞时间"+"</div>" ,
                    okValue: '确定',
                    ok: function () {
                    }
                }).showModal();

                return false;
            }
            if ("" == $("#jixin option:selected").val()) {
                $("#jixin").tips({
                    side: 2,
                    msg: '机型不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#jixin").focus();
                return false;
            }
            if ("" == $("#hangxian option:selected").val()) {
                $("#hangxian").tips({
                    side: 2,
                    msg: '航线不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#hangxian").focus();
                return false;
            }
            if ("" == $("#wait").val()) {
                $("#wait").tips({
                    side: 2,
                    msg: '经停时分不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#wait").focus();
                return false;
            }
            if ("" == $("#eseat").val()) {
                $("#eseat").tips({
                    side: 2,
                    msg: '经济舱位数不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#eseat").focus();
                return false;
            }
            if ("" == $("#eprice").val()) {
                $("#eprice").tips({
                    side: 2,
                    msg: '经济舱票价不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#eprice").focus();
                return false;
            }
            if ("" == $("#bseat").val()) {
                $("#bseat").tips({
                    side: 2,
                    msg: '头等舱位数不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#bseat").focus();
                return false;
            }
            if ("" == $("#bprice").val()) {
                $("#bprice").tips({
                    side: 2,
                    msg: '头等舱票价不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#bprice").focus();
                return false;
            }
            if ("" == $("#disc").val()) {
                $("#disc").tips({
                    side: 2,
                    msg: '折扣不得为空',
                    bg: '#AE81FF',
                    time: 2
                });
                $("#disc").focus();
                return false;
            }

            return true;
        }

        function qingkongair() {
            document.getElementById('comp').value="";
            document.getElementById('scity').value="";
            document.getElementById('lcity').value="";
            document.getElementById('jixin').value="";
            document.getElementById('hangxian').value="";
            document.getElementById('fnum').value="";
            document.getElementById('st').value="";
            document.getElementById('lt').value="";
            document.getElementById('wait').value="";
            document.getElementById('eseat').value="";
            document.getElementById('eprice').value="";
            document.getElementById('bseat').value="";
            document.getElementById('bprice').value="";
            document.getElementById('disc').value="";

        }

        function addair() {

            if (cheackinfo()) {

                var comp = $('#comp option:selected').val();//公司
                var scity = $("#scity option:selected").val();//出发城市
                var lcity = $("#lcity option:selected").val();//到达城市
                var jixin = $("#jixin option:selected").val();//机型
                var hangxian = $("#hangxian option:selected").val();//航线
                var fnum = $("#fnum").val();//航班号
                var st = $("#st").val();//起飞时间
                var lt = $("#lt").val();//到达时间
                var wait = $("#wait").val();//经停
                var eseat = $("#eseat").val();//经济舱座位数
                var eprice = $("#eprice").val();//经济舱票价
                var bseat = $("#bseat").val();//头等舱座位数
                var bprice = $("#bprice").val();//头等舱票价
                var disc = $("#disc").val();//折扣

                var dd = {
                    "comp":comp,
                    "scity":scity,
                    "lcity":lcity,
                    "jixin":jixin,
                    "hangxian":hangxian,
                    "fnum":fnum,
                    "st":st,
                    "lt":lt,
                    "wait":wait,
                    "eseat":eseat,
                    "eprice":eprice,
                    "bseat":bseat,
                    "bprice":bprice,
                    "disc":disc,

                }

                $.ajax({
                    type: "POST",
                    url: 'saerch/adminaddair',
                    async: true,
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(dd),
                    dataType: 'json',   //当这里指定为json的时候，获取到了数据后会自己解析的，只需要 返回值.字段名称 就能使用了
                    cache: false,
                    success: function (data) {
                        if (data.code == 1) {

                            dialog({
                                width: '20em',
                                cancel: false,
                                title: '提示...',
                                content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                okValue: '确定',
                                ok: function () {

                                },
                                button:[
                                    {
                                        value:"取消",
                                        callback:function () {
                                            window.history.back();
                                        }
                                    }
                                ]
                            }).showModal();
                        } else {
                            dialog({
                                width: '20em',
                                cancel: false,
                                title: '提示...',
                                content: "<div class='text' style=' text-align:center;'>"+data.msg+"</div>",
                                okValue: '确定',
                                ok: function () {
                                }
                            }).showModal();
                        }
                    }
                });
            }
        }

        function fanhuii() {
            window.location.href = "toaircraft";
            //window.history.back();
        }


        function vistaaa() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要登录用户系统吗？"+"</div>" ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "tologin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }

        function quanxian() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您没有该管理员权限！"+"</div>" ,
                okValue: '确定',
                ok: function () {
                },
            }).showModal();
        }

        function qiehuan() {
            dialog({
                width: '20em',
                cancel:false,
                title: '提示...',
                content: "<div class='text' style=' text-align:center;'>"+"您确定要切换用户吗？"+"</div>" ,
                okValue: '确定',
                ok: function () {
                    window.location.href = "toadminlogin";
                },
                button:[
                    {
                        value:"取消"
                    }
                ]
            }).showModal();

        }



    </script>
</body>
</html>
